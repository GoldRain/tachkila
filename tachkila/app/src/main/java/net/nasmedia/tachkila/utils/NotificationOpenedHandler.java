package net.nasmedia.tachkila.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.activities.MessageActivity;
import net.nasmedia.tachkila.activities.SplashActivity;
import net.nasmedia.tachkila.fragments.MessagesFragment;
import net.nasmedia.tachkila.realmmodels.Message;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

/**
 * Created by Mahmoud Galal on 07/07/2017.
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    Context context;

    public NotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OneSignal.clearOneSignalNotifications();
        JSONObject jsonObject = result.notification.payload.additionalData;
        Log.d("message===>OpenHandle=>", jsonObject.toString());
        if (jsonObject.has("message")) {
            try {
                final Message message = new Gson().fromJson(jsonObject.toString(), Message.class);

                Log.d("messageType==>",message.getType());
                Log.d("message_cotent+>", String.valueOf(message));

                if (message.getType().equals("typing_started")){

                } else if (message.getType().equals("typing_finished")){

                } else {

                    Realm realm = Realm.getDefaultInstance(); // opens "myrealm.realm"
                    try {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(message);
                                Log.d("messages_noti_open", "message recieved");
                            }
                        });
                    } finally {
                        realm.close();
                    }
                    int groupId = jsonObject.getInt("group_id");
                    Intent intent = new Intent(context, MessageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(MessagesFragment.ARG_GROUP_ID, groupId);
                    context.startActivity(intent);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Intent intent = new Intent(context, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

    }
}
