package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.activities.SplashActivity;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.delete_game.DeleteGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.viewmodels.SettingsViewModel;
import net.nasmedia.tachkila.widgets.CustomTextView;
import net.nasmedia.tachkila.widgets.WhiteCircularTextView;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * Created by Mahmoud Galal on 13/10/2016.
 */

public class SettingFragment extends BaseFragment implements OnDataLoaded, SettingsViewModel.OnError {


    @BindView(R.id.profilePictureInsTxt)
    WhiteCircularTextView profilePictureInsTxt;
    @BindView(R.id.profilePictureImg)
    ImageView profilePictureImg;
    @BindView(R.id.nameEdt)
    EditText nameEdt;
    @BindView(R.id.usernameEdt)
    EditText usernameEdt;
    @BindView(R.id.emailEdt)
    EditText emailEdt;
    @BindView(R.id.phoneEdt)
    EditText phoneEdt;
    @BindView(R.id.birthdayTxt)
    CustomTextView birthdayTxt;
    @BindView(R.id.genderSpn)
    Spinner genderSpn;
    @BindView(R.id.countrySpn)
    Spinner countrySpn;
    @BindView(R.id.heightEdt)
    EditText heightEdt;
    @BindView(R.id.positionSpn)
    Spinner positionSpn;
    @BindView(R.id.footSpn)
    Spinner footSpn;
    @BindView(R.id.tshirtEdt)
    EditText tshirtEdt;
    @BindView(R.id.oldPasswordEdt)
    EditText oldPasswordEdt;
    @BindView(R.id.newPasswordEdt)
    EditText newPasswordEdt;
    @BindView(R.id.confirmPasswordEdt)
    EditText confirmPasswordEdt;
    @BindView(R.id.notificationSoundChkbx)
    CheckBox notificationSoundChkbx;
    @BindView(R.id.logoutBtn)
    Button logoutBtn;
    @BindView(R.id.conactUsBtn)
    Button conactUsBtn;
    @BindView(R.id.faqBtn)
    Button faqBtn;

    private SettingsViewModel mViewModel;
    private String mAvatarPath;

    private MaterialDialog mProgressDialog;
//    RequestManager requestManager;

    public static SettingFragment getInstance() {
        return new SettingFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = new SettingsViewModel(getActivity(), this, this);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
//        requestManager = Glide.with(this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        notificationSoundChkbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PreferenceHelper.getInstance(getActivity()).setIsNotificationSoundEnabled(true);
                } else {
                    PreferenceHelper.getInstance(getActivity()).setIsNotificationSoundEnabled(false);
                }
            }
        });
        mViewModel.onStart();

        setUserData();

        return view;
    }

    @Override
    public void onDestroyView() {
        mViewModel.onStop();
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void setUserData() {
        nameEdt.setText(mViewModel.getUser().getName());
        usernameEdt.setText(mViewModel.getUser().getUsername());
        emailEdt.setText(mViewModel.getUser().getEmail());
        phoneEdt.setText(mViewModel.getUser().getPhone());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            Date before = simpleDateFormat.parse(mViewModel.getUser().getBirthday());
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM yyyy, dd", Locale.US);
            birthdayTxt.setText(simpleDateFormat1.format(before));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        heightEdt.setText(mViewModel.getUser().getHeight() + "");
        tshirtEdt.setText(mViewModel.getUser().getFavorite_number() + "");

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_settings_spinner_text, mViewModel.getGenderList());
        genderAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        genderSpn.setAdapter(genderAdapter);
        genderSpn.setSelection(mViewModel.getUserGenderPosition());

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_settings_spinner_text, mViewModel.getCountriesNames());
        countryAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        countrySpn.setAdapter(countryAdapter);
        countrySpn.setSelection(mViewModel.getSelectedCountryPosition());

        ArrayAdapter<String> positionsAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_settings_spinner_text, mViewModel.getPositionsNames());
        countryAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        positionSpn.setAdapter(positionsAdapter);
        positionSpn.setSelection(mViewModel.getSelectedPostionPosition());

        ArrayAdapter<String> perfectFootAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_settings_spinner_text, mViewModel.getPerfectFootList());
        countryAdapter.setDropDownViewResource(R.layout.item_spinner_text);
        footSpn.setAdapter(perfectFootAdapter);
        footSpn.setSelection(mViewModel.getUserPerfectFootPosition());


        if (mViewModel.getUser().getAvatar() != null && !TextUtils.isEmpty(mViewModel.getUser().getAvatar())) {
            profilePictureInsTxt.setVisibility(View.GONE);
            profilePictureImg.setVisibility(View.VISIBLE);

            loadImage(mViewModel.getUser().getAvatar(), profilePictureImg);

        }

        if (PreferenceHelper.getInstance(getActivity()).isNotificationSoundEnabled()) {
            notificationSoundChkbx.setChecked(true);
        } else {
            notificationSoundChkbx.setChecked(false);
        }

    }

    private void loadImage(String url, ImageView imageView) {
        Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url);

//        requestManager.load(Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url))
//                .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
//                .skipMemoryCache(true)
//                .into(imageView);
    }


    @Override
    public String getTitle() {
        return "Settings";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("Error did happened")
                        .positiveText("OK").show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("Please check internet connection")
                        .positiveText("OK").show();
                break;
            case START:
                mProgressDialog.show();
                break;
            case SUCCESS:
                new MaterialDialog.Builder(getActivity())
                        .title("Success")
                        .content("Your user successfully updated")
                        .positiveText("OK").show();
                break;
        }
    }

    @OnClick({R.id.profilePictureInsTxt, R.id.profilePictureImg, R.id.birthDayContainer, R.id.faqBtn, R.id.conactUsBtn, R.id.logoutBtn, R.id.privacyBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profilePictureInsTxt:
            case R.id.profilePictureImg:
                pickImage();
                break;
            case R.id.birthDayContainer:
                showDatePickerDialog();
                break;
            case R.id.faqBtn:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.putExtra("url", mViewModel.getFaqUrl());
                intent.putExtra("title", "FAQ");
                startActivity(intent);
                break;
            case R.id.conactUsBtn:
                Intent intent1 = new Intent(getActivity(), DetailsActivity.class);
                intent1.setAction(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT);
                intent1.addCategory(Intent.CATEGORY_DEFAULT);
                intent1.putExtra("url", mViewModel.getContactUrl());
                intent1.putExtra("title", "ContactUs");
                startActivity(intent1);
                break;
            case R.id.logoutBtn:

                new MaterialDialog.Builder(getActivity())
                        .content("Are you sure ?")
                        .positiveText("YES")
                        .negativeText("NO")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                logout();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();


                break;
            case R.id.privacyBtn:
                Intent intent2 = new Intent(getActivity(), DetailsActivity.class);
                intent2.setAction(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT);
                intent2.addCategory(Intent.CATEGORY_DEFAULT);
                intent2.putExtra("url", mViewModel.getPrivacyUrl());
                intent2.putExtra("title", "FAQ");
                startActivity(intent2);
                break;
        }
    }

    private void logout() {
        final MaterialDialog progressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait...")
                .progress(true, 0)
                .build();
        progressDialog.show();

        mSubscription = ApiHandler.getInstance().getServices().logout(Utils.getSavedUserIdInSharedPref(getActivity()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DeleteGameResponse>() {
                    @Override
                    public void call(DeleteGameResponse deleteGameResponse) {
                        progressDialog.dismiss();
                        if (deleteGameResponse.getResult().getSuccess() == 1) {
                            if (isFacebookLoggedIn()) {
                                LoginManager.getInstance().logOut();
                            }

                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.deleteAll();
                                }
                            });

                            PreferenceHelper.getInstance(getActivity()).setIsUserLogged(false);
                            startActivity(new Intent(getActivity(), SplashActivity.class));
                            getActivity().finish();
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        progressDialog.dismiss();
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                });
    }

    private boolean isGoogleLoggedIn() {
        return false;
    }

    public boolean isFacebookLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }


    ArrayList<String> filePaths = new ArrayList<>();

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        } else {
            FilePickerBuilder.getInstance().setMaxCount(1)
                    .setSelectedFiles(filePaths)
                    .setActivityTheme(R.style.AppTheme)
                    .enableVideoPicker(false)
                    .pickPhoto(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.AppTheme)
                            .pickPhoto(this);
                } else {
                    Toast.makeText(getActivity(), "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            filePaths = new ArrayList<>();
            filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
            File file = new File(filePaths.get(0));
            Picasso.with(getActivity()).load(file).resize(100, 100)
                    .into(profilePictureImg);

            profilePictureImg.invalidate();
            profilePictureInsTxt.setVisibility(View.GONE);
            profilePictureImg.setVisibility(View.VISIBLE);
        }
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                birthdayTxt.setText(getMonthName(i1) + " " + i + ", " + i2);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private String getMonthName(int month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";

            default:
                return "";
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                boolean allValuesValid = true;

                if (TextUtils.isEmpty(nameEdt.getText().toString().trim())) {
                    nameEdt.setError(getString(R.string.edittext_error_blank));
                    allValuesValid = false;
                }

                if (TextUtils.isEmpty(usernameEdt.getText().toString().trim())) {
                    usernameEdt.setError(getString(R.string.edittext_error_blank));
                    allValuesValid = false;
                }

                if (TextUtils.isEmpty(emailEdt.getText().toString().trim())) {
                    emailEdt.setError(getString(R.string.edittext_error_blank));
                    allValuesValid = false;
                }

                if (!TextUtils.isEmpty(emailEdt.getText().toString().trim()) && !mViewModel.isEmailValid(emailEdt.getText().toString().trim())) {
                    emailEdt.setError(getString(R.string.edittext_error_emailformat));
                    allValuesValid = false;
                }

                if (!TextUtils.isEmpty(oldPasswordEdt.getText().toString())) {
                    if (TextUtils.isEmpty(newPasswordEdt.getText().toString().trim())) {
                        newPasswordEdt.setError(getString(R.string.edittext_error_blank));
                        allValuesValid = false;
                    }

                    if (!TextUtils.isEmpty(confirmPasswordEdt.getText().toString().trim()) && confirmPasswordEdt.getText().toString().trim().length() < 6) {
                        confirmPasswordEdt.setError(getString(R.string.edittext_error_passwordlength));
                        allValuesValid = false;
                    }
                }

                if (TextUtils.isEmpty(phoneEdt.getText().toString().trim())) {
                    phoneEdt.setError(getString(R.string.edittext_error_blank));
                    allValuesValid = false;
                }

                if (TextUtils.isEmpty(birthdayTxt.getText().toString())) {
                    allValuesValid = false;
                }

                String birthday = null;
                if (allValuesValid) {

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM yyyy, dd");
                    try {
                        Date before = simpleDateFormat.parse(birthdayTxt.getText().toString());
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        birthday = simpleDateFormat1.format(before);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (filePaths != null && filePaths.size() > 0) {
                        mAvatarPath = filePaths.get(0);
                    }

                    mViewModel.updateUser(mAvatarPath, usernameEdt.getText().toString(), newPasswordEdt.getText().toString(),
                            emailEdt.getText().toString(), birthday, tshirtEdt.getText().toString(), footSpn.getSelectedItemPosition() == 0 ? "left" : "right",
                            nameEdt.getText().toString(), phoneEdt.getText().toString(), genderSpn.getSelectedItemPosition() == 0 ? "female" : "male",
                            heightEdt.getText().toString(), mViewModel.geCountryId(countrySpn.getSelectedItemPosition()), mViewModel.gePositionId(positionSpn.getSelectedItemPosition()));
                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Please fill all required field")
                            .positiveText("OK").show();
                }

                break;
        }
        return true;

    }

    @Override
    public void showError(String error) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(error)
                .positiveText("OK").show();
    }
}

