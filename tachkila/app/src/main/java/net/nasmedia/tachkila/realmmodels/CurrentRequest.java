
package net.nasmedia.tachkila.realmmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.test.Pivot;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CurrentRequest extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("country_id")
    @Expose
    private int countryId;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("losses")
    @Expose
    private int losses;
    @SerializedName("win")
    @Expose
    private int win;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("position_id")
    @Expose
    private int positionId;
    @SerializedName("favorite_number")
    @Expose
    private int favoriteNumber;
    @SerializedName("perfect_foot")
    @Expose
    private String perfectFoot;
    @SerializedName("isAdmin")
    @Expose
    private int isAdmin;
    @SerializedName("role_id")
    @Expose
    private int roleId;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return The countryId
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     * @param countryId The country_id
     */
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    /**
     * @return The sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex The sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @return The losses
     */
    public int getLosses() {
        return losses;
    }

    /**
     * @param losses The losses
     */
    public void setLosses(int losses) {
        this.losses = losses;
    }

    /**
     * @return The win
     */
    public int getWin() {
        return win;
    }

    /**
     * @param win The win
     */
    public void setWin(int win) {
        this.win = win;
    }

    /**
     * @return The points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points The points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return The positionId
     */
    public int getPositionId() {
        return positionId;
    }

    /**
     * @param positionId The position_id
     */
    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    /**
     * @return The favoriteNumber
     */
    public int getFavoriteNumber() {
        return favoriteNumber;
    }

    /**
     * @param favoriteNumber The favorite_number
     */
    public void setFavoriteNumber(int favoriteNumber) {
        this.favoriteNumber = favoriteNumber;
    }

    /**
     * @return The perfectFoot
     */
    public String getPerfectFoot() {
        return perfectFoot;
    }

    /**
     * @param perfectFoot The perfect_foot
     */
    public void setPerfectFoot(String perfectFoot) {
        this.perfectFoot = perfectFoot;
    }

    /**
     * @return The isAdmin
     */
    public int getIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin The isAdmin
     */
    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return The roleId
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * @param roleId The role_id
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     * @return The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType The deviceType
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return The deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * @param deviceToken The deviceToken
     */
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    /**
     * @return The rememberToken
     */
    public String getRememberToken() {
        return rememberToken;
    }

    /**
     * @param rememberToken The remember_token
     */
    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    /**
     * @return The active
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The pivot
     */
    public Pivot getPivot() {
        return pivot;
    }

    /**
     * @param pivot The pivot
     */
    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }

}
