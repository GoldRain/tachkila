package net.nasmedia.tachkila.widgets;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;



import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Mahmoud Galal on 20/11/2016.
 */

public class FieldPlayerLayout extends LinearLayout {

    Context mContext;
    GamePlayersWithCaptain mPlayer;
    @BindView(R.id.userImage)
    CircleImageView userImage;
    @BindView(R.id.userName)
    CustomTextView userName;
    @BindView(R.id.userContainer)
    LinearLayout userContainer;


    public FieldPlayerLayout(Context context) {
        super(context);
    }

    public FieldPlayerLayout(Context context, GamePlayersWithCaptain player) {
        super(context);
        mContext = context;
        mPlayer = player;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_field_player, this);
        ButterKnife.bind(this, v);
        userImage.setBorderColor(mContext.getResources().getColor(android.R.color.transparent));

//        initialize();
    }

    private void initialize() {
        userName.setText(mPlayer.getUserName());
        if (mPlayer.getAvatar() != null && !TextUtils.isEmpty(mPlayer.getAvatar())) {
            Constants.getPicResizeUrl(userImage, Constants.URL_AVATAR_PIC + mPlayer.getAvatar());
//            Glide.with(mContext)
//                    .load(Constants.getPicResizeUrl(userImage, Constants.URL_AVATAR_PIC + mPlayer.getAvatar()))
//                    .placeholder(R.drawable.ic_user_default)
//                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
//                    .into(userImage);
        }

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != 0)
            initialize();
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener l) {
        super.setOnFocusChangeListener(l);
//        initialize();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
//        initialize();
    }

    public void changeStroke(int color) {
        userImage.setBorderColor(color);
    }
}
