package net.nasmedia.tachkila.interfaces;

import android.view.View;

/**
 * Created by Mahmoud Galal on 12/02/2017.
 */

public interface OnItemClickListener {
    void onItemClick(int position, View view);
}
