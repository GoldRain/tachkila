package net.nasmedia.tachkila.services.response;

import net.nasmedia.tachkila.services.response.viewgame.ViewGameData;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class FinishedGameResponse {

    FinishedGameData result;

    public FinishedGameData getResult() {
        return result;
    }

    public void setResult(FinishedGameData result) {
        this.result = result;
    }
}
