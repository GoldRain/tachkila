package net.nasmedia.tachkila;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.utils.MyNotificationReceivedHandler;
import net.nasmedia.tachkila.utils.NotificationOpenedHandler;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Mahmoud Galal on 4/3/2016.
 */
public class TachkilaApplication extends Application {

    private static TachkilaApplication instance;

    public TachkilaApplication() {
        instance = this;
    }

    public static synchronized TachkilaApplication getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        OneSignal.startInit(this).setNotificationOpenedHandler(new NotificationOpenedHandler(this)).setNotificationReceivedHandler(new MyNotificationReceivedHandler(this)).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None).init();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }


}
