package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.searchuser.SearchUserNameResponse;
import net.nasmedia.tachkila.services.response.searchuser.UserSearch;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import retrofit2.Call;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 11/11/2016.
 */

public class SearchUserViewModel {

    Context mContext;
    ApiHandler mApiHandler;
    OnDataLoaded mListener;
    List<UserSearch> mUserResultList;

    public SearchUserViewModel(Context context, OnDataLoaded listener) {
        this.mContext = context;
        this.mApiHandler = ApiHandler.getInstance();
        this.mListener = listener;
    }

    public void onStart() {

    }

    public void onStop() {
    }

    public void startSearch(String userName) {
        mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
        mApiHandler.getServices().searchByUsername(userName, String.valueOf(Utils.getSavedUserIdInSharedPref(mContext)))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SearchUserNameResponse>() {
                    @Override
                    public void call(SearchUserNameResponse searchUserNameResponse) {
                        if (searchUserNameResponse.getResult().getData().size() > 0) {
                            mUserResultList = searchUserNameResponse.getResult().getData();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }

    public List<UserSearch> getUserResultList() {
        return mUserResultList;
    }


}
