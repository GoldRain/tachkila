package net.nasmedia.tachkila.services.response;

import net.nasmedia.tachkila.realmmodels.Message;

/**
 * Created by galal on 02/06/17.
 */

public class PostMessageRespone {

    PostMessageResult result;

    public PostMessageResult getResult() {
        return result;
    }

    public void setResult(PostMessageResult result) {
        this.result = result;
    }

    public class PostMessageResult {
        int success;
        Message data;

        public int getSuccess() {
            return success;
        }

        public void setSuccess(int success) {
            this.success = success;
        }

        public Message getData() {
            return data;
        }

        public void setData(Message data) {
            this.data = data;
        }
    }
}
