package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.realmmodels.GameType;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.creategames.CreateGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by mahmoudgalal on 10/20/16.
 */

public class NewMatchViewModel {

    Context mContext;
    Realm mRealm;
    OnDataLoaded mListener;
    OnError mErrorListener;
    ActiveGame mActiveGame;

    public NewMatchViewModel(Context context, OnDataLoaded listener, OnError errorListener) {
        mContext = context;
        mListener = listener;
        mErrorListener = errorListener;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
    }

    public void onStop() {
        mRealm.close();
    }

    public void createMatch(String dateTime, int gameTypeId, final int groupId, int teamAColorId, int teamBColorId, String location, double lng, double lat, String notes) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            ApiHandler.getInstance().getServices().createGame(dateTime,
                    location, lat, lng, groupId, Utils.getSavedUserIdInSharedPref(mContext), gameTypeId, teamAColorId, teamBColorId, notes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<CreateGameResponse>() {
                        @Override
                        public void call(final CreateGameResponse createGameResponse) {
                            mActiveGame = createGameResponse.getResult().getData();
                            Realm realm = Realm.getDefaultInstance();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.where(Group.class).equalTo("id", groupId).findFirst().getActive_games().add(createGameResponse.getResult().getData());
                                }
                            });
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                try {
                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                    String errorString = "";
                                    for (String string : response.getResult().getError()) {
                                        errorString += string + "\n";
                                    }                                    mErrorListener.showError(errorString);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            } else {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }
                    });
    }

    public List<Color> getChosserColors() {
        return mRealm.copyFromRealm(mRealm.where(Color.class).findAll());
    }

    public int getChossenColorId(int position) {
        return mRealm.copyFromRealm(mRealm.where(Color.class).findAll()).get(position).getId();
    }

    public int getGameTypeId(int position) {
        return mRealm.copyFromRealm(mRealm.where(GameType.class).findAll()).get(position).getId();
    }

    public ArrayList<String> getNumOfPlayersNames() {
        ArrayList<String> numOfPlayersNamesList = new ArrayList<>();
        List<GameType> gameTypes = mRealm.copyFromRealm(mRealm.where(GameType.class).findAll());
        for (GameType gameType : gameTypes) {
            numOfPlayersNamesList.add(gameType.getName());
        }
        return numOfPlayersNamesList;
    }

    public ArrayList<String> getGroupsNames() {
        ArrayList<String> groupsNameList = new ArrayList<>();
        List<Group> currentUserGroups = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups();
        for (Group group : currentUserGroups) {
            groupsNameList.add(group.getName());
        }
        return groupsNameList;
    }

    public int getGroupId(int position) {
        return mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups().get(position).getId();
    }

    public int getGroupPositionByGroupId(int groupId) {
        ArrayList<String> groupsNameList = new ArrayList<>();
        List<Group> currentUserGroups = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups();

        for (int i = 0; i < currentUserGroups.size(); i++) {
            if (currentUserGroups.get(i).getId() == groupId) {
                return i;
            }
        }
        return 0;
    }

    public String getUserName() {
        return mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getName();
    }

    public ActiveGame getActiveGame() {
        return mActiveGame;
    }

    public interface OnError {
        void showError(String error);
    }

}
