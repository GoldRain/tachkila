package net.nasmedia.tachkila.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;

import org.json.JSONObject;

import io.realm.Realm;

/**
 * Created by Mahmoud Galal on 24/09/2017.
 */

public class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    private static final String TAG = "MyNotReceivedHandler";
    Context context;

    public MyNotificationReceivedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationReceived(OSNotification notification) {
        try {
            JSONObject jsonObject = notification.payload.additionalData;
            Log.d("message==receiver=>", jsonObject.toString());
            if (jsonObject.has("message")) {

                final Message message = new Gson().fromJson(jsonObject.toString(), Message.class);
                Realm realm = Realm.getDefaultInstance(); // opens "myrealm.realm"

                Log.d("messsage===>", message.getType());

                try {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(message);
                            Log.d("messages_receiver+", "message recieved");
                        }
                    });
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            UnReadMessage unReadMessage = new UnReadMessage();
                            unReadMessage.setGroup_id(message.getGroup_id());
                            unReadMessage.setAndroid_id(message.getAndroid_id());
                            realm.copyToRealmOrUpdate(unReadMessage);
                        }
                    });
                } finally {
                    realm.close();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
