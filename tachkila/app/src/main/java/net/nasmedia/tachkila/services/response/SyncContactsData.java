package net.nasmedia.tachkila.services.response;

import net.nasmedia.tachkila.realmmodels.test.User;

import java.util.List;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class SyncContactsData {

    int success;
    List<User> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<User> getData() {
        return data;
    }

    public void setData(List<User> data) {
        this.data = data;
    }
}
