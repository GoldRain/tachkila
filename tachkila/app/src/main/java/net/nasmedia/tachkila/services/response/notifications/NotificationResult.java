package net.nasmedia.tachkila.services.response.notifications;


import net.nasmedia.tachkila.realmmodels.test.User;

/**
 * Created by Mahmoud Galal on 7/14/2016.
 */
public class NotificationResult {

    int success;
    User data;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}
