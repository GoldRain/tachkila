package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.BaseAdapter;

import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class MainPlayersFragment extends BaseFragment {

    public static final String ARG_TYPE = "ARG_TYPE";
    public static final String TYPE_PLAYERS = "TYPE_PLAYERS";
    public static final String TYPE_REQUESTES = "TYPE_REQUESTES";
    public static final String TYPE_SUGGESTED = "TYPE_SUGGESTED";


    private String mType;
    BaseAdapter mAdapter;

    public static MainPlayersFragment getInstance(String type) {
        MainPlayersFragment playersFragment = new MainPlayersFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TYPE, type);
        playersFragment.setArguments(bundle);
        return playersFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mType = getArguments().getString(ARG_TYPE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_players, container, false);
        ButterKnife.bind(this, view);

        switch (mType) {
            case TYPE_PLAYERS:
                getChildFragmentManager().beginTransaction().replace(R.id.container, PlayersFragment.getInstance()).commit();
                break;
            case TYPE_REQUESTES:
                getChildFragmentManager().beginTransaction().replace(R.id.container, FollwersPlayersFragment.getInstance()).commit();
                break;
            case TYPE_SUGGESTED:
                getChildFragmentManager().beginTransaction().replace(R.id.container, SuggestsPlayersFragment.getInstance()).commit();
                break;
        }


        return view;
    }

    @Override
    public String getTitle() {
        return null;
    }

}
