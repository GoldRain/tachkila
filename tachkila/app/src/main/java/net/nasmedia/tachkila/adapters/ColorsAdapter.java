package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Mahmoud Galal on 10/5/2016.
 */

public class ColorsAdapter extends ArrayAdapter<Color> {

    LayoutInflater mInflater;
    List<Color> mColors;



    public ColorsAdapter(Context context, List<Color> colors) {
        super(context, 0);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mColors = colors;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = mInflater.inflate(R.layout.item_color_picker, parent, false);

        /***** Get each Model object from Arraylist ********/
        Color color = mColors.get(position);

        LinearLayout colorHolder = (LinearLayout) row.findViewById(R.id.itemColor);
        TextView colorName = (TextView) row.findViewById(R.id.itemColorName);

        colorName.setText(color.getName());
        GradientDrawable background = (GradientDrawable) colorHolder.getBackground();
        background.setColor(android.graphics.Color.parseColor(color.getCode()));

        return row;
    }

    @Override
    public int getCount() {
        return mColors.size();
    }
}
