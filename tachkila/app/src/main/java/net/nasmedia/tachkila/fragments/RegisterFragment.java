package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.RegisterWelcomeActivity;
import net.nasmedia.tachkila.adapters.CountriesSpinnerAdapter;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.register.RegisterResponse;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.WhiteCircularTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by mahmoudgalal on 5/21/16.
 */


public class RegisterFragment extends BaseFragment {

    public static final String ARG_REGISTER_TYPE = "ARG_REGISTER_TYPE";
    public static final String ARG_GOOGLE_ACCOUNT = "ARG_GOOGLE_ACCOUNT";

    @BindView(R.id.uploadBtn)
    Button uploadBtn;
    @BindView(R.id.nameEdt)
    EditText nameEdt;
    @BindView(R.id.userNameEdt)
    EditText userNameEdt;
    @BindView(R.id.emailEdt)
    EditText emailEdt;
    @BindView(R.id.passwordEdt)
    EditText passwordEdt;
    @BindView(R.id.phoneEdt)
    EditText phoneEdt;
    @BindView(R.id.heightEdt)
    EditText heightEdt;
    @BindView(R.id.tshirtNumEdt)
    EditText frontNumEdt;
    @BindView(R.id.countriesSpn)
    Spinner countriesSpn;
    @BindView(R.id.rightFootCheck)
    CheckedTextView rightCheck;
    @BindView(R.id.leftFootCheck)
    CheckedTextView leftCheck;
    @BindView(R.id.maleCheck)
    CheckedTextView maleCheck;
    @BindView(R.id.femaleCheck)
    CheckedTextView femaleCheck;
    @BindView(R.id.profilePictureInsTxt)
    WhiteCircularTextView mProfilePictureInsTxt;
    @BindView(R.id.profilePictureImg)
    ImageView mProfilePictureImg;

    String avatarPath;
    String favFoot;
    String sex;
    String countryId;
    //    RegisterViewModel mModel;
    RegisterType mRegisterType;
    @BindView(R.id.birthDayTxt)
    TextView mBirthDayTxt;
    @BindView(R.id.dayTxt)
    TextView mDayTxt;
    @BindView(R.id.yearTxt)
    TextView mYearTxt;
    @BindView(R.id.birthDayContainer)
    RelativeLayout mBirthDayContainer;
    @BindView(R.id.monthTxt)
    TextView mMonthTxt;
    @BindView(R.id.positionSpn)
    Spinner mPositionSpn;

    private MaterialDialog mProgressDialog;
    DatePickerDialog datePickerDialog;
    private GoogleSignInAccount mGoogleSignInAccount;
    private String mSocialTokenId;
    RealmResults<Position> mPositions;
    RealmResults<Country> mCountries;

    private String mRegId;

    public enum RegisterType {
        NORMAL,
        GOOGLE,
        FACEBOOK
    }

    @Override
    public String getTitle() {
        return null;
    }

    public static RegisterFragment getInstance(RegisterType registerType, GoogleSignInAccount googleSignInAccount) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_REGISTER_TYPE, registerType.ordinal());
        if (googleSignInAccount != null) {
            bundle.putParcelable(ARG_GOOGLE_ACCOUNT, googleSignInAccount);
        }
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        mModel = new RegisterViewModel(getActivity(), this, this);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();

        mRegisterType = RegisterType.values()[getArguments().getInt(ARG_REGISTER_TYPE)];
        if (getArguments().containsKey(ARG_GOOGLE_ACCOUNT)) {
            mGoogleSignInAccount = getArguments().getParcelable(ARG_GOOGLE_ACCOUNT);
        }

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                mRegId = userId;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, null);
        ButterKnife.bind(this, view);

        SpannableStringBuilder cs = new SpannableStringBuilder(getString(R.string.hint_name));
        cs.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        nameEdt.setHint(cs);

        SpannableStringBuilder cs1 = new SpannableStringBuilder(getString(R.string.hint_username));
        cs1.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        userNameEdt.setHint(cs1);

        SpannableStringBuilder cs2 = new SpannableStringBuilder(getString(R.string.hint_mail));
        cs2.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        emailEdt.setHint(cs2);

        SpannableStringBuilder cs3 = new SpannableStringBuilder(getString(R.string.hint_reg_password));
        cs3.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        passwordEdt.setHint(cs3);

        SpannableStringBuilder cs4 = new SpannableStringBuilder(getString(R.string.hint_phone));
        cs4.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        phoneEdt.setHint(cs4);

        SpannableStringBuilder cs5 = new SpannableStringBuilder(getString(R.string.star_Birthday));
        cs5.setSpan(new SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBirthDayTxt.setText(cs5);

        maleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!maleCheck.isChecked()) {
                    maleCheck.setChecked(true);
                    femaleCheck.setChecked(false);
                }
            }
        });

        femaleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!femaleCheck.isChecked()) {
                    maleCheck.setChecked(false);
                    femaleCheck.setChecked(true);
                }
            }
        });

        rightCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!rightCheck.isChecked()) {
                    rightCheck.setChecked(true);
                    leftCheck.setChecked(false);
                }
            }
        });

        leftCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!leftCheck.isChecked()) {
                    rightCheck.setChecked(false);
                    leftCheck.setChecked(true);
                }
            }
        });

        switch (mRegisterType) {
            case FACEBOOK:
                getDataFromFaceBook();
                break;
            case GOOGLE:
                getDataFromGoogle();
                break;
            case NORMAL:

                break;
        }


        return view;
    }

    private void getDataFromGoogle() {
        nameEdt.setText(mGoogleSignInAccount.getDisplayName());
        emailEdt.setText(mGoogleSignInAccount.getEmail());
        mSocialTokenId = mGoogleSignInAccount.getId();
        if (mGoogleSignInAccount.getPhotoUrl() != null && !TextUtils.isEmpty(mGoogleSignInAccount.getPhotoUrl().toString()))
            Picasso.with(getActivity())
                    .load(mGoogleSignInAccount.getPhotoUrl())
                    .into(mProfilePictureImg);
    }


    private void getDataFromFaceBook() {
        nameEdt.setText(String.format("%s %s", Profile.getCurrentProfile().getFirstName(), Profile.getCurrentProfile().getLastName()));
        mSocialTokenId = AccessToken.getCurrentAccessToken().getUserId();
//        Profile.getCurrentProfile().getProfilePictureUri()
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.v("LoginActivity Response ", response.toString());

                        try {
                            emailEdt.setText(object.getString("email"));
//                            birthDayEdt.setText(object.getString("birthday"));
                            Log.d("gender", object.getString("gender"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadCountries();
        loadPositionsName();
    }

    private void loadPositionsName() {

        mRealm.where(Position.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<Position>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Position> positions) {
                        return positions.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<Position>>() {
                    @Override
                    public void call(RealmResults<Position> positions) {
                        if (isAdded()) {
                            mPositions = positions;
                            ArrayList<String> positionsNames = new ArrayList<>();
                            for (Position country : positions) {
                                positionsNames.add(country.getName());
                            }
                            ArrayAdapter<String> positionAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner_text, positionsNames);
                            positionAdapter.setDropDownViewResource(R.layout.item_spinner_text);
                            mPositionSpn.setAdapter(positionAdapter);
                        }
                    }
                });
    }

    private void loadCountries() {
//        unSubscribeFromObservable();
         mRealm.where(Country.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<Country>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Country> countries) {
                        return countries.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<Country>>() {
                    @Override
                    public void call(RealmResults<Country> countries) {
                        if (isAdded()) {
                            mCountries = countries;
                            CountriesSpinnerAdapter adapter1 = new CountriesSpinnerAdapter(getActivity(), R.layout.item_spinner_text, countries);
                            countriesSpn.setAdapter(adapter1);
                            for (Country country : countries) {
                                if (country.getAlpha_2().equalsIgnoreCase("kw")) {
                                    countriesSpn.setSelection(countries.indexOf(country));
                                }
                            }
                        }
                    }
                });
    }

    @OnClick({R.id.profilePictureImg, R.id.profilePictureInsTxt, R.id.uploadBtn, R.id.birthDayContainer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profilePictureImg:
            case R.id.profilePictureInsTxt:
                pickImage();
                break;
            case R.id.uploadBtn:
                uploadData();
                break;
            case R.id.birthDayContainer:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                mDayTxt.setText(i2 + "");
                mYearTxt.setText(i + "");
                mMonthTxt.setText(getMonthName(i1));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    private String getMonthName(int month) {
        switch (month) {
            case 0:
                return "january";
            case 1:
                return "february";
            case 2:
                return "march";
            case 3:
                return "april";
            case 4:
                return "may";
            case 5:
                return "june";
            case 6:
                return "july";
            case 7:
                return "august";
            case 8:
                return "september";
            case 9:
                return "october";
            case 10:
                return "november";
            case 11:
                return "december";

            default:
                return "";
        }
    }

    private int getMonthNumber(String monthName) {
        switch (monthName) {
            case "january":
                return 1;
            case "february":
                return 2;
            case "march":
                return 3;
            case "april":
                return 4;
            case "may":
                return 5;
            case "june":
                return 6;
            case "july":
                return 7;
            case "august":
                return 8;
            case "september":
                return 9;
            case "october":
                return 10;
            case "november":
                return 11;
            case "december":
                return 12;

            default:
                return 1;
        }
    }

    ArrayList<String> filePaths = new ArrayList<>();

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        } else {
            FilePickerBuilder.getInstance().setMaxCount(1)
                    .setSelectedFiles(filePaths)
                    .setActivityTheme(R.style.AppTheme)
                    .enableVideoPicker(false)
                    .pickPhoto(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.AppTheme)
                            .pickPhoto(this);
                } else {
                    Toast.makeText(getActivity(), "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            filePaths = new ArrayList<>();
            filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
            File file = new File(filePaths.get(0));
            Picasso.with(getActivity()).load(file).resize(100, 100)
                    .into(mProfilePictureImg);

            mProfilePictureImg.invalidate();
            mProfilePictureInsTxt.setVisibility(View.GONE);
            mProfilePictureImg.setVisibility(View.VISIBLE);
        }
    }

    private void uploadData() {
        boolean allValuesValid = true;

        if (TextUtils.isEmpty(nameEdt.getText().toString().trim())) {
            nameEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(userNameEdt.getText().toString().trim())) {
            userNameEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(emailEdt.getText().toString().trim())) {
            emailEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (!TextUtils.isEmpty(emailEdt.getText().toString().trim()) && !isEmailValid(emailEdt.getText().toString().trim())) {
            emailEdt.setError(getString(R.string.edittext_error_emailformat));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(passwordEdt.getText().toString().trim())) {
            passwordEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (!TextUtils.isEmpty(passwordEdt.getText().toString().trim()) && passwordEdt.getText().toString().trim().length() < 6) {
            passwordEdt.setError(getString(R.string.edittext_error_passwordlength));
            allValuesValid = false;
        }

        if (TextUtils.isEmpty(phoneEdt.getText().toString().trim())) {
            phoneEdt.setError(getString(R.string.edittext_error_blank));
            allValuesValid = false;
        }

        if (filePaths != null && filePaths.size() > 0) {
            avatarPath = filePaths.get(0);
        }

        if (allValuesValid) {

            performRegistration(avatarPath,
                    userNameEdt.getText().toString().trim(),
                    passwordEdt.getText().toString().trim(),
                    emailEdt.getText().toString().trim(),
                    mYearTxt.getText().toString() + "-" + getMonthNumber(mMonthTxt.getText().toString()) + "-" + mDayTxt.getText().toString(),
                    frontNumEdt.getText().toString().trim(),
                    leftCheck.isChecked() ? "left" : "right",
                    nameEdt.getText().toString().trim(),
                    phoneEdt.getText().toString().trim(),
                    femaleCheck.isChecked() ? "female" : "male",
                    heightEdt.getText().toString().trim(),
                    mRegId,
                    String.valueOf(mCountries.get(countriesSpn.getSelectedItemPosition()).getId()),
                    mRegisterType.name(),
                    mSocialTokenId == null ? "" : mSocialTokenId,
                    String.valueOf(mPositions.get(mPositionSpn.getSelectedItemPosition()).getId()));
        } else {
            DialogsUtils.showError(getActivity(), "Please fill all required field", null);
        }


    }

    private void performRegistration(String avatarPath, String userName, String password, String email, String birthDate,
                                     String favNum, String favFoot, String name, String phone, String sex,
                                     String height, String deviceToken, String countryId, String socialType, String socialId, String positionId) {
        unSubscribeFromObservable();
        mProgressDialog.show();
        File avatarFile = null;
        if (avatarPath != null) {
            avatarFile = new File(avatarPath);
        }


        RequestBody requestFile = null;
        if (avatarFile != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
        }
        RequestBody userNameBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), userName);
        RequestBody emailBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), email);
        RequestBody birthDateBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), birthDate != null ? birthDate : "");
        RequestBody favNumBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), favNum != null ? favNum : "");
        RequestBody favFootBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), favFoot != null ? favFoot : "");
        RequestBody nameBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), name != null ? name : "");
        RequestBody phoneBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), phone != null ? phone : "");
        RequestBody sexBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), sex != null ? sex : "");
        RequestBody deviceType =
                RequestBody.create(
                        MediaType.parse("text/plain"), "2");
        RequestBody heightBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), height != null ? height : "");
        RequestBody deviceTokenBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), deviceToken != null ? deviceToken : "");
        RequestBody countryIdBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), countryId);
        RequestBody socialTypeBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), socialType != null ? socialType : "");
        RequestBody socialKeyBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), socialId != null ? socialId : "");
        RequestBody passwordBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), password);
        RequestBody positionIdBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), positionId);

        mSubscription = ApiHandler.getInstance().getServices().
                registerUser(requestFile, userNameBody, passwordBody, nameBody, emailBody,
                        phoneBody, countryIdBody, sexBody,
                        favFootBody, favNumBody, positionIdBody, heightBody, birthDateBody,
                        deviceType, deviceTokenBody, socialTypeBody, socialKeyBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<RegisterResponse>() {
                    @Override
                    public void call(final RegisterResponse registerResponse) {
                        mProgressDialog.dismiss();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(registerResponse.getResult().getData());
                            }
                        });

                        Utils.saveUserIdToSharedPref(getActivity(), registerResponse.getResult().getData().getId());
                        PreferenceHelper.getInstance(getActivity()).setIsUserLogged(true);

                        new MaterialDialog.Builder(getActivity())
                                .title("Success")
                                .content("Thank you for registration")
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivity(new Intent(getActivity(), RegisterWelcomeActivity.class));
                                        getActivity().finish();
                                    }
                                })
                                .show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                DialogsUtils.showError(getActivity(), errorString, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                });
    }

    public boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
