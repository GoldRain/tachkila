package net.nasmedia.tachkila.services.response.settings;

import net.nasmedia.tachkila.realmmodels.Message;

import java.util.List;

/**
 * Created by galal on 01/06/17.
 */

public class Chats {

    int count_chat_notifications;
    List<Message> messages;

    public int getCount_chat_notifications() {
        return count_chat_notifications;
    }

    public void setCount_chat_notifications(int count_chat_notifications) {
        this.count_chat_notifications = count_chat_notifications;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
