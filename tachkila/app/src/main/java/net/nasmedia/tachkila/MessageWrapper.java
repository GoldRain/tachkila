package net.nasmedia.tachkila;

import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.realmmodels.test.User_;

/**
 * Created by galal on 06/06/17.
 */

public class MessageWrapper {

    int id;
    String type;
    String message;
    int group_id;
    int user_id;
    String created_at;
    String updated_at;
    User_ otherUser;
    User myUser;
    boolean looding;

    public boolean isLooding() {
        return looding;
    }

    public void setLooding(boolean looding) {
        this.looding = looding;
    }

    public void setMessage(Message message) {
        id = message.getId();
        type = message.getType();
        group_id=  message.getGroup_id();
        this.message = message.getMessage();
        user_id = message.getUser_id();
        created_at = message.getCreated_at();
        looding = message.isLooding();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public User_ getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(User_ otherUser) {
        this.otherUser = otherUser;
    }

    public User getMyUser() {
        return myUser;
    }

    public void setMyUser(User myUser) {
        this.myUser = myUser;
    }
}
