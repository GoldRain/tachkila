package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import net.nasmedia.tachkila.MessageWrapper;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by galal on 04/06/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    private final static int TYPE_USER_TEXT = 0;
    private final static int TYPE_USER_IMG = 1;
    private final static int TYPE_OTHER_USER_TEXT = 2;
    private final static int TYPE_OTHER_USER_IMG = 3;
    private final static int TYPE_SYSTEM_TEXT = 4;

    Context mContext;
    List<MessageWrapper> mItems;
    OnImageClicked listener;

    public void setListener(OnImageClicked listener) {
        this.listener = listener;
    }

    SimpleDateFormat inputSimpleDate;
    SimpleDateFormat outputSimpleDate;
    SimpleDateFormat headerOutputSimpleDate;

    public MessagesAdapter(Context context) {
        mContext = context;
        mItems = Collections.EMPTY_LIST;

        inputSimpleDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        outputSimpleDate = new SimpleDateFormat("hh:mm aaa", Locale.US);
        headerOutputSimpleDate = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
    }

    public void setItems(List<MessageWrapper> messages) {
        mItems = messages;
        notifyDataSetChanged();
    }

    public void addItem(MessageWrapper messageWrapper) {
        mItems.add(messageWrapper);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       /* if (viewType == TYPE_SYSTEM_TEXT){

            return createSystemText(parent);

        } else */

       if (viewType == TYPE_USER_TEXT) {
            return createUserText(parent);
        } else if (viewType == TYPE_USER_IMG) {
            return createUserImg(parent);
        } else if (viewType == TYPE_OTHER_USER_TEXT) {
            return createOtherUserText(parent);
        } else if (viewType == TYPE_OTHER_USER_IMG) {
            return createOtherUserImg(parent);
        }
        return null;
    }

    private RecyclerView.ViewHolder createSystemText(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_other_user_message_img, parent, false);
        return new OtherUserImageViewHolder(view);
    }

    private RecyclerView.ViewHolder createOtherUserImg(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_other_user_message_img, parent, false);
        return new OtherUserImageViewHolder(view);
    }

    private RecyclerView.ViewHolder createOtherUserText(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_other_user_message_text, parent, false);
        return new OtherUserMessageViewHolder(view);
    }

    private RecyclerView.ViewHolder createUserImg(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_message_img, parent, false);
        return new UserImageViewHolder(view);
    }

    private RecyclerView.ViewHolder createUserText(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_message_text, parent, false);
        return new UserMessageViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MessageWrapper message = mItems.get(position);

      if (holder.getItemViewType() == TYPE_USER_TEXT) {
            UserMessageViewHolder userMessageViewHolder = (UserMessageViewHolder) holder;
            userMessageViewHolder.messageTxt.setText(message.getMessage());
            if (message.isLooding()) {
                userMessageViewHolder.progressBar.setVisibility(View.VISIBLE);
            } else {
                userMessageViewHolder.progressBar.setVisibility(View.GONE);
            }
            try {
                userMessageViewHolder.date.setText(
                        outputSimpleDate.format(inputSimpleDate.parse(message.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else if (holder.getItemViewType() == TYPE_USER_IMG) {
            UserImageViewHolder userImageViewHolder = (UserImageViewHolder) holder;
            if (message.isLooding()) {
                userImageViewHolder.progressBar.setVisibility(View.VISIBLE);
                Picasso.with(mContext)
                        .load(new File(message.getMessage()))
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(userImageViewHolder.image);
            } else {
                userImageViewHolder.progressBar.setVisibility(View.GONE);
                Constants.getPicResizeUrl(userImageViewHolder.image, Constants.URL_CHATS_PIC + message.getMessage());
            }
            try {
                userImageViewHolder.date.setText(
                        outputSimpleDate.format(inputSimpleDate.parse(message.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (holder.getItemViewType() == TYPE_OTHER_USER_TEXT) {

            OtherUserMessageViewHolder otherUserMessageViewHolder = (OtherUserMessageViewHolder) holder;
            otherUserMessageViewHolder.username.setText(message.getOtherUser().getName());
            try {
                otherUserMessageViewHolder.date.setText(
                        outputSimpleDate.format(inputSimpleDate.parse(message.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            otherUserMessageViewHolder.userImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.imageClicked(message.getUser_id());
                }
            });
            Constants.getPicResizeUrl(otherUserMessageViewHolder.userImg, Constants.URL_AVATAR_PIC + message.getOtherUser().getAvatar());
            otherUserMessageViewHolder.messageTxt.setText(message.getMessage());
        }
        else if (holder.getItemViewType() == TYPE_OTHER_USER_IMG) {
            OtherUserImageViewHolder otherUserImageViewHolder = (OtherUserImageViewHolder) holder;
            otherUserImageViewHolder.username.setText(message.getOtherUser().getName());
            try {
                otherUserImageViewHolder.date.setText(
                        outputSimpleDate.format(inputSimpleDate.parse(message.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            otherUserImageViewHolder.userImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.imageClicked(message.getUser_id());
                }
            });
            Constants.getPicResizeUrl(otherUserImageViewHolder.userImg, Constants.URL_AVATAR_PIC + message.getOtherUser().getAvatar());
            Constants.getPicResizeUrl(otherUserImageViewHolder.image, Constants.URL_CHATS_PIC + message.getMessage());
            otherUserImageViewHolder.date.setText(message.getCreated_at());
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position).getUser_id() == Utils.getSavedUserIdInSharedPref(mContext)) {
            if (mItems.get(position).getType().equalsIgnoreCase("text")) {
                return TYPE_USER_TEXT;
            } else {
                return TYPE_USER_IMG;
            }
        } else {
            if (mItems.get(position).getType().equalsIgnoreCase("text")) {
                return TYPE_OTHER_USER_TEXT;
            } else {
                return TYPE_OTHER_USER_IMG;
            }
        }
    }

    @Override
    public long getHeaderId(int position) {
        MessageWrapper message = mItems.get(position);
        try {
            return headerOutputSimpleDate.parse(headerOutputSimpleDate.format(inputSimpleDate.parse(message.getCreated_at()))).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_header, parent, false);
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessageWrapper messageWrapper = mItems.get(position);
        HeaderViewHolder viewHolder = (HeaderViewHolder) holder;

        try {
            viewHolder.txt.setText(headerOutputSimpleDate.format(inputSimpleDate.parse(messageWrapper.getCreated_at())));
        } catch (ParseException e) {
            e.printStackTrace();
            viewHolder.txt.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static class UserMessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.messageTxt)
        CustomTextView messageTxt;
        @BindView(R.id.date)
        CustomTextView date;
        @BindView(R.id.progress)
        ProgressBar progressBar;

        UserMessageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class UserImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.date)
        CustomTextView date;
        @BindView(R.id.progress)
        ProgressBar progressBar;

        UserImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class OtherUserMessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.messageTxt)
        CustomTextView messageTxt;
        @BindView(R.id.date)
        CustomTextView date;
        @BindView(R.id.userImg)
        CircleImageView userImg;
        @BindView(R.id.username)
        CustomTextView username;

        OtherUserMessageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class OtherUserImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.date)
        CustomTextView date;
        @BindView(R.id.userImg)
        CircleImageView userImg;
        @BindView(R.id.username)
        CustomTextView username;

        OtherUserImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.txt)
        TextView txt;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface OnImageClicked {
        void imageClicked(int userId);
    }
}
