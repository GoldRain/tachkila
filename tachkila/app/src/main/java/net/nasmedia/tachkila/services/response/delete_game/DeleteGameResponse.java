package net.nasmedia.tachkila.services.response.delete_game;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mahmoud Galal on 21/11/2016.
 */

public class DeleteGameResponse {

    @SerializedName("result")
    DeleteGameResult result;

    public DeleteGameResult getResult() {
        return result;
    }

    public void setResult(DeleteGameResult result) {
        this.result = result;
    }
}
