package net.nasmedia.tachkila.services.response.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.test.User;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class RegisterResult {
    @SerializedName("success")
    @Expose
    int success;
    @SerializedName("data")
    @Expose
    User data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }
}
