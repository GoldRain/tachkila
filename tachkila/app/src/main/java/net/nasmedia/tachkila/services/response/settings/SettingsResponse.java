
package net.nasmedia.tachkila.services.response.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsResponse {

    @SerializedName("result")
    @Expose
    private SettingsResult settingsResult;

    /**
     * 
     * @return
     *     The settingsResult
     */
    public SettingsResult getSettingsResult() {
        return settingsResult;
    }

    /**
     * 
     * @param settingsResult
     *     The settingsResult
     */
    public void setSettingsResult(SettingsResult settingsResult) {
        this.settingsResult = settingsResult;
    }

}
