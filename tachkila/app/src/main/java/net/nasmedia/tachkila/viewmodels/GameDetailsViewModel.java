package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.Player;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.delete_game.DeleteGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.utils.GamePlayersWithCaptain;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by mahmoudgalal on 11/2/16.
 */

public class GameDetailsViewModel {

    private Context mContext;
    private OnDataLoaded mListener;
    private OnDataLoaded mDeleteGameListener;
    private OnDataLoaded mSaveChangesListener;
    private ApiHandler mApiHandler;
    Realm mRealm;
    private int mGameId;
    private ActiveGame mActiveGame;
    private NewMatchViewModel.OnError mErrorListener;

    public GameDetailsViewModel(Context context, int gameId, OnDataLoaded listener, OnDataLoaded deleteGameListener, OnDataLoaded saveChangesListener, NewMatchViewModel.OnError errorListener) {
        this.mContext = context;
        this.mGameId = gameId;
        this.mListener = listener;
        this.mApiHandler = ApiHandler.getInstance();
        this.mDeleteGameListener = deleteGameListener;
        this.mSaveChangesListener = saveChangesListener;
        this.mErrorListener = errorListener;
    }

    public void onStart() {
        Log.d("Log", "GameId= " + mGameId + " UserId= " + Utils.getSavedUserIdInSharedPref(mContext));
        mRealm = Realm.getDefaultInstance();
        mApiHandler.getServices().viewGame(mGameId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ViewGameResponse>() {
                    @Override
                    public void call(ViewGameResponse viewGameResponse) {
                        mActiveGame = viewGameResponse.getResult().getData();
                        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                mErrorListener.showError(errorString);
                            } catch (IOException e) {
                                e.printStackTrace();
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }

    public void onStop() {
        if (mRealm != null)
            mRealm.close();
    }

    public ActiveGame getActiveGame() {
        return mActiveGame;
    }

    public String getGroupName() {
        Group group = mRealm.where(Group.class).equalTo("id", mActiveGame.getGroupId()).findFirst();
        if (group != null)
            return group.getName();
        else
            return "";
    }

    public String getGameType() {
        return mActiveGame.getType().getName();
    }

    public String getCaptineName() {
        return "Captain: " + mActiveGame.getCaptain().getName();
    }

    public String getDate() throws ParseException {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat toFormat = new SimpleDateFormat("EEE, MMM d, yyyy");

        return toFormat.format(fromFormat.parse(mActiveGame.getTimeOfGame()));
    }

    public String getTime() throws ParseException {
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat toFormat = new SimpleDateFormat("hh:mm a");

        return toFormat.format(fromFormat.parse(mActiveGame.getTimeOfGame()));
    }

    public String getLocation() {
        return mActiveGame.getLocation();
    }

    public int getTeamAColor() {
        return Color.parseColor(mActiveGame.getTeamColor().getCode());
    }

    public int getTeamBColor() {
        return Color.parseColor(mActiveGame.getTeam2Color().getCode());
    }

    public List<GamePlayersWithCaptain> getGamePlayers() {
        List<GamePlayersWithCaptain> gamePlayersWithCaptainList = new ArrayList<>();

        for (Player player : mActiveGame.getPlayers()) {
            GamePlayersWithCaptain gamePlayersWithCaptain1 = new GamePlayersWithCaptain();
            gamePlayersWithCaptain1.setAvatar(player.getUser().getAvatar());
            gamePlayersWithCaptain1.setUserName(player.getUser().getUsername());
            gamePlayersWithCaptain1.setId(player.getUser().getId());
            gamePlayersWithCaptain1.setPositionX(player.getPosition_x());
            gamePlayersWithCaptain1.setPositionY(player.getPosition_y());
            gamePlayersWithCaptain1.setCaptineId(mActiveGame.getCaptainId());
            gamePlayersWithCaptainList.add(gamePlayersWithCaptain1);
        }

        return gamePlayersWithCaptainList;
    }

    public boolean isMyPlayerInField() {
        for (Player player : mActiveGame.getPlayers()) {
            if (player.getUser().getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
                return true;
            }
        }

        return false;
    }

    public GamePlayersWithCaptain getMyPlayer() {
        GamePlayersWithCaptain gamePlayersWithCaptain = new GamePlayersWithCaptain();
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();

        gamePlayersWithCaptain.setAvatar(user.getAvatar());
        gamePlayersWithCaptain.setUserName(user.getUsername());
        gamePlayersWithCaptain.setId(user.getId());
        realm.close();
        return gamePlayersWithCaptain;
    }

    public List<My_current_friend> myFriendsList() {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
        List<My_current_friend> my_current_friends = realm.copyFromRealm(user.getMy_current_friends());
        realm.close();
        return my_current_friends;
    }

    public List<Player> getPlayers() {
        return mActiveGame.getPlayers();
    }

    public void joinGame(int positionX, int positionY, int team) {
        mApiHandler.getServices().joinGame(team, mGameId, Utils.getSavedUserIdInSharedPref(mContext), positionX, positionY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ViewGameResponse>() {
                    @Override
                    public void call(final ViewGameResponse viewGameResponse) {
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                           realm.copyToRealmOrUpdate(viewGameResponse.getResult().getData());
                            }
                        });
                        mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                mErrorListener.showError(errorString);
                            } catch (IOException e) {
                                e.printStackTrace();
                                mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        } else {
                            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }

    public void saveChanges(int positionX, int positionY, int team) {
        mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.START);
        if (positionX != 0 && positionY != 0) {
            joinGame(positionX, positionY, team);
        } else {
            leaveGame();
        }
    }

    public void deleteGame() {
        mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.START);
        mApiHandler.getServices().deleteGame(mGameId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DeleteGameResponse>() {
                    @Override
                    public void call(DeleteGameResponse deleteGameResponse) {
                        if (mActiveGame.getCaptain().getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
                            Realm realm = Realm.getDefaultInstance();
                            final ActiveGame activeGame = realm.where(ActiveGame.class).equalTo("id", mActiveGame.getId()).findFirst();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    activeGame.deleteFromRealm();
                                }
                            });
                            realm.close();
                        }
                        mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                mErrorListener.showError(errorString);
                            } catch (IOException e) {
                                e.printStackTrace();
                                mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        } else {
                            mDeleteGameListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }

    public void leaveGame() {
        mApiHandler.getServices().leaveGame(mGameId, Utils.getSavedUserIdInSharedPref(mContext))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ViewGameResponse>() {
                    @Override
                    public void call(final ViewGameResponse viewGameResponse) {
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(viewGameResponse.getResult().getData());
                            }
                        });
                        mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                mErrorListener.showError(errorString);
                            } catch (IOException e) {
                                e.printStackTrace();
                                mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        } else {
                            mSaveChangesListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }

}
