package net.nasmedia.tachkila.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mahmoud Galal on 18/10/2016.
 */

public class PreferenceHelper {

    private static final String USER_LOGGED = "USER_LOGGED";
    private static final String USER_FIRST_LOGIN = "USER_FIRST_LOGIN";
    private static final String NOTIFICATION_SOUND = "NOTIFICATION_SOUND";


    private SharedPreferences mSharedPreference;
    private static PreferenceHelper mInstance;

    private PreferenceHelper(Context context){
        mSharedPreference = context.getSharedPreferences(Constants.APP_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static PreferenceHelper getInstance(Context context){
        if (mInstance == null){
            mInstance = new PreferenceHelper(context);
        }

        return mInstance;
    }

    public void setIsUserLogged(boolean userLogged){
        mSharedPreference.edit().putBoolean(USER_LOGGED, userLogged).apply();
    }

    public boolean isUserLogged(){
        return mSharedPreference.getBoolean(USER_LOGGED, false);
    }

    public void setIsNotificationSoundEnabled(boolean isNotificationSoundEnabled){
        mSharedPreference.edit().putBoolean(NOTIFICATION_SOUND, isNotificationSoundEnabled).apply();
    }

    public boolean isNotificationSoundEnabled(){
        return mSharedPreference.getBoolean(NOTIFICATION_SOUND, true);
    }
}
