package net.nasmedia.tachkila.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.MatchHistoryAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.Finished_game;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.response.FinishedGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmObject;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/19/2016.
 */
public class MatchHistoryFragment extends BaseFragment implements OnItemClickListener {


    @BindView(R.id.matchList)
    RecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;


    //    MatchHistoryViewModel mViewModel;
    MatchHistoryAdapter mAdapter;

    int mGroupId;

    private MaterialDialog mProgressDialog;


    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";

    public static MatchHistoryFragment getInstance(int groupId) {
        MatchHistoryFragment fragment = new MatchHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt(ARG_GROUP_ID, 0);

        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content("Please wait ...")
                .progress(true, 0)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_match_history, container, false);
        ButterKnife.bind(this, view);

        setupRecyclerView();
        setupPullToRefresh();


        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), R.dimen.matchHistoryRecyclerViewVerticalSpace);
        mRecyclerView.addItemDecoration(itemDecoration);
        mAdapter = new MatchHistoryAdapter(getActivity());
        mAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        loadHistory();
    }

    private void loadHistory() {
        unSubscribeFromObservable();
        if (mGroupId == 0) {
            mSubscription = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(getActivity())).findFirstAsync().asObservable()
                    .filter(new Func1<RealmObject, Boolean>() {
                        @Override
                        public Boolean call(RealmObject realmObject) {
                            return realmObject.isLoaded();
                        }
                    }).subscribe(new Action1<RealmObject>() {
                        @Override
                        public void call(RealmObject realmObject) {
                            User user = mRealm.copyFromRealm((User) realmObject);
                            List<Finished_game> games = new ArrayList<>();
                            if (user.getGroups() != null && user.getGroups().size() > 0) {
                                for (Group group : user.getGroups()) {
                                    if (group.getFinished_games() != null && group.getFinished_games().size() > 0) {
                                        for (Finished_game game : group.getFinished_games()) {
                                            games.add(game);
                                        }
                                    }
                                }
                                mAdapter.setNotificationList(games);
                                mNoDataTxt.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            } else {
                                mNoDataTxt.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            }

                        }
                    });
        } else {
            mSubscription = mRealm.where(Group.class).equalTo("id", mGroupId).findFirstAsync().asObservable()
                    .filter(new Func1<RealmObject, Boolean>() {
                        @Override
                        public Boolean call(RealmObject realmObject) {
                            return realmObject.isLoaded();
                        }
                    }).subscribe(new Action1<RealmObject>() {
                        @Override
                        public void call(RealmObject realmObject) {
                            Group group = (Group) realmObject;
                            if (group.getFinished_games() != null && group.getFinished_games().size() > 0) {
                                List<Finished_game> games = new ArrayList<>();
                                for (Finished_game game : group.getFinished_games()) {
                                    games.add(game);
                                }
                                mAdapter.setNotificationList(games);
                                mNoDataTxt.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            } else {
                                mNoDataTxt.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            }
                        }
                    });

        }
    }

    @Override
    public String getTitle() {
        return "Match History";
    }

    @Override
    public void onItemClick(final int position, View view) {
        if (mAdapter.getGameAtPosition(position).isValid()) {
            if (mAdapter.getGameAtPosition(position).getCaptain_id() == Utils.getSavedUserIdInSharedPref(getActivity()) && mAdapter.getGameAtPosition(position).getScored() == 0) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_score);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

                final MaterialEditText teamAScoreEdt = (MaterialEditText) dialog.findViewById(R.id.teamAScore);
                teamAScoreEdt.setTextColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamAColorAtPosition(position));
                teamAScoreEdt.setPrimaryColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamAColorAtPosition(position));

                final MaterialEditText teamBScoreEdt = (MaterialEditText) dialog.findViewById(R.id.teamBScore);
                teamBScoreEdt.setTextColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamBColorAtPosition(position));
                teamBScoreEdt.setPrimaryColor(((MatchHistoryAdapter) mRecyclerView.getAdapter()).getTeamBColorAtPosition(position));

                Button setScore = (Button) dialog.findViewById(R.id.setScoresBtn);
                setScore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!TextUtils.isEmpty(teamAScoreEdt.getText().toString().trim()) && !TextUtils.isEmpty(teamBScoreEdt.getText().toString().trim())) {
                            int teamAScore = Integer.parseInt(teamAScoreEdt.getText().toString());
                            int teamBScore = Integer.parseInt(teamBScoreEdt.getText().toString());
                            updateGameScore(mAdapter.getGameAtPosition(position).getId(), teamAScore, teamBScore);
                            dialog.dismiss();

                        }
                                                }
                                            }
                );
                dialog.show();
            }
        }
    }

    private void updateGameScore(int gameId, int teamAScore, int teamBScore) {
        mProgressDialog.show();
        mApiHandler.getServices().updateGameScore(1, gameId, teamAScore, teamBScore)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FinishedGameResponse>() {
                    @Override
                    public void call(final FinishedGameResponse finishedGameResponse) {
                        mProgressDialog.dismiss();
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(finishedGameResponse.getResult().getData());
                            }
                        });
                        new MaterialDialog.Builder(getActivity())
                                .title("Save Changes")
                                .content("changes you made are successfully saved")
                                .positiveText("OK")
                                .show();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }                                DialogsUtils.showError(getActivity(), errorString, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }


                    }
                });

    }
}
