package net.nasmedia.tachkila.viewmodels;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import net.nasmedia.tachkila.ContactModel;
import net.nasmedia.tachkila.SyncContactsModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.SyncContactsResponse;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class AddressBookViewModel {

    Context mContext;
    ApiHandler mApiHandler;
    OnDataLoaded mListener;
    NewMatchViewModel.OnError mError;
    List<User> mUsers;
    public List<ContactModel> mContactsList = new ArrayList<>();

    public AddressBookViewModel(Context context, OnDataLoaded listener, NewMatchViewModel.OnError error) {
        mContext = context;
        mListener = listener;
        mError = error;
        mApiHandler = ApiHandler.getInstance();
    }

    public void displayContacts() {
        new AsyncTask<Void, Void, List<ContactModel>>() {

            @Override
            protected List<ContactModel> doInBackground(Void... voids) {
                List<ContactModel> contactsList = new ArrayList<>();

                ContentResolver cr = mContext.getContentResolver();
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                        null, null, null, null);
                if (cur.getCount() > 0) {
                    while (cur.moveToNext()) {
                        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        if (Integer.parseInt(cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);
                            while (pCur.moveToNext()) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                ContactModel contactModel = new ContactModel();
                                contactModel.setName(name);
                                contactModel.setPhone(phoneNo);
                                contactsList.add(contactModel);
                            }
                            pCur.close();
                        }
                    }
                }
                mContactsList = contactsList;
                return contactsList;

            }

            @Override
            protected void onPostExecute(List<ContactModel> contactModelList) {
                super.onPostExecute(contactModelList);
                syncContact(contactModelList);
            }
        }.execute();
    }

    public void syncContact(List<ContactModel> contactModelList) {
        SyncContactsModel syncContactsModel = new SyncContactsModel();
        syncContactsModel.setUserID(Utils.getSavedUserIdInSharedPref(mContext));
        ArrayList<String> phones = new ArrayList<>();
        for (ContactModel model : contactModelList) {
            phones.add(model.getPhone().replace(" ", ""));
        }
        syncContactsModel.setPhones(phones);
        mApiHandler.getServices().syncContact(syncContactsModel)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SyncContactsResponse>() {
                    @Override
                    public void call(SyncContactsResponse syncContactsResponse) {
                        if (syncContactsResponse.getResult().getData().size() > 0) {
                            mUsers = syncContactsResponse.getResult().getData();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_DATA);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });


    }

    public List<User> getUsers() {
        return mUsers;
    }

}
