package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.PendingRequest;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.services.response.viewplayer.ViewPlayerResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.IOException;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/27/2016.
 */

public class PlayerCardViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    My_current_friend mMyCurrentFriend;
    int mPlayerId;
    ApiHandler mApiHandler;
    OnDataLoaded removeFriend;
    OnDataLoaded addFriend;
    NewMatchViewModel.OnError mErrorListener;


    public PlayerCardViewModel(Context context, int playerId, OnDataLoaded listener, OnDataLoaded addFriendLisnere, OnDataLoaded removeFriendLisnere, NewMatchViewModel.OnError errorListener) {
        mContext = context;
        mListener = listener;
        this.mPlayerId = playerId;
        mApiHandler = ApiHandler.getInstance();
        removeFriend = removeFriendLisnere;
        addFriend = addFriendLisnere;
        mErrorListener = errorListener;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
//        My_current_friend myCurrentFriend = mRealm.where(My_current_friend.class).equalTo("id", mPlayerId).findFirst();
//        mMyCurrentFriend = myCurrentFriend;
//        mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
    }

    public void onStop() {
        mRealm.close();
    }


    public My_current_friend getMyCurrentFriend() {
        return mMyCurrentFriend;
    }

    public String getPostionName(int positionId) {
        Position position = mRealm.where(Position.class).equalTo("id", positionId).findFirst();
        if (position != null) {
            return position.getName();
        } else {
            return "";
        }
    }

    public String getCountryName(int countryId) {
        return mRealm.where(Country.class).equalTo("id", countryId).findFirst().getTitleEn();
    }

    public FriendState isUserInFriends(int id) {
        FriendState friendState = FriendState.NOT_FRIEND;
        PendingRequest pendingRequest = mRealm.where(PendingRequest.class).equalTo("id", mMyCurrentFriend.getId()).findFirst();
        if (pendingRequest != null) {
            friendState = FriendState.PENDING;
        }
        User myCurrentFriend = mRealm.where(User.class).equalTo("my_current_friends.id", id).findFirst();
        if (myCurrentFriend != null) {
            friendState = FriendState.FRIEND;
        } else if (pendingRequest == null) {
            friendState = FriendState.NOT_FRIEND;
        }
        return friendState;
    }

    public void addFriend() {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().makeFriend(Utils.getSavedUserIdInSharedPref(mContext), mMyCurrentFriend.getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<ViewGroupResponse>() {
                        @Override
                        public void call(ViewGroupResponse viewGroupResponse) {
                            mRealm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    PendingRequest pendingRequest = new PendingRequest();
                                    pendingRequest.setId(mMyCurrentFriend.getId());
                                    realm.copyToRealmOrUpdate(pendingRequest);
                                }
                            });
                            addFriend.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                addFriend.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                try {
                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                    String errorString = "";
                                    for (String string : response.getResult().getError()) {
                                        errorString += string + "\n";
                                    }
                                    mErrorListener.showError(errorString);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    addFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            } else {
                                addFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }
                    });
        } else {
            addFriend.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

    public void removeFriend() {
        addFriend.downloadComplete(OnDataLoaded.ResponseStates.START);
        mApiHandler.getServices().unFriend(Utils.getSavedUserIdInSharedPref(mContext), mMyCurrentFriend.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ViewGroupResponse>() {
                    @Override
                    public void call(ViewGroupResponse viewGroupResponse) {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getMy_current_friends().remove(mMyCurrentFriend);
                            }
                        });
                        removeFriend.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            removeFriend.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                mErrorListener.showError(errorString);
                            } catch (IOException e) {
                                e.printStackTrace();
                                removeFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        } else {
                            removeFriend.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    }
                });
    }


    public void viewPlayer(int playerId) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            mApiHandler.getServices().viewPlayer(playerId, Utils.getSavedUserIdInSharedPref(mContext))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<ViewPlayerResponse>() {
                        @Override
                        public void call(ViewPlayerResponse viewPlayerResponse) {
                            mMyCurrentFriend = viewPlayerResponse.getResult().getData();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                try {
                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                    String errorString = "";
                                    for (String string : response.getResult().getError()) {
                                        errorString += string + "\n";
                                    }
                                    mErrorListener.showError(errorString);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            } else {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }
                    });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }


    public static enum FriendState {
        PENDING,
        NOT_FRIEND,
        FRIEND
    }
}
