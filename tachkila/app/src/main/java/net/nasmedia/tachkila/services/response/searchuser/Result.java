
package net.nasmedia.tachkila.services.response.searchuser;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("data")
    @Expose
    private List<UserSearch> data = new ArrayList<UserSearch>();

    /**
     * 
     * @return
     *     The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The data
     */
    public List<UserSearch> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<UserSearch> data) {
        this.data = data;
    }

}
