package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RealPathUtil;
import net.nasmedia.tachkila.viewmodels.EditGroupViewModel;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;


/**
 * Created by Sherif on 8/19/2016.
 */
public class EditGroupFragment extends BaseFragment implements OnDataLoaded {

    public final static String ARG_GROUP_ID = "ARG_GROUP_ID";

    String avatarPath = null;
    @BindView(R.id.addGroupPictureInsTxt)
    TextView mAddGroupPictureInsTxt;
    @BindView(R.id.groupPictureImg)
    ImageView mGroupPictureImg;
    @BindView(R.id.editTextGroupName)
    EditText mGroupNameEdt;

    ProgressDialog mProgressDialog;
    EditGroupViewModel mViewModel;

    private int mGroupId;

    public static BaseFragment getInstance(int groupId) {
        EditGroupFragment fragment = new EditGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGroupId = getArguments().getInt(ARG_GROUP_ID);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mViewModel = new EditGroupViewModel(getActivity(), mGroupId, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_group, container, false);
        ButterKnife.bind(this, view);

        mViewModel.onStart();

        mGroupNameEdt.setText(mViewModel.getGroupName());
        if (mViewModel.getGroupPhoto() != null && !mViewModel.getGroupPhoto().equals("")) {
            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
            Constants.getPicResizeUrl(mGroupPictureImg, Constants.URL_GROUP_PIC + mViewModel.getGroupPhoto());
//            Glide.with(getActivity())
//                    .load(Constants.getPicResizeUrl(mGroupPictureImg, Constants.URL_GROUP_PIC + mViewModel.getGroupPhoto()))
//                    .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity()).getBitmapPool()))
//                    .into(mGroupPictureImg);
        }else{
            mAddGroupPictureInsTxt.setVisibility(View.VISIBLE);
            mGroupPictureImg.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public String getTitle() {
        return "Edit Group";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.AppTheme)
                            .pickPhoto(this);
                } else {
                    Toast.makeText(getActivity(), "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    ArrayList<String> filePaths = new ArrayList<>();

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        } else {
            FilePickerBuilder.getInstance().setMaxCount(1)
                    .setSelectedFiles(filePaths)
                    .setActivityTheme(R.style.AppTheme)
                    .enableVideoPicker(false)
                    .pickPhoto(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            filePaths = new ArrayList<>();
            filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
            File file = new File(filePaths.get(0));
            Picasso.with(getActivity()).load(file).resize(100, 100)
                    .into(mGroupPictureImg);

            mGroupPictureImg.invalidate();
            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
        }
    }


    @OnClick({R.id.addGroupPictureInsTxt, R.id.groupPictureImg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addGroupPictureInsTxt:
            case R.id.groupPictureImg:
                pickImage();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_group_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                editGroup();
                break;
        }
        return true;

    }

    private void editGroup() {
        if (!TextUtils.isEmpty(mGroupNameEdt.getText().toString())) {
            if (filePaths != null && filePaths.size() > 0) {
                avatarPath = filePaths.get(0);
            }
            mViewModel.editGroup(avatarPath, mGroupNameEdt.getText().toString());
        } else {
            Toast.makeText(getActivity(), "Please enter group name", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                Toast.makeText(getActivity(), "Error happened", Toast.LENGTH_LONG).show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_LONG).show();
                break;
            case START:
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Please wait ...");
                mProgressDialog.setTitle(null);
                mProgressDialog.show();
                break;
            case SUCCESS:
                Toast.makeText(getActivity(), "Group edited", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroyView() {
        mViewModel.onStart();
        super.onDestroyView();
    }


}
