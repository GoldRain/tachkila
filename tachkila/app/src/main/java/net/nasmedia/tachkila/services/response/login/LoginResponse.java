package net.nasmedia.tachkila.services.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.services.response.register.RegisterResult;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class LoginResponse {
    @SerializedName("result")
    @Expose
    private LoginResult result;

    public LoginResult getResult() {
        return result;
    }

    public void setResult(LoginResult result) {
        this.result = result;
    }
}
