
package net.nasmedia.tachkila.services.response.error;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("result")
    @Expose
    private Error result;

    /**
     * 
     * @return
     *     The result
     */
    public Error getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Error result) {
        this.result = result;
    }

}
