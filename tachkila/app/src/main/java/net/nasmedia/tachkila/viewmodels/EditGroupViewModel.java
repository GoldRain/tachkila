package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;

import java.io.File;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class EditGroupViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    Group mGroup;

    int mGroupId;

    public EditGroupViewModel(Context context, int groupId, OnDataLoaded listener) {
        this.mContext = context;
        this.mListener = listener;
        mGroupId = groupId;
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mGroup = mRealm.where(Group.class).equalTo("id", mGroupId).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public String getGroupName() {
        return mGroup.getName();
    }

    public String getGroupPhoto() {
        return mGroup.getAvatar();
    }

    public void editGroup(String avatarPath, String groupName) {
        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

            RequestBody requestFile = null;
            File avatarFile = null;
            if (avatarPath != null) {
                avatarFile = new File(avatarPath);
            }
            if (avatarFile != null) {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            }
            RequestBody groupNameBody = RequestBody.create(MediaType.parse("text/plain"), groupName);
            RequestBody groupIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mGroupId));
            mApiHandler.getServices().editGroup(requestFile, groupNameBody, groupIdBody)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<CreateGroupResponse>() {
                        @Override
                        public void call(final CreateGroupResponse createGroupResponse) {
                            mRealm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(createGroupResponse.getResult().getData());
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                }
                            });
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                        }
                    });
        } else {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
        }
    }

}
