package net.nasmedia.tachkila.services.response.creategames;

import net.nasmedia.tachkila.realmmodels.ActiveGame;

/**
 * Created by Mahmoud Galal on 8/18/2016.
 */
public class CreateGameResult {
    private int success;
    private ActiveGame data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ActiveGame getData() {
        return data;
    }

    public void setData(ActiveGame data) {
        this.data = data;
    }
}
