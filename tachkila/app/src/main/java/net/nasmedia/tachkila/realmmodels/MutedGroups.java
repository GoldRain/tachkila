package net.nasmedia.tachkila.realmmodels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by galal on 12/06/17.
 */

public class MutedGroups extends RealmObject {

    @PrimaryKey
    int groupId;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
