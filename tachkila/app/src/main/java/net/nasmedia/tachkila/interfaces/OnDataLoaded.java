package net.nasmedia.tachkila.interfaces;


/**
 * Created by Mahmoud El-Morsi on 2/24/2016.
 */
public interface OnDataLoaded {

    enum ResponseStates {
        SUCCESS,
        NO_NETWORK,
        ERROR,
        NO_DATA,
        START
    }

    void downloadComplete(ResponseStates responseStates);
}
