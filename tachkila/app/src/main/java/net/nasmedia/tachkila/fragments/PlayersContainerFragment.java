package net.nasmedia.tachkila.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class PlayersContainerFragment extends BaseFragment {

    @BindView(R.id.playersBtn)
    Button mPlayersBtn;
    @BindView(R.id.requestsBtn)
    Button mRequestsBtn;
    @BindView(R.id.suggestedBtn)
    Button mSuggestedBtn;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    public static PlayersContainerFragment getInstance() {
        return new PlayersContainerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.players_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT);
                startActivity(intent);
                break;
        }
        return true;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players_container, container, false);
        ButterKnife.bind(this, view);

        loadFragment(PlayersFragment.TYPE_PLAYERS);
        mPlayersBtn.setSelected(true);

        setupPullToRefresh();

        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public String getTitle() {
        return "Players";
    }

    @OnClick({R.id.playersBtn, R.id.requestsBtn, R.id.suggestedBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.playersBtn:
                if (!mPlayersBtn.isSelected()) {
                    loadFragment(PlayersFragment.TYPE_PLAYERS);
                    mPlayersBtn.setSelected(true);
                    mRequestsBtn.setSelected(false);
                    mSuggestedBtn.setSelected(false);
                }
                break;
            case R.id.requestsBtn:
                if (!mRequestsBtn.isSelected()) {

                    loadFragment(PlayersFragment.TYPE_REQUESTES);
                    mPlayersBtn.setSelected(false);
                    mRequestsBtn.setSelected(true);
                    mSuggestedBtn.setSelected(false);
                }
                break;
            case R.id.suggestedBtn:
                if (!mSuggestedBtn.isSelected()) {

                    loadFragment(PlayersFragment.TYPE_SUGGESTED);
                    mPlayersBtn.setSelected(false);
                    mRequestsBtn.setSelected(false);
                    mSuggestedBtn.setSelected(true);
                }
                break;
        }
    }

    private void loadFragment(String type) {
        if (type.equals(PlayersFragment.TYPE_SUGGESTED)) {
            getChildFragmentManager().beginTransaction().replace(R.id.fragmentContainer, SuggestsPlayersFragment.getInstance()).commit();
        }else if (type.equals(PlayersFragment.TYPE_REQUESTES)) {
            getChildFragmentManager().beginTransaction().replace(R.id.fragmentContainer, FollwersPlayersFragment.getInstance()).commit();
        }else{
            getChildFragmentManager().beginTransaction().replace(R.id.fragmentContainer, PlayersFragment.getInstance()).commit();
        }
    }
}
