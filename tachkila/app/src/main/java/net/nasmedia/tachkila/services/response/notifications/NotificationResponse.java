package net.nasmedia.tachkila.services.response.notifications;

/**
 * Created by Mahmoud Galal on 7/14/2016.
 */
public class NotificationResponse {

    NotificationResult result;

    public NotificationResult getResult() {
        return result;
    }

    public void setResult(NotificationResult result) {
        this.result = result;
    }
}
