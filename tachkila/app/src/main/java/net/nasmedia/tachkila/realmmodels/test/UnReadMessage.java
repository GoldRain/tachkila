package net.nasmedia.tachkila.realmmodels.test;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by galal on 01/06/17.
 */

public class UnReadMessage extends RealmObject {

    @Index
    int group_id;
    @PrimaryKey
    String android_id;


    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getAndroid_id() {
        return android_id;
    }

    public void setAndroid_id(String android_id) {
        this.android_id = android_id;
    }
}
