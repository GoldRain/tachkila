package net.nasmedia.tachkila.utils;


import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;


/**
 * Created by Mahmoud Galal on 4/3/2016.
 */
public class Constants {

    public static final String BASE_URL = "http://app.tachkila.com/api/";

    public static final String URL_GROUP_PIC = "media/groups/";
    public static final String URL_AVATAR_PIC = "media/avatars/";
    public static final String URL_CHATS_PIC = "media/chats/";


    public static void getPicResizeUrl(final ImageView imageView, final String path) {

//        String url = "http://app.tachkila.com/resize?w=" + 200 + "&h=" + 200 + "&src=" + path;
        String url = "http://app.tachkila.com/" + path;
        Log.d("URL", url);

        Picasso.with(imageView.getContext())
                .load(url)
                .resize(200, 200)
                .placeholder(R.drawable.ic_user_default)
                .error(R.drawable.ic_user_default)
                .into(imageView);
    }

    public static void getPicGroupUrl(final ImageView imageView, final String path) {

//        String url = "http://app.tachkila.com/resize?w=" + 200 + "&h=" + 200 + "&src=" + path;
        String url = "http://app.tachkila.com/" + path;
        Log.d("URL", url);

        Picasso.with(imageView.getContext())
                .load(url)
                .resize(200, 200)
                .placeholder(R.drawable.group)
                .error(R.drawable.group)
                .into(imageView);
    }

    public static void getPicChatResizeUrl(final ImageView imageView, final String path) {

        String url = "http://app.tachkila.com/" + path;
//        String url = "http://app.tachkila.com/resize?w=" + 50 + "&h=" + 50 + "&src=" + path;
        Log.d("URL", url);

        Picasso.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .resize(50, 50)
                .error(R.drawable.placeholder)
                .into(imageView);
    }

    public static final String ACTION_SHOW_REGISTER_ACTIVITY = "ACTION_SHOW_REGISTER_ACTIVITY";
    public static final String APP_PREF_NAME = "pref_tachkela";
    public static final String USER_ID_PREF_KEY = "pref_userId";
    public static final String PASSED_LOGIN_KEY = "pref_login";


    //    public static final String ACTION_SHOW_LOGIN_ACTIVITY = "ACTION_SHOW_LOGIN_ACTIVITY";
    public static final String ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT = "ACTION_SHOW_ADD_NEW_MATCH";
    public static final String ACTION_SHOW_ADD_NEW_Group_FRAGMENT = "ACTION_SHOW_ADD_NEW_Group";
    public static final String ACTION_SHOW_GROUP_DETAILS_FRAGMENT = "ACTION_SHOW_GROUP_DETAILS_FRAGMENT";
    public static final String ACTION_SHOW_GROUP_DETAILS_FROM_FRAGMENT = "ACTION_SHOW_GROUP_DETAILS_FROM_FRAGMENT";
    public static final String ACTION_SHOW_ADD_PLAYERS_FRAGMENT = "ACTION_SHOW_ADD_PLAYERS_FRAGMENT";
    public static final String ACTION_SHOW_MATCH_HISTORY_FRAGMENT = "ACTION_SHOW_MATCH_HISTORY_FRAGMENT";
    public static final String ACTION_SHOW_OTHER_PLAYER_FRAGMENT = "ACTION_SHOW_OTHER_PLAYER_FRAGMENT";
    public static final String ACTION_SHOW_NOTIFICATION_FRAGMENT = "ACTION_SHOW_NOTIFICATION_FRAGMENT";
    public static final String ACTION_SHOW_WEBVIEW_FRAGMENT = "ACTION_SHOW_WEBVIEW_FRAGMENT";
    public static final String ACTION_SHOW_GAME_DETAILS_FRAGMENT = "ACTION_SHOW_GAME_DETAILS_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYERS_FRAGMENT = "ACTION_SHOW_INVITE_PLAYERS_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT = "ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT";
    public static final String ACTION_SHOW_ADDRESS_BOOK_FRAGMENT = "ACTION_SHOW_ADDRESS_BOOK_FRAGMENT";
    public static final String ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT = "ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT";
    public static final String ACTION_SHOW_SETTINGS_FRAGMENT = "ACTION_SHOW_SETTINGS_FRAGMENT";
    public static final String ACTION_SHOW_EDIT_GROUP_FRAGMENT = "ACTION_SHOW_EDIT_GROUP_FRAGMENT";
    public static final String ACTION_SHOW_CHATS_FRAGMENT = "ACTION_SHOW_CHATS_FRAGMENT";
    public static final String ACTION_SHOW_MESSAGES_FRAGMENT = "ACTION_SHOW_MESSAGES_FRAGMENT";
    public static final String ACTION_SHOW_CHAT_INFO_FRAGMENT = "ACTION_SHOW_CHAT_INFO_FRAGMENT";
    public static final String ACTION_SHOW_CHAT_GALLERY_FRAGMENT = "ACTION_SHOW_CHAT_GALLERY_FRAGMENT";

}
