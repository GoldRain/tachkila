
package net.nasmedia.tachkila.realmmodels.test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Finished_game extends RealmObject {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("time_of_game")
    @Expose
    private String time_of_game;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("map_lat")
    @Expose
    private double map_lat;
    @SerializedName("map_lng")
    @Expose
    private double map_lng;
    @SerializedName("group_id")
    @Expose
    private int group_id;
    @SerializedName("captain_id")
    @Expose
    private int captain_id;
    @SerializedName("type_id")
    @Expose
    private int type_id;
    @SerializedName("team1_color_id")
    @Expose
    private int team1_color_id;
    @SerializedName("team2_color_id")
    @Expose
    private int team2_color_id;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("completed")
    @Expose
    private int completed;
    @SerializedName("finished")
    @Expose
    private int finished;
    @SerializedName("scored")
    @Expose
    private int scored;
    @SerializedName("team1_score")
    @Expose
    private int team1_score;
    @SerializedName("team2_score")
    @Expose
    private int team2_score;
    @SerializedName("image_url")
    @Expose
    private String image_url;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("deleted_at")
    @Expose
    private String deleted_at;
    @SerializedName("captain")
    @Expose
    private Captain captain;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("team1_color")
    @Expose
    private Team1_color team1_color;
    @SerializedName("team2_color")
    @Expose
    private Team2_color team2_color;
    @SerializedName("players")
    @Expose
    private RealmList<Player> players = new RealmList<Player>();

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The time_of_game
     */
    public String getTime_of_game() {
        return time_of_game;
    }

    /**
     * @param time_of_game The time_of_game
     */
    public void setTime_of_game(String time_of_game) {
        this.time_of_game = time_of_game;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The map_lat
     */
    public double getMap_lat() {
        return map_lat;
    }

    /**
     * @param map_lat The map_lat
     */
    public void setMap_lat(double map_lat) {
        this.map_lat = map_lat;
    }

    /**
     * @return The map_lng
     */
    public double getMap_lng() {
        return map_lng;
    }

    /**
     * @param map_lng The map_lng
     */
    public void setMap_lng(double map_lng) {
        this.map_lng = map_lng;
    }

    /**
     * @return The group_id
     */
    public int getGroup_id() {
        return group_id;
    }

    /**
     * @param group_id The group_id
     */
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    /**
     * @return The captain_id
     */
    public int getCaptain_id() {
        return captain_id;
    }

    /**
     * @param captain_id The captain_id
     */
    public void setCaptain_id(int captain_id) {
        this.captain_id = captain_id;
    }

    /**
     * @return The type_id
     */
    public int getType_id() {
        return type_id;
    }

    /**
     * @param type_id The type_id
     */
    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    /**
     * @return The team1_color_id
     */
    public int getTeam1_color_id() {
        return team1_color_id;
    }

    /**
     * @param team1_color_id The team1_color_id
     */
    public void setTeam1_color_id(int team1_color_id) {
        this.team1_color_id = team1_color_id;
    }

    /**
     * @return The team2_color_id
     */
    public int getTeam2_color_id() {
        return team2_color_id;
    }

    /**
     * @param team2_color_id The team2_color_id
     */
    public void setTeam2_color_id(int team2_color_id) {
        this.team2_color_id = team2_color_id;
    }

    /**
     * @return The notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes The notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return The completed
     */
    public int getCompleted() {
        return completed;
    }

    /**
     * @param completed The completed
     */
    public void setCompleted(int completed) {
        this.completed = completed;
    }

    /**
     * @return The finished
     */
    public int getFinished() {
        return finished;
    }

    /**
     * @param finished The finished
     */
    public void setFinished(int finished) {
        this.finished = finished;
    }

    /**
     * @return The scored
     */
    public int getScored() {
        return scored;
    }

    /**
     * @param scored The scored
     */
    public void setScored(int scored) {
        this.scored = scored;
    }

    /**
     * @return The team1_score
     */
    public int getTeam1_score() {
        return team1_score;
    }

    /**
     * @param team1_score The team1_score
     */
    public void setTeam1_score(int team1_score) {
        this.team1_score = team1_score;
    }

    /**
     * @return The team2_score
     */
    public int getTeam2_score() {
        return team2_score;
    }

    /**
     * @param team2_score The team2_score
     */
    public void setTeam2_score(int team2_score) {
        this.team2_score = team2_score;
    }

    /**
     * @return The image_url
     */
    public String getImage_url() {
        return image_url;
    }

    /**
     * @param image_url The image_url
     */
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    /**
     * @return The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * @param created_at The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * @return The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * @param updated_at The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * @return The deleted_at
     */
    public String getDeleted_at() {
        return deleted_at;
    }

    /**
     * @param deleted_at The deleted_at
     */
    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     * @return The captain
     */
    public Captain getCaptain() {
        return captain;
    }

    /**
     * @param captain The captain
     */
    public void setCaptain(Captain captain) {
        this.captain = captain;
    }

    /**
     * @return The type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return The team1_color
     */
    public Team1_color getTeam1_color() {
        return team1_color;
    }

    /**
     * @param team1_color The team1_color
     */
    public void setTeam1_color(Team1_color team1_color) {
        this.team1_color = team1_color;
    }

    /**
     * @return The team2_color
     */
    public Team2_color getTeam2_color() {
        return team2_color;
    }

    /**
     * @param team2_color The team2_color
     */
    public void setTeam2_color(Team2_color team2_color) {
        this.team2_color = team2_color;
    }

    /**
     * @return The players
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * @param players The players
     */
    public void setPlayers(RealmList<Player> players) {
        this.players = players;
    }

}
