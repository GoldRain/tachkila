package net.nasmedia.tachkila.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.MutedGroups;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomButton;
import net.nasmedia.tachkila.widgets.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;

/**
 * Created by galal on 08/06/17.
 */

public class ChatInfoFragment extends BaseFragment {


    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";

    Unbinder unbinder;
    @BindView(R.id.photoCountTxt)
    CustomTextView mPhotoCountTxt;
    @BindView(R.id.muteChatBtn)
    CustomButton mMuteChatBtn;

    int mGroupId;


    public static ChatInfoFragment getInstance(int groupId) {
        ChatInfoFragment fragment = new ChatInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_info, container, false);
        unbinder = ButterKnife.bind(this, view);

        mGroupId = getArguments().getInt(ARG_GROUP_ID);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        long photosCount = mRealm.where(Message.class).equalTo("group_id", mGroupId).equalTo("type", "photo").count();

        if (isGroupMuted()) {
            changeButtonText(true);
        } else {
            changeButtonText(false);
        }

        mPhotoCountTxt.setText(String.valueOf(photosCount));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public String getTitle() {
        return "Chat Info";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.galleryBtn, R.id.muteChatBtn, R.id.clearChatBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.galleryBtn:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_CHAT_GALLERY_FRAGMENT);
                intent.putExtra(GalleryFragment.ARG_GROUP_ID, mGroupId);
                startActivity(intent);
                break;
            case R.id.muteChatBtn:
                muteChat();
                break;
            case R.id.clearChatBtn:
                new MaterialDialog.Builder(getActivity())
                        .content("Are you sure?")
                        .title("Clear chat")
                        .positiveText("Yes")
                        .negativeText("No")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                mRealm.executeTransactionAsync(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.where(Message.class).equalTo("group_id", mGroupId).findAll().deleteAllFromRealm();
                                    }
                                });
                            }
                        })
                        .show();

                break;
        }
    }

    private void muteChat() {
        if (!isGroupMuted()) {
            OneSignal.sendTag("silent_group_" + mGroupId, "1");
            saveMutedGroup();
            changeButtonText(true);
        } else {
            OneSignal.sendTag("silent_group_" + mGroupId, "0");
            deleteMutedGroup();
            changeButtonText(false);
        }
    }

    private void changeButtonText(boolean isMuted) {
        if (isMuted) {
            mMuteChatBtn.setText("Unmute the group");
        } else {
            mMuteChatBtn.setText("Mute the group");
        }
    }

    private void deleteMutedGroup() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MutedGroups mutedGroups = realm.where(MutedGroups.class).equalTo("groupId", mGroupId).findFirst();
                if (mutedGroups != null) {
                    mutedGroups.deleteFromRealm();
                }
            }
        });
    }

    private void saveMutedGroup() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MutedGroups mutedGroups = new MutedGroups();
                mutedGroups.setGroupId(mGroupId);
                realm.copyToRealmOrUpdate(mutedGroups);
            }
        });
    }

    public boolean isGroupMuted() {
        boolean groupMuted = false;

        MutedGroups mutedGroups = mRealm.where(MutedGroups.class).equalTo("groupId", mGroupId).findFirst();
        if (mutedGroups != null) {
            groupMuted = true;
        }

        return groupMuted;
    }
}