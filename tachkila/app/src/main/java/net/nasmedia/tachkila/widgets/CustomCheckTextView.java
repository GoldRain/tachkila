package net.nasmedia.tachkila.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.utils.FontCache;


public class CustomCheckTextView extends CheckedTextView {

    public CustomCheckTextView(Context context) {
        super(context);
    }

    public CustomCheckTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomCheckTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomCheckTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFontFromCache(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFontFromCache(Context ctx, String asset) {
        Typeface tf = FontCache.get("fonts/" + asset, ctx);
        setTypeface(tf);
        return true;
    }

}
