package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import net.nasmedia.tachkila.MessageWrapper;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.MessagesAdapter;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.realmmodels.test.User_;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.PostMessageRespone;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static permissions.dispatcher.R.id.time;

/**
 * Created by galal on 02/06/17.
 */

public class MessagesFragment extends Fragment implements MessagesAdapter.OnImageClicked {

    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";

    @BindView(R.id.message_input) EditText mMessageInput;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    Unbinder unbinder;

    int mGroupId;
    MessagesAdapter mAdapter;
    String avatarPath;
    Subscription mSubscription;
    /*Realm Realm.getDefaultInstance();*/
    ApiHandler mApiHandler;

    @BindView(R.id.send_button) Button sendButton;

    Timer typingTimer;
    TimerTask task;

    int _remainTime = 0;
    Boolean timerStared = false;

    @BindView(R.id.txv_typing) TextView txv_typing;
    ArrayList<Integer> userIds = new ArrayList<>();


    Group group;
    public static MessagesFragment getInstance(int groupId) {
        MessagesFragment fragment = new MessagesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.message_menu, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.info:
//                Intent intent = new Intent(getActivity(), DetailsActivity.class);
//                intent.setAction(Constants.ACTION_SHOW_CHAT_INFO_FRAGMENT);
//                intent.putExtra(ARG_GROUP_ID, mGroupId);
//                startActivity(intent);
//                break;
//        }
//        return true;
//
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_messages, container, false);
        unbinder = ButterKnife.bind(this, view);

        txv_typing.setVisibility(View.GONE);

        mGroupId = getArguments().getInt(ARG_GROUP_ID);
        setupRecyclerView();

        //mRealm = Realm.getDefaultInstance();
        mApiHandler = ApiHandler.getInstance();

        mMessageInput.clearFocus();
        mMessageInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        });


        mMessageInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //got focus
                    mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                } else {
                    //lost focus
                }
            }
        });


        mMessageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    sendButton.setTextColor(getResources().getColor(R.color.sendTextColor));
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                    sendButton.setTextColor(Color.parseColor("#FF919191"));
                }
                _remainTime = 0;
                if(!timerStared) {
                    postSystemMessage("typing_started");
                    startTimer();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final RealmResults<Message> messages = Realm.getDefaultInstance().where(Message.class).equalTo("group_id", mGroupId).equalTo("looding", true).findAll();
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                messages.deleteAllFromRealm();
            }
        });

        unSubscribeFromObservable();
        mSubscription = Realm.getDefaultInstance().where(Message.class).equalTo("group_id", mGroupId).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<Message>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Message> messages) {
                        Log.d("message", messages.isLoaded() + "");
                        return messages.isLoaded();
                    }
                })
                .subscribe(new Action1<RealmResults<Message>>() {
                    @Override
                    public void call(RealmResults<Message> messages) {
                        if (isAdded()) {
                            Log.d("messages", "changed");
                            List<MessageWrapper> messageWrappers = new ArrayList<MessageWrapper>();
                            for (Message message : messages) {
                                MessageWrapper messageWrapper = new MessageWrapper();
                                messageWrapper.setMessage(message);
                                if (Utils.getSavedUserIdInSharedPref(getActivity()) == message.getUser_id()) {
                                    messageWrapper.setMyUser(Realm.getDefaultInstance().where(User.class).equalTo("id", message.getUser_id()).findFirst());
                                } else {
                                    group = Realm.getDefaultInstance().where(Group.class).equalTo("id", mGroupId).findFirst();
                                    for (User_ user : group.getUsers()) {
                                        if (user.getId() == message.getUser_id()) {
                                            messageWrapper.setOtherUser(user);
                                            break;
                                        }
                                    }
                                }
                                

                                if(messageWrapper.getMessage().equals("typing_started")){

                                    txv_typing.setVisibility(View.VISIBLE);
                                    addTypingUser(messageWrapper.getUser_id());
                                    txv_typing.setText(getTypingUserName());

                                } else if (messageWrapper.getMessage().equals("typing_finished")){
                                    removeTypingUser(messageWrapper.getUser_id());

                                    txv_typing.setVisibility(View.GONE);

                                } else {

                                    txv_typing.setVisibility(View.GONE);

                                        messageWrappers.add(messageWrapper);
                                }
                            }

                            mAdapter.setItems(messageWrappers);
                            mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    }
                });


        return view;
    }

    private void addTypingUser(int id){

        removeTypingUser(id);
        userIds.add(0, id);

    }

    private void removeTypingUser(int id){

        for (int i = 0; i <  userIds.size(); i++){

            if (userIds.get(i) == id){
                userIds.remove(i);
                break;
            }
        }

    }

    private String getTypingUserName(){
        if(userIds.size() > 0) {
            for (int i = 0; i < group.getUsers().size(); i++) {
                User_ user = group.getUsers().get(i);
                if (user.getId() == userIds.get(0)) {
                    return user.getName() + " is typing...";
                }
            }
        }
        return "";

    }

    public void startTimer(){

        timerStared = true;
        typingTimer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {

                _remainTime++;
                if (_remainTime >= 5) {
                    _remainTime = 0;
                    typingTimer.cancel();
                    postSystemMessage("typing_finished");
                    timerStared = false;
                }
                Log.d("timer session", String.valueOf(_remainTime));
            }
            @Override
            public boolean cancel() {
                return super.cancel();
            }
        };
        typingTimer.scheduleAtFixedRate(task, 0, 1000);
    }

    void unSubscribeFromObservable() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    private void deleteUnReadMessage() {
        final RealmResults<UnReadMessage> unReadMessages = Realm.getDefaultInstance().where(UnReadMessage.class).equalTo("group_id", mGroupId).findAll();
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unReadMessages.deleteAllFromRealm();
            }
        });
    }

    void reset() {
        unSubscribeFromObservable();
        /*Realm.getDefaultInstance().close();
        Realm.getDefaultInstance() = null;*/
        mApiHandler = null;
        mSubscription = null;
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.dimen.chatrecyclerViewVerticalSpace));

        mAdapter = new MessagesAdapter(getActivity());
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(mAdapter));

    }

    @Override
    public void onStart() {
        super.onStart();
        deleteUnReadMessage();
        // initialize Pusher
    }

    @Override
    public void onStop() {
        super.onStop();
        deleteUnReadMessage();
//        mPusher.disconnect();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        reset();
    }

    ArrayList<String> filePaths = new ArrayList<>();

    private void postImageMessage() {
        pickImage();
    }

    public void pickImage() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        } else {
            FilePickerBuilder.getInstance().setMaxCount(1)
                    .setSelectedFiles(filePaths)
                    .setActivityTheme(R.style.AppTheme)
                    .enableVideoPicker(false)
                    .pickPhoto(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.AppTheme)
                            .pickPhoto(this);
                } else {
                    Toast.makeText(getActivity(), "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            filePaths = new ArrayList<>();
            filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
            avatarPath = filePaths.get(0);
            postImage();
        }
    }

    private void postImage() {
        File avatarFile = null;
        if (avatarPath != null) {
            avatarFile = new File(avatarPath);
        }

        final Message message = new Message();
        message.setAndroid_id(UUID.randomUUID().toString());
        message.setId(0);
        message.setGroup_id(mGroupId);
        message.setMessage(avatarPath);
        message.setType("photo");
        message.setUser_id(Utils.getSavedUserIdInSharedPref(getActivity()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        Date date = new Date();
        message.setCreated_at(simpleDateFormat.format(date));
        message.setLooding(true);

        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(message);
            }
        });


        RequestBody requestFile = null;
        if (avatarFile != null) {
//            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            requestFile = RequestBody.create(MediaType.parse("image/*"), avatarFile);
        }
        RequestBody userId =
                RequestBody.create(
                        MediaType.parse("text/plain"), String.valueOf(Utils.getSavedUserIdInSharedPref(getActivity())));
        RequestBody groupId =
                RequestBody.create(
                        MediaType.parse("text/plain"), String.valueOf(mGroupId));
        RequestBody type =
                RequestBody.create(
                        MediaType.parse("text/plain"), "photo");

        RequestBody android_uuid =
                RequestBody.create(
                        MediaType.parse("text/plain"), message.getAndroid_id());

        ApiHandler.getInstance().getServices().sendImageMessage(requestFile, groupId, type, android_uuid, userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PostMessageRespone>() {
                    @Override
                    public void call(final PostMessageRespone postMessageRespone) {
                        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(postMessageRespone.getResult().getData());
                            }
                        });
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void postSystemMessage(String text) {

        Log.d("Statuse==>", text);

        final Message message = new Message();
        message.setAndroid_id(UUID.randomUUID().toString());
        message.setId(0);
        message.setGroup_id(mGroupId);
        message.setMessage(text);
        message.setType("system");
        message.setUser_id(Utils.getSavedUserIdInSharedPref(getActivity()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        Date date = new Date();
        message.setCreated_at(simpleDateFormat.format(date));
        message.setLooding(true);


        mSubscription = mApiHandler.getServices().sendTextMessage(Utils.getSavedUserIdInSharedPref(getActivity()),
                mGroupId,
                text,
                "system",
                message.getAndroid_id())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PostMessageRespone>() {
                    @Override
                    public void call(final PostMessageRespone finishedGameResponse) {
                        Log.e("response", String.valueOf(finishedGameResponse));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
        //DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
    }
});
    }

    private void postTextMessage() {

        if(timerStared) {
            typingTimer.cancel();
            postSystemMessage("typing_finished");
            timerStared = false;
        }

        String text = mMessageInput.getText().toString();
        mMessageInput.setText("");
        // return if the text is blank
        if (text.equals("")) {
            return;
        }

        final Message message = new Message();
        message.setAndroid_id(UUID.randomUUID().toString());
        message.setId(0);
        message.setGroup_id(mGroupId);
        message.setMessage(text);
        message.setType("text");
        message.setUser_id(Utils.getSavedUserIdInSharedPref(getActivity()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        Date date = new Date();
        message.setCreated_at(simpleDateFormat.format(date));
        message.setLooding(true);

        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(message);
            }
        });


//        MessageWrapper messageWrapper = new MessageWrapper();
//        messageWrapper.setId(122222);
//        messageWrapper.setGroup_id(mGroupId);
//        messageWrapper.setMessage(text);
//        messageWrapper.setType("text");
//        messageWrapper.setMyUser(Realm.getDefaultInstance().where(User.class).findFirst());
//        messageWrapper.setUser_id(Utils.getSavedUserIdInSharedPref(getActivity()));
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
//        Date date = new Date();
//        messageWrapper.setCreated_at(simpleDateFormat.format(date));
//        messageWrapper.setLooding(true);
//        mAdapter.addItem(messageWrapper);
//        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);


        mSubscription = mApiHandler.getServices().sendTextMessage(Utils.getSavedUserIdInSharedPref(getActivity()),
                mGroupId,
                text,
                "text",
                message.getAndroid_id())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PostMessageRespone>() {
                    @Override
                    public void call(final PostMessageRespone finishedGameResponse) {
                        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(finishedGameResponse.getResult().getData());
                            }
                        });
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                    }
                });
    }

    @OnClick(R.id.imageBtn)
    public void onImageBtnClicked() {
        postImageMessage();
    }

    @OnClick(R.id.send_button)
    public void onSendButtonClicked() {
        postTextMessage();
    }

    @Override
    public void imageClicked(int userId) {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
        intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, userId);
        intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
        startActivity(intent);
    }

}
