package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tubb.smrv.SwipeMenuRecyclerView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.PlayersAdapter;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class PlayersFragment extends BaseFragment {

    public static final String ARG_TYPE = "ARG_TYPE";
    public static final String TYPE_PLAYERS = "TYPE_PLAYERS";
    public static final String TYPE_REQUESTES = "TYPE_REQUESTES";
    public static final String TYPE_SUGGESTED = "TYPE_SUGGESTED";

    @BindView(R.id.recyclerView)
    SwipeMenuRecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;

    PlayersAdapter mAdapter;

    public static PlayersFragment getInstance() {
        PlayersFragment playersFragment = new PlayersFragment();
        return playersFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new PlayersAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

        unSubscribeFromObservable();
        mSubscription = mRealm.where(My_current_friend.class).findAllAsync()
                .asObservable()
                .filter(new Func1<RealmResults<My_current_friend>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<My_current_friend> my_current_friends) {
                        return my_current_friends.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<My_current_friend>>() {
                    @Override
                    public void call(RealmResults<My_current_friend> my_current_friends) {
                        if (my_current_friends != null && my_current_friends.size() > 0) {
                            mAdapter.setUsers(mRealm.copyFromRealm(my_current_friends));
                            mNoDataTxt.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mNoDataTxt.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                });
    }

}
