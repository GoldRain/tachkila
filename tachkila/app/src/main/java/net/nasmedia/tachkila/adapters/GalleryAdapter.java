package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by galal on 08/06/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Message> mItems;

    public GalleryAdapter(Context context) {
        this.mContext = context;
        mItems = Collections.EMPTY_LIST;
    }

    public void setmItems(List<Message> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = mItems.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        Constants.getPicChatResizeUrl(viewHolder.image, Constants.URL_CHATS_PIC + message.getMessage());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
