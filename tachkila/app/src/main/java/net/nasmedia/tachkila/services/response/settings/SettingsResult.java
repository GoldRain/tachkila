
package net.nasmedia.tachkila.services.response.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.services.response.settings.Data;

public class SettingsResult {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("data")
    @Expose
    private Data data;

    /**
     * 
     * @return
     *     The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * 
     * @param success
     *     The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * 
     * @return
     *     The data
     */
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(Data data) {
        this.data = data;
    }

}
