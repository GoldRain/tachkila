package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.Finished_game;
import net.nasmedia.tachkila.realmmodels.test.Group;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by mahmoudgalal on 7/28/16.
 */
public class MatchHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Finished_game> mNotificationList;
    private Context mContext;

    private OnItemClickListener mClickListener;

    public void setClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public MatchHistoryAdapter(Context context) {
        mContext = context;
        mNotificationList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = createNotificationViewHolder(parent);
        return viewHolder;
    }

    private RecyclerView.ViewHolder createNotificationViewHolder(ViewGroup parent) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_match_history, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindTestViewHolder(holder, position);
    }

    private void bindTestViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        Finished_game notification = mNotificationList.get(position);
        holder.leftScore.setText(notification.getTeam1_score() + "");
        holder.rightScore.setText(notification.getTeam2_score() + "");
        Realm realm = Realm.getDefaultInstance();
        holder.matchLocation.setText(notification.getLocation());
        holder.matchName.setText(realm.where(Group.class).equalTo("id", notification.getGroup_id()).findFirst().getName());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date = simpleDateFormat.parse(notification.getCreated_at());
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy");
            holder.matchDate.setText(simpleDateFormat1.format(date));
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("hh:mm aaa");
            holder.matchTime.setText(simpleDateFormat2.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(position, view);
            }
        });
//        holder.message.setText(notification.getText());
//        if (notification.getFriend() != null) {
        loadImage("http://tachkila.sz4h.com/api/game-image/image/" + notification.getId() + "?t=" + Calendar.getInstance().getTimeInMillis(), holder);
//        } else {
//            holder.userImg.setVisibility(View.GONE);
//        }
//        holder.time.setText(notification.getCreatedAt());

    }

    private void loadImage(String url, final ViewHolder holder) {
        Log.d("url", url);
        if (url == null || url.equals("")) {
            holder.progressBar.setVisibility(View.GONE);
            Picasso.with(mContext)
                    .load(R.drawable.placeholder)
                    .into(holder.matchImg);
        } else {
            Picasso.with(mContext)
                    .load(url)

                    .placeholder(R.drawable.bgk_field)
                    .into(holder.matchImg, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);

                        }

                        @Override
                        public void onError() {
                            holder.progressBar.setVisibility(View.GONE);

                        }
                    });
        }
    }

    public Finished_game getGameAtPosition(int position) {
        return mNotificationList.get(position);
    }

    private String getPostTime(long postTime) {
        DateTime startTime = new DateTime(postTime);
        DateTime endTime = new DateTime();
        Period p = new Period(startTime, endTime);
        if (p.getDays() == 0 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            if (p.getHours() == 0 && p.getMinutes() <= 5) {
                return "Now";
            } else if (p.getHours() == 0 && p.getMinutes() > 5) {
                return p.getMinutes() + " m";
            } else if (p.getHours() > 0 && p.getMinutes() == 0) {
                return p.getHours() + " h";
            } else {
                return p.getHours() + " h " + p.getMinutes() + " m";
            }
        } else if (p.getDays() == 1 && p.getWeeks() == 0 && p.getMonths() == 0 && p.getYears() == 0) {
            return "Yesterday";
        } else {
            DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MMMM dd, yyyy");
            return dtfOut.print(startTime);
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }


    public void setNotificationList(List<Finished_game> notifications) {
        this.mNotificationList = notifications;
        notifyDataSetChanged();
    }

    public int getTeamAColorAtPosition(int position) {
        return Color.parseColor(mNotificationList.get(position).getTeam1_color().getCode());
    }

    public int getTeamBColorAtPosition(int position) {
        return Color.parseColor(mNotificationList.get(position).getTeam2_color().getCode());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.leftScore)
        TextView leftScore;
        @BindView(R.id.rightScore)
        TextView rightScore;
        @BindView(R.id.matchImg)
        ImageView matchImg;
        @BindView(R.id.matchName)
        TextView matchName;
        @BindView(R.id.matchLocation)
        TextView matchLocation;
        @BindView(R.id.matchDate)
        TextView matchDate;
        @BindView(R.id.matchTime)
        TextView matchTime;
        @BindView(R.id.root)
        LinearLayout root;
        @BindView(R.id.progress)
        ProgressBar progressBar;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
