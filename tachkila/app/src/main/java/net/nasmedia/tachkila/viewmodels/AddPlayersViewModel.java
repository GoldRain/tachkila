package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class AddPlayersViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    User mUser;

    public AddPlayersViewModel(Context context, OnDataLoaded listener) {
        this.mContext = context;
        this.mListener = listener;
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void addPlayers(int groupId, List<My_current_friend> selectedFriends) {
        if (selectedFriends != null && selectedFriends.size() > 0) {
            if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
                mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
                if (selectedFriends != null && selectedFriends.size() > 0) {
                    TestModel model = new TestModel();
                    model.setGroupId(groupId);
                    ArrayList<Integer> userIds = new ArrayList<Integer>();
                    for (My_current_friend user : selectedFriends) {
                        userIds.add(user.getId());
                    }
                    model.setUserIds(userIds);
                    model.setMyID(Utils.getSavedUserIdInSharedPref(mContext));
                    mApiHandler.getServices().inviteUsersToGroup(model)
                    .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<CreateGroupResponse>() {
                                @Override
                                public void call(final CreateGroupResponse createGroupResponse) {
                                    mRealm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.copyToRealmOrUpdate(createGroupResponse.getResult().getData());
                                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                        }
                                    });
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            });
                }
            } else {
                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
            }
        }else{
            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
        }
    }

    public List<My_current_friend> getUserFriends() {
        return mRealm.copyFromRealm(mUser.getMy_current_friends());
    }
}
