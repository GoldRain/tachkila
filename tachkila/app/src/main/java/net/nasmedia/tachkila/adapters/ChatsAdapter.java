package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import net.nasmedia.tachkila.ChatWrapper;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by galal on 01/06/17.
 */

public class ChatsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ChatWrapper> mItems;
    Context mContext;
    OnItemClickListener mOnItemClickListener;

    public ChatsAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        mItems = Collections.EMPTY_LIST;
        mOnItemClickListener = onItemClickListener;
    }

    public void setItems(List<ChatWrapper> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        ChatWrapper group = mItems.get(position);

        viewHolder.mGroupNameTxt.setText(group.getGroupName());
        if (group.isMuted()) {
            viewHolder.muteIc.setVisibility(View.VISIBLE);
        } else {
            viewHolder.muteIc.setVisibility(View.GONE);
        }
        if (group.isHasNewMessage()) {
            viewHolder.unReadCircle.setVisibility(View.VISIBLE);
        } else {
            viewHolder.unReadCircle.setVisibility(View.GONE);
        }

        viewHolder.latestChatUserTxt.setText(group.getUsername());


        viewHolder.latestMessageTxt.setText(group.getMessage());

        Constants.getPicGroupUrl(viewHolder.mGroupImg, Constants.URL_GROUP_PIC + group.getGroupAvatar());


        viewHolder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(position, v);
            }
        });
    }

    public int getSelectedGroupId(int position) {
        return mItems.get(position).getGroupId();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.groupImg) CircleImageView mGroupImg;
        @BindView(R.id.groupNameTxt) CustomTextView mGroupNameTxt;
        @BindView(R.id.root) LinearLayout mRoot;
        @BindView(R.id.latestChatUserTxt) CustomTextView latestChatUserTxt;
        @BindView(R.id.latestMessageTxt) CustomTextView latestMessageTxt;
        @BindView(R.id.muteIc) ImageView muteIc;
        @BindView(R.id.unReadCircle) ImageView unReadCircle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
