package net.nasmedia.tachkila.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 27/02/2017.
 */

public class ImageFullScreenActivity extends BaseActivity {

    public final static String ARG_AVATAR = "ARG_AVATAR";

    @BindView(R.id.imageView)
    ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_screen);
        ButterKnife.bind(this);

        String url = getIntent().getStringExtra(ARG_AVATAR);

        Picasso.with(imageView.getContext())
                .load("http://app.tachkila.com/" + url)
                .into(imageView);
    }
}
