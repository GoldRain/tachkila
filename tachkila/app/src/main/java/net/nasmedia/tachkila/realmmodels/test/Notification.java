
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Notification extends RealmObject{
@PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("group_id")
    @Expose
    private int group_id;
    @SerializedName("game_id")
    @Expose
    private int game_id;
    @SerializedName("friend_id")
    @Expose
    private int friend_id;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("group")
    @Expose
    private Group_ group;
    @SerializedName("game")
    @Expose
    private Game game;
    @SerializedName("friend")
    @Expose
    private Friend friend;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * 
     * @param user_id
     *     The user_id
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * 
     * @return
     *     The group_id
     */
    public int getGroup_id() {
        return group_id;
    }

    /**
     * 
     * @param group_id
     *     The group_id
     */
    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    /**
     * 
     * @return
     *     The game_id
     */
    public int getGame_id() {
        return game_id;
    }

    /**
     * 
     * @param game_id
     *     The game_id
     */
    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    /**
     * 
     * @return
     *     The friend_id
     */
    public int getFriend_id() {
        return friend_id;
    }

    /**
     * 
     * @param friend_id
     *     The friend_id
     */
    public void setFriend_id(int friend_id) {
        this.friend_id = friend_id;
    }

    /**
     * 
     * @return
     *     The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * 
     * @param created_at
     *     The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * 
     * @return
     *     The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * 
     * @param updated_at
     *     The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * 
     * @return
     *     The group
     */
    public Group_ getGroup() {
        return group;
    }

    /**
     * 
     * @param group
     *     The group
     */
    public void setGroup(Group_ group) {
        this.group = group;
    }

    /**
     * 
     * @return
     *     The game
     */
    public Game getGame() {
        return game;
    }

    /**
     * 
     * @param game
     *     The game
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * 
     * @return
     *     The friend
     */
    public Friend getFriend() {
        return friend;
    }

    /**
     * 
     * @param friend
     *     The friend
     */
    public void setFriend(Friend friend) {
        this.friend = friend;
    }

}
