package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.GameInviteModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.viewgame.ViewGameResponse;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class InvitePlayersGameViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    NewMatchViewModel.OnError mErrorListener;
    User mUser;
    int mGameId;

    public InvitePlayersGameViewModel(Context context, int gameId, OnDataLoaded listener, NewMatchViewModel.OnError errorListener) {
        this.mContext = context;
        this.mListener = listener;
        mApiHandler = ApiHandler.getInstance();
        mGameId = gameId;
        mErrorListener = errorListener;
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void invitePlayers(List<My_current_friend> players) {
        if (players != null && players.size() > 0) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);
            ArrayList<Integer> playersIds = new ArrayList<>();
            for (My_current_friend player : players) {
                playersIds.add(player.getId());
            }
            GameInviteModel gameInviteModel = new GameInviteModel();
            gameInviteModel.setGameID(mGameId);
            gameInviteModel.setMyID(Utils.getSavedUserIdInSharedPref(mContext));
            gameInviteModel.setUserIds(playersIds);

            mApiHandler.getServices().gameInvite(gameInviteModel)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<ViewGameResponse>() {
                        @Override
                        public void call(ViewGameResponse viewGameResponse) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                try {
                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                    String errorString = "";
                                    for (String string : response.getResult().getError()) {
                                        errorString += string + "\n";
                                    }                                    mErrorListener.showError(errorString);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            } else {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }
                    });

        }
    }

    public List<My_current_friend> getUserFriends() {
        return mRealm.copyFromRealm(mUser.getMy_current_friends());
    }
}
