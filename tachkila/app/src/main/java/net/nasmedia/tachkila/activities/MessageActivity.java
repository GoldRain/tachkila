package net.nasmedia.tachkila.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.GroupDetailsFragment;
import net.nasmedia.tachkila.fragments.MessagesFragment;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by galal on 08/06/17.
 */

public class MessageActivity extends BaseActivity {

    @BindView(R.id.toolbarImg)
    CircleImageView toolbarImg;
    @BindView(R.id.toolbarTitle)
    CustomTextView toolbarTitle;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;

    String name;
    int mGroupId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);
        setToolBar();

        mGroupId = getIntent().getIntExtra(MessagesFragment.ARG_GROUP_ID, 0);

        Group group = Realm.getDefaultInstance().where(Group.class).equalTo("id", mGroupId).findFirst();

        if (group != null) {

            name = group.getName();

            toolbarTitle.setText(group.getName());
            Constants.getPicGroupUrl(toolbarImg, Constants.URL_GROUP_PIC + group.getAvatar());
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.container, MessagesFragment.getInstance(mGroupId)).commit();
    }

    private void setToolBar() {
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageActivity.this.finish();
            }
        });
        myToolbar.setNavigationIcon(R.drawable.back_chevron);
    }

    @OnClick(R.id.chatName)
    public void onViewClicked() {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.setAction(Constants.ACTION_SHOW_GROUP_DETAILS_FROM_FRAGMENT);
        intent.putExtra("name", name);
        intent.putExtra(GroupDetailsFragment.ARG_GROUP_ID, mGroupId);
        startActivity(intent);
    }
}
