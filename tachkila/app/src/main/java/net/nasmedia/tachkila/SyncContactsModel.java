package net.nasmedia.tachkila;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class SyncContactsModel {

    @SerializedName("userID")
    int userID;

    @SerializedName("phones")
    ArrayList<String> phones;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public ArrayList<String> getPhones() {
        return phones;
    }

    public void setPhones(ArrayList<String> phones) {
        this.phones = phones;
    }
}
