package net.nasmedia.tachkila.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.fragments.BaseFragment;
import net.nasmedia.tachkila.fragments.GroupFragment;
import net.nasmedia.tachkila.fragments.HomeFragment;
import net.nasmedia.tachkila.fragments.MatchHistoryFragment;
import net.nasmedia.tachkila.fragments.PlayerCardFragment;
import net.nasmedia.tachkila.fragments.PlayersContainerFragment;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.register.RegisterResponse;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.RealPathUtil;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CircularProgressBar;
import net.nasmedia.tachkila.widgets.residemenu.ResideMenu;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 7/15/2016.
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.toolbarImg)
    ImageView toolbarImg;

    CircleImageView userImg;
    TextView userNameTxt;
    TextView userNickName;
    LinearLayout homeItemContainer;
    LinearLayout playerItemContainer;
    LinearLayout grouptemContainer;
    LinearLayout playerCardItemContainer;
    LinearLayout matchHistoryItemContainer;
    CircularProgressBar mCircularProgressBar;
    TextView mScoreTxt;
    ImageButton mSettingsBtn;

    private String mAvatarPath;

    private FragmentSwitcherReceiver fragmentSwitcherReceiver = new FragmentSwitcherReceiver();

    private ResideMenu mResideMenu;
    private MaterialDialog mProgressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mProgressDialog = new MaterialDialog.Builder(this)
                .content("Please wait ...")
                .progress(true, 0)
                .build();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        myToolbar.setNavigationIcon(R.drawable.menu);

        mResideMenu = new ResideMenu(this, R.layout.left_menu, -1);
        ButterKnife.bind(mResideMenu.getLeftMenuView());
        mResideMenu.setBackground(R.drawable.menu_bkg);
        mResideMenu.attachToActivity(this);
        mResideMenu.setScaleValue(0.5f);

        mResideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        grouptemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.grouptemContainer);
        grouptemContainer.setOnClickListener(this);
        homeItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.homeItemContainer);
        homeItemContainer.setOnClickListener(this);


        playerCardItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.playerCardItemContainer);
        playerCardItemContainer.setOnClickListener(this);
        playerItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.playerItemContainer);
        playerItemContainer.setOnClickListener(this);
        matchHistoryItemContainer = (LinearLayout) mResideMenu.getLeftMenuView().findViewById(R.id.matchHistoryItemContainer);
        matchHistoryItemContainer.setOnClickListener(this);
        mSettingsBtn = (ImageButton) mResideMenu.getLeftMenuView().findViewById(R.id.settingBtn);
        mSettingsBtn.setOnClickListener(this);
        mCircularProgressBar = (CircularProgressBar) mResideMenu.getLeftMenuView().findViewById(R.id.winProgress);
        mScoreTxt = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.scoreTxt);

        userImg = (CircleImageView) mResideMenu.getLeftMenuView().findViewById(R.id.userImg);
        User user = Realm.getDefaultInstance().where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(this)).findFirst();
        loadImage(user.getAvatar(), userImg);

        userImg.setOnClickListener(this);

        userNameTxt = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.userNameTxt);
        userNameTxt.setText(user.getName());
        userNickName = (TextView) mResideMenu.getLeftMenuView().findViewById(R.id.userNickName);
        userNickName.setText("@" + user.getUsername());

        mScoreTxt.setText(user.getPoints() + "");
        mCircularProgressBar.setTitle("Win " + user.getWin());
        mCircularProgressBar.setLoss("Lose " + user.getLosses());
        mCircularProgressBar.setSubTitle("Total " + (user.getWin() + user.getLosses()));
        mCircularProgressBar.setMax(user.getWin() + user.getLosses());
        mCircularProgressBar.setProgress(user.getWin());

        homeItemContainer.setTag("Selected");
        homeItemContainer.setSelected(true);
        playerCardItemContainer.setTag("UnSelected");
        grouptemContainer.setTag("UnSelected");
        playerItemContainer.setTag("UnSelected");
        matchHistoryItemContainer.setTag("UnSelected");

        loadFragment(0, null);
    }

    private void loadImage(String url, ImageView imageView) {
//        if (url == null || url.equals("")) {
//            imageView.setImageResource(R.drawable.ic_user_default);
//        } else {
            Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url);
//            Glide.with(this)
//                    .load(Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url))
//                    .bitmapTransform(new CropCircleTransformation(Glide.get(this).getBitmapPool()))
//                    .into(imageView);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Intent filter for fragment switching broadcasts
        IntentFilter fragmentSwitcherFilter = new IntentFilter();
        fragmentSwitcherFilter.addCategory(Intent.CATEGORY_DEFAULT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT);
        fragmentSwitcherFilter.addAction(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT);


        // Registers the receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(
                fragmentSwitcherReceiver,
                fragmentSwitcherFilter);
        //////////////////////////////////////////////////////////////////
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregisters the fragmentSwitcherReceiver instance
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.fragmentSwitcherReceiver);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.settingBtn) {
            mResideMenu.closeMenu();
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.setAction(Constants.ACTION_SHOW_SETTINGS_FRAGMENT);
            startActivity(intent);
            finish();
            return;
        }

        if (view.getId() == R.id.userImg) {
//            pickImage();
            return;
        }

        if (!view.getTag().equals("Selected")) {
            unSelectSideMenuItem((LinearLayout) mResideMenu.getLeftMenuView().findViewWithTag("Selected"));
            switch (view.getId()) {
                case R.id.homeItemContainer:
                    selectSideMenuItem(homeItemContainer);
                    loadFragment(0, null);
                    break;
                case R.id.playerCardItemContainer:
                    selectSideMenuItem(playerCardItemContainer);
                    loadFragment(1, null);
                    break;
                case R.id.grouptemContainer:
                    selectSideMenuItem(grouptemContainer);
                    loadFragment(2, null);
                    break;
                case R.id.playerItemContainer:
                    selectSideMenuItem(playerItemContainer);
                    loadFragment(3, null);
                    break;
                case R.id.matchHistoryItemContainer:
                    selectSideMenuItem(matchHistoryItemContainer);
                    Intent intent = new Intent();
                    intent.putExtra(MatchHistoryFragment.ARG_GROUP_ID, 0);
                    loadFragment(4, intent);
                    break;
            }
        }
    }

    private void pickImage() {
        new MaterialDialog.Builder(this)
                .title("Pick image")
                .content("Chose how you want to pick image")
                .positiveText("Camera")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        pickFromCamera();
                    }
                })
                .negativeText("Gallery")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        pickFromGallery();
                    }
                })
                .show();
    }

    private void pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            if (Build.VERSION.SDK_INT < 19) {
                Intent intent = new Intent();
                intent.setType("image/jpeg");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            } else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/jpeg");
                startActivityForResult(intent, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT < 19) {
                        Intent intent = new Intent();
                        intent.setType("image/jpeg");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent, 1);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/jpeg");
                        startActivityForResult(intent, 1);
                    }
                } else {
                    Toast.makeText(this, "No permission for Gallery", Toast.LENGTH_LONG).show();
                }

                break;
            case 2:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
                    File newdir = new File(dir);
                    newdir.mkdirs();
                    String file = dir + "Tachkila" + ".jpg";
                    File newfile = new File(file);
                    try {
                        newfile.createNewFile();
                    } catch (IOException e) {
                    }

                    outputFileUri = Uri.fromFile(newfile);

                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                    startActivityForResult(cameraIntent, 2);

                } else {
                    Toast.makeText(this, "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }

// SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                mAvatarPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                mAvatarPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                mAvatarPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            Picasso.with(this).load(mAvatarPath)
                    .into(userImg);

            userImg.invalidate();

            uploadImage(mAvatarPath);

        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            mAvatarPath = outputFileUri.getPath();
            Picasso.with(this).load(mAvatarPath)
                    .into(userImg);
            userImg.invalidate();
            uploadImage(mAvatarPath);
        }
    }

    private void uploadImage(String avatarPath) {
        mProgressDialog.show();
        File avatarFile = null;
        if (avatarPath != null) {
            avatarFile = new File(avatarPath);
        }


        RequestBody requestFile = null;
        if (avatarFile != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
        }

        RequestBody userNameBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody emailBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody birthDateBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody favNumBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody favFootBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody nameBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody phoneBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody sexBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody deviceType =
                RequestBody.create(
                        MediaType.parse("text/plain"), "2");
        RequestBody positionIdBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody heightBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody countryIdBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody passwordBody =
                RequestBody.create(
                        MediaType.parse("text/plain"), "");
        RequestBody userId =
                RequestBody.create(
                        MediaType.parse("text/plain"), String.valueOf(Utils.getSavedUserIdInSharedPref(this)));


        ApiHandler.getInstance().getServices().
                updateUser(requestFile, userNameBody, passwordBody, nameBody, emailBody,
                        phoneBody, countryIdBody, sexBody,
                        favFootBody, favNumBody, positionIdBody, heightBody, birthDateBody,
                        deviceType, userId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<RegisterResponse>() {
                    @Override
                    public void call(final RegisterResponse registerResponse) {
                        mProgressDialog.dismiss();
                        Realm realm = Realm.getDefaultInstance();
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(registerResponse.getResult().getData());
                            }
                        });
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(MainActivity.this, getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }
                                DialogsUtils.showError(MainActivity.this, errorString, null);

                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(MainActivity.this, getString(R.string.error_happened), null);
                            }
                        } else {
                            DialogsUtils.showError(MainActivity.this, getString(R.string.error_happened), null);
                        }
                    }
                });
    }

    private void pickFromCamera() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        } else {
            final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
            File newdir = new File(dir);
            newdir.mkdirs();
            String file = dir + "Tachkila" + ".jpg";
            File newfile = new File(file);
            try {
                newfile.createNewFile();
            } catch (IOException e) {
            }

            outputFileUri = Uri.fromFile(newfile);

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            startActivityForResult(cameraIntent, 2);
        }
    }

    Uri outputFileUri;

    private void selectSideMenuItem(LinearLayout view) {
        view.setSelected(true);
        view.setTag("Selected");
        mResideMenu.closeMenu();
    }

    private void unSelectSideMenuItem(LinearLayout view) {
        view.setTag("UnSelected");
        view.setSelected(false);
    }

    public void loadFragment(int fragmentIndex, Intent intent) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseFragment baseFragment = null;
        switch (fragmentIndex) {
            case 0:
                baseFragment = HomeFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "HomeFragment").commit();
                break;
            case 1:
                baseFragment = PlayerCardFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "PLayerCard").commit();
                break;
            case 2:
                baseFragment = GroupFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "Groups").commit();
                break;
            case 3:
                baseFragment = PlayersContainerFragment.getInstance();
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "PlayersContainerFragment").commit();
                break;
            case 4:
                baseFragment = MatchHistoryFragment.getInstance(getIntent().getIntExtra(MatchHistoryFragment.ARG_GROUP_ID, 0));
                fragmentManager.beginTransaction().replace(R.id.container, baseFragment, "MatchHistoryFragment").commit();
                break;

        }
        if (baseFragment.getTitle() != null) {
            toolbarTitle.setText(baseFragment.getTitle());
            if (baseFragment.getTitle().equals("Home")) {
                toolbarTitle.setVisibility(View.GONE);
                toolbarImg.setImageResource(R.drawable.ic_tachkila_logo);
                toolbarImg.setVisibility(View.VISIBLE);
            } else {
                toolbarTitle.setVisibility(View.VISIBLE);
                toolbarImg.setVisibility(View.GONE);
            }
        }
    }

    private class FragmentSwitcherReceiver extends BroadcastReceiver {
        public FragmentSwitcherReceiver() {
            super();
        }

        /**
         * Receives broadcast Intents for content Switching requests , and displays the
         * appropriate Fragment.
         *
         * @param context The current Context of the callback
         * @param intent  The broadcast Intent that triggered the callback
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT)) {
                loadFragment(3, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_NEW_Group_FRAGMENT)) {
                loadFragment(4, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT)) {
                loadFragment(5, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT)) {
                loadFragment(6, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT)) {
                loadFragment(8, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT)) {
                loadFragment(9, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT)) {
                loadFragment(10, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_WEBVIEW_FRAGMENT)) {
                loadFragment(12, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT)) {
                loadFragment(13, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_FRAGMENT)) {
                loadFragment(14, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYERS_BY_USERNAME_FRAGMENT)) {
                loadFragment(15, intent);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_ADDRESS_BOOK_FRAGMENT)) {
                loadFragment(16, null);
            } else if (intent.getAction().equals(Constants.ACTION_SHOW_INVITE_PLAYER_TO_GAME_FRAGMENT)) {
                loadFragment(17, intent);
            }
        }

    }
}
