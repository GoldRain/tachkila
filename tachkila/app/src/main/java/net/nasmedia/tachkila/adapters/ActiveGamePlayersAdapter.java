package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.fragments.OtherPlayerCardFragment;
import net.nasmedia.tachkila.realmmodels.test.Player;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class ActiveGamePlayersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Player> mFriendsList;
    private Context mContext;

    public ActiveGamePlayersAdapter(Context context, List<Player> friendsList) {
        this.mContext = context;
        mFriendsList = friendsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_active_game_players, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        final Player friend = mFriendsList.get(position);
        if (friend.getId() == Utils.getSavedUserIdInSharedPref(mContext)) {
            viewHolder.mPlayerName.setText(friend.getUser().getUsername() + " (YOU)");
        } else {
            viewHolder.mPlayerName.setText(friend.getUser().getUsername());
        }
        loadImage(friend.getUser().getAvatar(), viewHolder.mPlayerImg);

        viewHolder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getIntent().getIntExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, 0), getIntent().getStringExtra(OtherPlayerCardFragment.ARG_TYPE)

                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, friend.getPlayer_id());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                mContext.startActivity(intent);

            }
        });

    }

    private void loadImage(String url, ImageView imageView) {
        Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url);
    }

    @Override
    public int getItemCount() {
        return mFriendsList.size();
    }

    public Player getItemAtPosition(int position) {
        return mFriendsList.get(position);
    }

    public void addItemAtPosition(Player player, int position) {
        mFriendsList.add(position, player);
        notifyDataSetChanged();
    }

    public void removeItemAtPosition(int position) {
        mFriendsList.remove(position);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.playerImg)
        ImageView mPlayerImg;
        @BindView(R.id.playerName)
        TextView mPlayerName;
        @BindView(R.id.root)
        LinearLayout root;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
