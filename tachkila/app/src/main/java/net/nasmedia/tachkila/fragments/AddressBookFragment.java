package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.ContactsAdapter;
import net.nasmedia.tachkila.adapters.SyncContactsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.RecyclerItemClickListener;
import net.nasmedia.tachkila.viewmodels.AddressBookViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class AddressBookFragment extends BaseFragment implements OnDataLoaded, NewMatchViewModel.OnError {

    private static final String TAG = AddressBookFragment.class.getSimpleName();

    @BindView(R.id.playersList)
    RecyclerView mPlayersList;
    @BindView(R.id.contactList)
    RecyclerView mContactList;

    ContactsAdapter mContactsAdapter;
    SyncContactsAdapter mSyncContactsAdapter;
    AddressBookViewModel mViewModel;
    @BindView(R.id.root)
    NestedScrollView root;
    @BindView(R.id.progress)
    ProgressBar progress;

    public static AddressBookFragment getInstance() {
        return new AddressBookFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewModel = new AddressBookViewModel(context, this, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_book, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);

        setupRecycler();
        mViewModel.displayContacts();

        mContactList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mContactList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                sendSMS(((ContactsAdapter) mContactList.getAdapter()).getPhoneNumberAtPosition(position));
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));


        mPlayersList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mPlayersList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, ((SyncContactsAdapter) mPlayersList.getAdapter()).getUserIdAtPosition(position));
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        return view;
    }

    private void setupRecycler() {
        mContactList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPlayersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContactsAdapter = new ContactsAdapter();
        mContactList.setAdapter(mContactsAdapter);
        mSyncContactsAdapter = new SyncContactsAdapter(getActivity());
        mPlayersList.setAdapter(mSyncContactsAdapter);

    }



    public void sendSMS(String phone) {
        Uri uri = Uri.parse("smsto:" + phone);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "Please join us to Tachkila application by click on this link : http://www.tachkila.com");
        startActivity(it);
    }

    @Override
    public String getTitle() {
        return "All Contacts";
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {

        root.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);

        mContactsAdapter.setItems(mViewModel.mContactsList);

        switch (responseStates) {
            case ERROR:

                break;
            case NO_DATA:

                break;
            case NO_NETWORK:

                break;
            case START:

                break;
            case SUCCESS:
                mSyncContactsAdapter.setItems(mViewModel.getUsers());
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView sv = new SearchView(((DetailsActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mPlayersList.getAdapter() != null)
                    ((SyncContactsAdapter) mPlayersList.getAdapter()).filter(newText);
                if (mContactList.getAdapter() != null)
                    ((ContactsAdapter) mContactList.getAdapter()).filter(newText);
                return false;
            }
        });
    }

    @Override
    public void showError(String error) {

    }
}
