
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class User_ extends RealmObject{

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("country_id")
    @Expose
    private int country_id;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("losses")
    @Expose
    private int losses;
    @SerializedName("win")
    @Expose
    private int win;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("position_id")
    @Expose
    private int position_id;
    @SerializedName("favorite_number")
    @Expose
    private int favorite_number;
    @SerializedName("perfect_foot")
    @Expose
    private String perfect_foot;
    @SerializedName("isAdmin")
    @Expose
    private int isAdmin;
    @SerializedName("role_id")
    @Expose
    private int role_id;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;
    @SerializedName("remember_token")
    @Expose
    private String remember_token;
    @SerializedName("active")
    @Expose
    private int active;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("country")
    @Expose
    private Country_ country;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username
     *     The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return
     *     The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * 
     * @param birthday
     *     The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 
     * @param avatar
     *     The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 
     * @return
     *     The country_id
     */
    public int getCountry_id() {
        return country_id;
    }

    /**
     * 
     * @param country_id
     *     The country_id
     */
    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    /**
     * 
     * @return
     *     The sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 
     * @param sex
     *     The sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 
     * @return
     *     The losses
     */
    public int getLosses() {
        return losses;
    }

    /**
     * 
     * @param losses
     *     The losses
     */
    public void setLosses(int losses) {
        this.losses = losses;
    }

    /**
     * 
     * @return
     *     The win
     */
    public int getWin() {
        return win;
    }

    /**
     * 
     * @param win
     *     The win
     */
    public void setWin(int win) {
        this.win = win;
    }

    /**
     * 
     * @return
     *     The points
     */
    public int getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The position_id
     */
    public int getPosition_id() {
        return position_id;
    }

    /**
     * 
     * @param position_id
     *     The position_id
     */
    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    /**
     * 
     * @return
     *     The favorite_number
     */
    public int getFavorite_number() {
        return favorite_number;
    }

    /**
     * 
     * @param favorite_number
     *     The favorite_number
     */
    public void setFavorite_number(int favorite_number) {
        this.favorite_number = favorite_number;
    }

    /**
     * 
     * @return
     *     The perfect_foot
     */
    public String getPerfect_foot() {
        return perfect_foot;
    }

    /**
     * 
     * @param perfect_foot
     *     The perfect_foot
     */
    public void setPerfect_foot(String perfect_foot) {
        this.perfect_foot = perfect_foot;
    }

    /**
     * 
     * @return
     *     The isAdmin
     */
    public int getIsAdmin() {
        return isAdmin;
    }

    /**
     * 
     * @param isAdmin
     *     The isAdmin
     */
    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * 
     * @return
     *     The role_id
     */
    public int getRole_id() {
        return role_id;
    }

    /**
     * 
     * @param role_id
     *     The role_id
     */
    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    /**
     * 
     * @return
     *     The deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * 
     * @param deviceType
     *     The deviceType
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 
     * @return
     *     The deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * 
     * @param deviceToken
     *     The deviceToken
     */
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    /**
     * 
     * @return
     *     The remember_token
     */
    public String getRemember_token() {
        return remember_token;
    }

    /**
     * 
     * @param remember_token
     *     The remember_token
     */
    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    /**
     * 
     * @return
     *     The active
     */
    public int getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * 
     * @param created_at
     *     The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * 
     * @return
     *     The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * 
     * @param updated_at
     *     The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    /**
     * 
     * @return
     *     The country
     */
    public Country_ getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(Country_ country) {
        this.country = country;
    }

}
