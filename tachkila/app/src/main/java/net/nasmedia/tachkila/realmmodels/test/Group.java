
package net.nasmedia.tachkila.realmmodels.test;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import net.nasmedia.tachkila.realmmodels.ActiveGame;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Group extends RealmObject{

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("admin_id")
    @Expose
    private int admin_id;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("deleted_at")
    @Expose
    private String deleted_at;
    @SerializedName("users")
    @Expose
    private RealmList<User_> users = new RealmList<User_>();
    @SerializedName("games")
    @Expose
    private RealmList<Game> games = new RealmList<Game>();
    @SerializedName("admin")
    @Expose
    private Admin admin;
    @SerializedName("active_games")
    @Expose
    private RealmList<ActiveGame> active_games = new RealmList<ActiveGame>();
    @SerializedName("finished_games")
    @Expose
    private RealmList<Finished_game> finished_games = new RealmList<Finished_game>();

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 
     * @param avatar
     *     The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The admin_id
     */
    public int getAdmin_id() {
        return admin_id;
    }

    /**
     * 
     * @param admin_id
     *     The admin_id
     */
    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    /**
     * 
     * @return
     *     The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * 
     * @param created_at
     *     The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * 
     * @return
     *     The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * 
     * @param updated_at
     *     The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * 
     * @return
     *     The deleted_at
     */
    public String getDeleted_at() {
        return deleted_at;
    }

    /**
     * 
     * @param deleted_at
     *     The deleted_at
     */
    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }


    /**
     * 
     * @return
     *     The users
     */
    public List<User_> getUsers() {
        return users;
    }

    /**
     * 
     * @param users
     *     The users
     */
    public void setUsers(RealmList<User_> users) {
        this.users = users;
    }

    /**
     * 
     * @return
     *     The games
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     * 
     * @param games
     *     The games
     */
    public void setGames(RealmList<Game> games) {
        this.games = games;
    }

    /**
     * 
     * @return
     *     The admin
     */
    public Admin getAdmin() {
        return admin;
    }

    /**
     * 
     * @param admin
     *     The admin
     */
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    /**
     * 
     * @return
     *     The active_games
     */
    public List<ActiveGame> getActive_games() {
        return active_games;
    }

    /**
     * 
     * @param active_games
     *     The active_games
     */
    public void setActive_games(RealmList<ActiveGame> active_games) {
        this.active_games = active_games;
    }

    /**
     * 
     * @return
     *     The finished_games
     */
    public List<Finished_game> getFinished_games() {
        return finished_games;
    }

    /**
     * 
     * @param finished_games
     *     The finished_games
     */
    public void setFinished_games(RealmList<Finished_game> finished_games) {
        this.finished_games = finished_games;
    }

}
