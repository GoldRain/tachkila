
package net.nasmedia.tachkila.realmmodels.test;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Player extends RealmObject{
@PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("team")
    @Expose
    private String team;
    @SerializedName("game_id")
    @Expose
    private int game_id;
    @SerializedName("player_id")
    @Expose
    private int player_id;
    @SerializedName("position_x")
    @Expose
    private int position_x;
    @SerializedName("position_y")
    @Expose
    private int position_y;
    @SerializedName("user")
    @Expose
    private User__ user;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The team
     */
    public String getTeam() {
        return team;
    }

    /**
     * 
     * @param team
     *     The team
     */
    public void setTeam(String team) {
        this.team = team;
    }

    /**
     * 
     * @return
     *     The game_id
     */
    public int getGame_id() {
        return game_id;
    }

    /**
     * 
     * @param game_id
     *     The game_id
     */
    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    /**
     * 
     * @return
     *     The player_id
     */
    public int getPlayer_id() {
        return player_id;
    }

    /**
     * 
     * @param player_id
     *     The player_id
     */
    public void setPlayer_id(int player_id) {
        this.player_id = player_id;
    }

    /**
     * 
     * @return
     *     The position_x
     */
    public int getPosition_x() {
        return position_x;
    }

    /**
     * 
     * @param position_x
     *     The position_x
     */
    public void setPosition_x(int position_x) {
        this.position_x = position_x;
    }

    /**
     * 
     * @return
     *     The position_y
     */
    public int getPosition_y() {
        return position_y;
    }

    /**
     * 
     * @param position_y
     *     The position_y
     */
    public void setPosition_y(int position_y) {
        this.position_y = position_y;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User__ getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User__ user) {
        this.user = user;
    }

}
