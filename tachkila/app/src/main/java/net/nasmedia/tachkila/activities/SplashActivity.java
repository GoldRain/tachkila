package net.nasmedia.tachkila.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.realmmodels.GameType;
import net.nasmedia.tachkila.realmmodels.Paths;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.settings.SettingsResponse;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.util.UUID;

import io.realm.Realm;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SplashActivity extends BaseActivity implements Realm.Transaction {

    Realm mRealm;
    ApiHandler mApiHandler;
    Subscription mSubscription;

    SettingsResponse mSettingsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Log.d("ggg", UUID.randomUUID().toString());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mRealm = Realm.getDefaultInstance();
        mApiHandler = ApiHandler.getInstance();
        getSettingsData();
    }

    private void getSettingsData() {
        unSubscribeFromObservable();
        String url = "settings";
        if (PreferenceHelper.getInstance(this).isUserLogged()) {
            url += "?userID=" + Utils.getSavedUserIdInSharedPref(this);
        }
        mSubscription = mApiHandler.getServices().getSettings(url).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SettingsResponse>() {
                    @Override
                    public void call(final SettingsResponse response) {
                        mSettingsResponse = response;
                        mRealm.executeTransactionAsync(SplashActivity.this);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        if (mRealm.where(Settings.class).count() > 0) {
                            openNextActivity();
                        } else {
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                DialogsUtils.showError(SplashActivity.this, getString(R.string.error_No_Ineternet_connection), null);
                            } else {
                                DialogsUtils.showError(SplashActivity.this, getString(R.string.error_happened), null);
                            }
                        }
                    }
                });
    }

    @Override
    protected void onPause() {
        destroy();
        super.onPause();
    }

    public void destroy() {
        reset();
    }

    private void unSubscribeFromObservable() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    private void reset() {
        unSubscribeFromObservable();
        mRealm.close();
        mRealm = null;
        mSubscription = null;
    }

    @Override
    public void execute(Realm realm) {
        realm.delete(User.class);
        realm.delete(Color.class);
        realm.delete(ActiveGame.class);
        realm.delete(Country.class);
        realm.delete(GameType.class);
        realm.delete(Position.class);
        realm.delete(Settings.class);

        Settings settings = mSettingsResponse.getSettingsResult().getData().getSettings();
        settings.setPrimaryKet(1);
        realm.copyToRealmOrUpdate(settings);
        Paths paths = mSettingsResponse.getSettingsResult().getData().getPaths();
        paths.setPrimaryKey(1);
        realm.copyToRealmOrUpdate(paths);
        if (mSettingsResponse.getSettingsResult().getData().getUser() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getUser());
        if (mSettingsResponse.getSettingsResult().getData().getColors() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getColors());
        if (mSettingsResponse.getSettingsResult().getData().getActiveGames() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getActiveGames());
        if (mSettingsResponse.getSettingsResult().getData().getCountries() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getCountries());
        if (mSettingsResponse.getSettingsResult().getData().getGameTypes() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getGameTypes());
        if (mSettingsResponse.getSettingsResult().getData().getPositions() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getPositions());
        if (mSettingsResponse.getSettingsResult().getData().getChats() != null && mSettingsResponse.getSettingsResult().getData().getChats().getMessages() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getChats().getMessages());

        openNextActivity();
    }

    private void openNextActivity() {
        if (PreferenceHelper.getInstance(this).isUserLogged()) {
            startMainActivity();
        } else {
            startForumActivity();
        }
        this.finish();
    }

    private void startForumActivity() {
        startActivity(new Intent(this, ForumActivity.class));
    }

    private void startMainActivity() {
        startActivity(new Intent(this, WelcomeActivity.class));
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_out_right);
    }
}
