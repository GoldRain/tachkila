package net.nasmedia.tachkila.services.response.creategames;

/**
 * Created by Mahmoud Galal on 8/18/2016.
 */
public class CreateGameResponse {
    CreateGameResult result;

    public CreateGameResult getResult() {
        return result;
    }

    public void setResult(CreateGameResult result) {
        this.result = result;
    }
}
