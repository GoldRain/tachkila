package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tubb.smrv.SwipeMenuRecyclerView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.BaseAdapter;
import net.nasmedia.tachkila.adapters.PlayersAdapter;
import net.nasmedia.tachkila.adapters.FollowersAdapter;
import net.nasmedia.tachkila.adapters.SuggestedAdapter;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.My_followers_friend;
import net.nasmedia.tachkila.realmmodels.test.My_people_my_know_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Mahmoud Galal on 9/17/2016.
 */
public class FollwersPlayersFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    SwipeMenuRecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;

    FollowersAdapter mAdapter;

    public static FollwersPlayersFragment getInstance() {
        FollwersPlayersFragment playersFragment = new FollwersPlayersFragment();
        return playersFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new FollowersAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

        unSubscribeFromObservable();
        mSubscription = mRealm.where(My_followers_friend.class).findAllAsync()
//        mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(getActivity())).findFirstAsync()
                .asObservable()
                .filter(new Func1<RealmResults<My_followers_friend>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<My_followers_friend> my_people_my_know_friends) {
                        return my_people_my_know_friends.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<My_followers_friend>>() {
                    @Override
                    public void call(RealmResults<My_followers_friend> my_people_my_know_friends) {
                        if (my_people_my_know_friends != null && my_people_my_know_friends.size() > 0) {
                            mAdapter.setUsers(mRealm.copyFromRealm(my_people_my_know_friends));
                            mNoDataTxt.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        }else{
                            mNoDataTxt.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                });
    }

}
