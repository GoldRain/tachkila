package net.nasmedia.tachkila.services.response.groups;

/**
 * Created by Mahmoud Galal on 8/31/2016.
 */
public class ViewGroupResponse {

    ViewGroupResult result;

    public ViewGroupResult getResult() {
        return result;
    }

    public void setResult(ViewGroupResult result) {
        this.result = result;
    }
}
