package net.nasmedia.tachkila.services.response.error;

/**
 * Created by Mahmoud Galal on 19/10/2016.
 */

public class ErrorAbstract {

    ErrorResponse errorResponse;
    ErrorRegisterResponse errorRegisterResponse;

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }

    public ErrorRegisterResponse getErrorRegisterResponse() {
        return errorRegisterResponse;
    }

    public void setErrorRegisterResponse(ErrorRegisterResponse errorRegisterResponse) {
        this.errorRegisterResponse = errorRegisterResponse;
    }
}
