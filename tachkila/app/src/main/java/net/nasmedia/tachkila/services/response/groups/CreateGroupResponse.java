package net.nasmedia.tachkila.services.response.groups;

/**
 * Created by Sherif on 8/19/2016.
 */
public class CreateGroupResponse {
    CreateGroupResult result;

    public CreateGroupResult getResult() {
        return result;
    }

    public void setResult(CreateGroupResult result) {
        this.result = result;
    }
}
