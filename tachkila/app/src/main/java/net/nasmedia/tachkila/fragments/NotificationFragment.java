package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onesignal.OneSignal;
import com.tubb.smrv.SwipeMenuRecyclerView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.NotificationAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.Notification;
import net.nasmedia.tachkila.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Mahmoud Galal on 7/16/2016.
 */
public class NotificationFragment extends BaseFragment implements OnItemClickListener {

    @BindView(R.id.notificationRecyclerView)
    SwipeMenuRecyclerView mRecyclerView;
    @BindView(R.id.noData)
    TextView mNoDataTxt;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    private NotificationAdapter mAdapter;

    public static NotificationFragment getInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAdapter = new NotificationAdapter(context);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, view);
        OneSignal.clearOneSignalNotifications();

        setupPullToRefresh();
        setupRecyclerView();

        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    private void setupRecyclerView() {
        mAdapter = new NotificationAdapter(getActivity());
        mAdapter.setClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        RecyclerView.ItemDecoration itemDecoration =
//                new DividerItemDecoration(getActivity(), R.dimen.recyclerViewVerticalSpace);
//        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadNotifications();
    }

    private void loadNotifications() {
        unSubscribeFromObservable();
        mSubscription = mRealm.where(Notification.class).findAllSortedAsync("id", Sort.DESCENDING).asObservable().filter(new Func1<RealmResults<Notification>, Boolean>() {
            @Override
            public Boolean call(RealmResults<Notification> notifications) {
                return notifications.isLoaded();
            }
        }).subscribe(new Action1<RealmResults<Notification>>() {
            @Override
            public void call(RealmResults<Notification> notifications) {
                if (notifications.size() > 0) {
                    mAdapter.setNotificationList(mRealm.copyFromRealm(notifications));
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mNoDataTxt.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mNoDataTxt.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public String getTitle() {
        return "Notification";
    }

    @Override
    public void onItemClick(int position, View view) {
        Notification notification = ((NotificationAdapter) mRecyclerView.getAdapter()).getItemAtPosition(position);
        if (notification.getFriend_id() != 0) {
            Intent playerIntent = new Intent(getActivity(), DetailsActivity.class);
            playerIntent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, notification.getFriend_id());
            playerIntent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
            playerIntent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
            startActivity(playerIntent);
        } else if (notification.getGame_id() != 0) {
            Intent gameDetails = new Intent(getActivity(), DetailsActivity.class);
            gameDetails.putExtra(GameDetailsFragment.ARG_GAME_ID, notification.getGame_id());
            gameDetails.setAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
            startActivity(gameDetails);
        } else if (notification.getGroup_id() != 0) {
            Intent groupIntent = new Intent(getActivity(), DetailsActivity.class);
            groupIntent.putExtra(GroupDetailsFragment.ARG_GROUP_ID, notification.getGroup_id());
            groupIntent.putExtra("name", notification.getGroup().getName());
            groupIntent.setAction(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT);
            startActivity(groupIntent);
        }
    }
}
