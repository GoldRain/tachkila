package net.nasmedia.tachkila.utils;

import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorRegisterResponse;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Mahmoud Galal on 18/10/2016.
 */

public class ErrorUtils {

    public static ErrorAbstract parseError(Response<?> response) {

        ErrorAbstract error = new ErrorAbstract();

        try {

            ErrorRegisterResponse errorRegisterResponse;

            Converter<ResponseBody, ErrorRegisterResponse> converter =
                    ApiHandler.getInstance().getRetrofit()
                            .responseBodyConverter(ErrorRegisterResponse.class, new Annotation[0]);

            try {
                errorRegisterResponse = converter.convert(response.errorBody());
                error.setErrorRegisterResponse(errorRegisterResponse);
            } catch (IOException e) {
                return new ErrorAbstract();
            }
        } catch (Exception e) {
            e.printStackTrace();

            ErrorResponse errorResponse;

            Converter<ResponseBody, ErrorResponse> converter =
                    ApiHandler.getInstance().getRetrofit()
                            .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

            try {
                errorResponse = converter.convert(response.errorBody());
                error.setErrorResponse(errorResponse);
            } catch (IOException e1) {
                return new ErrorAbstract();
            }

        }


        return error;
    }
}
