package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sherif on 8/18/2016.
 */
public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.MyViewHolder> {

    List<Group> mGroups;
    Context mContext;
    private OnItemClickListener mClickListener;

    public void setClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }


    public GroupAdapter(Context fragment) {
        mContext = fragment;
        mGroups = Collections.EMPTY_LIST;
    }


    public void setGroups(List<Group> groups) {
        mGroups = groups;
        notifyDataSetChanged();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Group groupItem = mGroups.get(position);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(position, view);
            }
        });

        holder.mTextViewTitleGroup.setText(groupItem.getName());
        Constants.getPicGroupUrl(holder.mImageGroup, Constants.URL_GROUP_PIC + groupItem.getAvatar());
//        Glide.with(mContext)
//                .load(Constants.getPicResizeUrl(holder.mImageGroup, Constants.URL_GROUP_PIC + groupItem.getAvatar()))
//                .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
//                .into(holder.mImageGroup);
    }

    public Group getGroupAtPosition(int position) {
        return mGroups.get(position);
    }

    @Override
    public int getItemCount() {
        return mGroups.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewGroupCardView)
        ImageView mImageGroup;
        @BindView(R.id.textViewGroupCardView)
        CustomTextView mTextViewTitleGroup;
        @BindView(R.id.root)
        LinearLayout root;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

