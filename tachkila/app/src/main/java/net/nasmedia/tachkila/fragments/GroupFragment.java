package net.nasmedia.tachkila.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.GroupAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CustomTextView;
import net.nasmedia.tachkila.widgets.ItemDecorationAlbumColumns;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends BaseFragment implements OnItemClickListener {

    private static final String TAG = GroupFragment.class.getSimpleName();

    @BindView(R.id.gridViewGroups)
    RecyclerView gridViewGroups;
    @BindView(R.id.noDataTxt)
    CustomTextView noDataTxt;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    GroupAdapter mAdapter;

    public static GroupFragment getInstance() {
        return new GroupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.groups_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_ADD_NEW_Group_FRAGMENT);
                startActivity(intent);
                break;
        }
        return true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        ButterKnife.bind(this, view);


        setupPullToRefresh();
        setupRecyclerView();

        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    private void setupRecyclerView() {
        gridViewGroups.setHasFixedSize(true);
        gridViewGroups.setLayoutManager(new GridLayoutManager(this.getActivity(), 3));
        gridViewGroups.addItemDecoration(new ItemDecorationAlbumColumns(getActivity(), R.dimen.groupRecyclerViewVerticalSpace));
        mAdapter = new GroupAdapter(getActivity());
        mAdapter.setClickListener(this);
        gridViewGroups.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadGroups();
    }

    private void loadGroups() {
        unSubscribeFromObservable();
        mSubscription = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(getActivity())).findFirstAsync().asObservable()
                .filter(new Func1<RealmObject, Boolean>() {
                    @Override
                    public Boolean call(RealmObject realmObject) {
                        return realmObject.isLoaded();
                    }
                }).subscribe(new Action1<RealmObject>() {
                    @Override
                    public void call(RealmObject realmObject) {
                        User user = (User) realmObject;
                        if (user.getGroups().size() > 0) {
                            mAdapter.setGroups(mRealm.copyFromRealm(user.getGroups()));
                            noDataTxt.setVisibility(View.GONE);
                            gridViewGroups.setVisibility(View.VISIBLE);

                        } else {
                            noDataTxt.setVisibility(View.VISIBLE);
                            gridViewGroups.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public String getTitle() {
        return "Groups";
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(GroupDetailsFragment.ARG_GROUP_ID, mAdapter.getGroupAtPosition(position).getId());
        intent.putExtra("name", mAdapter.getGroupAtPosition(position).getName());
        intent.setAction(Constants.ACTION_SHOW_GROUP_DETAILS_FRAGMENT);
        startActivity(intent);
    }
}
