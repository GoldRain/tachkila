package net.nasmedia.tachkila.viewmodels;

import android.content.Context;

import net.nasmedia.tachkila.TestModel;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.CreateGroupResponse;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/11/2016.
 */
public class CreateGroupViewModel {

    Realm mRealm;
    Context mContext;
    OnDataLoaded mListener;
    ApiHandler mApiHandler;
    User mUser;

    public CreateGroupViewModel(Context context, OnDataLoaded listener) {
        this.mContext = context;
        this.mListener = listener;
        mApiHandler = ApiHandler.getInstance();
    }


    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    public void createGroup(String avatarPath, String groupName, final List<My_current_friend> selectedFriends) {
        mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

        RequestBody requestFile = null;
        File avatarFile = null;
        if (avatarPath != null) {
            avatarFile = new File(avatarPath);
        }
        if (avatarFile != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
        }
        RequestBody groupNameBody = RequestBody.create(MediaType.parse("text/plain"), groupName);
        RequestBody adminIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mUser.getId()));
        mApiHandler.getServices().createGroup(requestFile, groupNameBody, adminIdBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<CreateGroupResponse>() {
                    @Override
                    public void call(final CreateGroupResponse createGroupResponse) {
                        if (selectedFriends != null && selectedFriends.size() > 0) {
                            TestModel model = new TestModel();
                            model.setGroupId(createGroupResponse.getResult().getData().getId());
                            ArrayList<Integer> userIds = new ArrayList<Integer>();
                            for (My_current_friend user : selectedFriends) {
                                userIds.add(user.getId());
                            }
                            model.setUserIds(userIds);
                            model.setMyID(Utils.getSavedUserIdInSharedPref(mContext));


                            mApiHandler.getServices().inviteUsersToGroup(model)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<CreateGroupResponse>() {
                                @Override
                                public void call(final CreateGroupResponse createGroupResponse) {
                                    mRealm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst().getGroups().add(createGroupResponse.getResult().getData());
                                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                        }
                                    });
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    throwable.printStackTrace();
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            });
                        } else {
                            mRealm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    Group group = realm.copyToRealmOrUpdate(createGroupResponse.getResult().getData());
                                    mUser.getGroups().add(group);
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                                }
                            });
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                        } else
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                    }
                });
    }

    public List<My_current_friend> getUserFriends() {
        return mRealm.copyFromRealm(mUser.getMy_current_friends());
    }
}
