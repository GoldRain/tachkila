package net.nasmedia.tachkila.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.SelectFriendsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.viewmodels.CreateGroupViewModel;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;


/**
 * Created by Sherif on 8/19/2016.
 */
public class NewGroupFragment extends BaseFragment implements OnDataLoaded {
    String avatarPath = null;
    @BindView(R.id.addGroupPictureInsTxt)
    TextView mAddGroupPictureInsTxt;
    @BindView(R.id.groupPictureImg)
    ImageView mGroupPictureImg;
    @BindView(R.id.editTextGroupName)
    EditText mGroupNameEdt;
    @BindView(R.id.searchEdt)
    EditText mSearchEdt;
    @BindView(R.id.friendsList)
    RecyclerView mFriendsList;
    @BindView(R.id.friendsContainer)
    LinearLayout mFriendsContainer;

    SelectFriendsAdapter mSelectFriendsAdapter;

    ProgressDialog mProgressDialog;
    CreateGroupViewModel mViewModel;

    public static BaseFragment getInstance() {
        return new NewGroupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mViewModel = new CreateGroupViewModel(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_group, container, false);
        ButterKnife.bind(this, view);

        mSearchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((SelectFriendsAdapter) mFriendsList.getAdapter()).filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    @Override
    public String getTitle() {
        return "Create Group";
    }


    ArrayList<String> filePaths = new ArrayList<>();

    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        } else {
            FilePickerBuilder.getInstance().setMaxCount(1)
                    .setSelectedFiles(filePaths)
                    .setActivityTheme(R.style.AppTheme)
                    .enableVideoPicker(false)
                    .pickPhoto(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.AppTheme)
                            .pickPhoto(this);
                } else {
                    Toast.makeText(getActivity(), "No permission for Camera", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK && data != null) {
            filePaths = new ArrayList<>();
            filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
            File file = new File(filePaths.get(0));
            Picasso.with(getActivity()).load(file).resize(100, 100)
                    .into(mGroupPictureImg);

            mGroupPictureImg.invalidate();
            mAddGroupPictureInsTxt.setVisibility(View.GONE);
            mGroupPictureImg.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.addGroupPictureInsTxt, R.id.groupPictureImg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addGroupPictureInsTxt:
            case R.id.groupPictureImg:
                pickImage();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_group_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                createGroup();
                break;
        }
        return true;

    }

    private void createGroup() {
        if (!TextUtils.isEmpty(mGroupNameEdt.getText().toString())) {
            if (filePaths != null && filePaths.size() > 0) {
                avatarPath = filePaths.get(0);
            }
            mViewModel.createGroup(avatarPath, mGroupNameEdt.getText().toString(), mSelectFriendsAdapter != null ? mSelectFriendsAdapter.getSelectedFriends() : null);
        } else {
            Toast.makeText(getActivity(), "Please enter group name", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                Toast.makeText(getActivity(), "Error happened", Toast.LENGTH_LONG).show();
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_LONG).show();
                break;
            case START:
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Please wait ...");
                mProgressDialog.setTitle(null);
                mProgressDialog.show();
                break;
            case SUCCESS:
                Toast.makeText(getActivity(), "Group created", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        if (mViewModel.getUserFriends() != null && mViewModel.getUserFriends().size() > 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mFriendsList.setLayoutManager(linearLayoutManager);
            mSelectFriendsAdapter = new SelectFriendsAdapter(getActivity());
            mSelectFriendsAdapter.setFriendsList(mViewModel.getUserFriends());
            mFriendsList.setAdapter(mSelectFriendsAdapter);
            mFriendsContainer.setVisibility(View.VISIBLE);
        } else {
            mFriendsContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }
}
