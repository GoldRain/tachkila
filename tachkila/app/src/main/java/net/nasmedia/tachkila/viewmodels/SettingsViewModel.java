package net.nasmedia.tachkila.viewmodels;

import android.content.Context;
import android.util.Log;

import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.error.ErrorAbstract;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.register.RegisterResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.ErrorUtils;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 13/10/2016.
 */

public class SettingsViewModel {

    private static final String TAG = SettingsViewModel.class.getSimpleName();

    Realm mRealm;
    ApiHandler mApiHandler;
    Context mContext;
    OnDataLoaded mListener;
    User mUser;
    private OnError mOnError;


    public SettingsViewModel(Context context, OnDataLoaded onDataLoaded, OnError onError) {
        this.mContext = context;
        this.mListener = onDataLoaded;
        mOnError = onError;
    }

    public void onStart() {
        mRealm = Realm.getDefaultInstance();
        mApiHandler = ApiHandler.getInstance();

        mUser = mRealm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
    }

    public void onStop() {
        mRealm.close();
    }

    public User getUser() {
        return mUser;
    }

    public List<String> getGenderList() {
        ArrayList<String> genders = new ArrayList<>(2);
        genders.add("female");
        genders.add("male");

        return genders;
    }

    public int getUserGenderPosition() {
        if (mUser.getSex().toLowerCase().equals("male")) {
            return 1;
        } else {
            return 0;
        }
    }

    public ArrayList<String> getCountriesNames() {
        ArrayList<String> countriesNames = new ArrayList<>();
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();
        for (Country country : countries) {
            countriesNames.add(country.getTitleEn());
        }
        return countriesNames;
    }

    public String geCountryId(int countryPosition) {
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();

        return String.valueOf(countries.get(countryPosition).getId());
    }

    public int getSelectedCountryPosition() {
        Country country = mRealm.where(Country.class).equalTo("id", mUser.getCountry_id()).findFirst();
        RealmResults<Country> countries = mRealm.where(Country.class).findAll();
        return countries.indexOf(country);
    }

    public ArrayList<String> getPositionsNames() {
        ArrayList<String> positionsNames = new ArrayList<>();
        RealmResults<Position> positions = mRealm.where(Position.class).findAll();
        for (Position country : positions) {
            positionsNames.add(country.getName());
        }
        return positionsNames;
    }

    public String gePositionId(int positionPosition) {
        RealmResults<Position> positions = mRealm.where(Position.class).findAll();
        return String.valueOf(positions.get(positionPosition).getId());
    }

    public int getSelectedPostionPosition() {
        Position position = mRealm.where(Position.class).equalTo("id", mUser.getPosition_id()).findFirst();
        RealmResults<Position> positions = mRealm.where(Position.class).findAll();
        return positions.indexOf(position);
    }

    public List<String> getPerfectFootList() {
        ArrayList<String> foots = new ArrayList<>(2);
        foots.add("left");
        foots.add("right");

        return foots;
    }

    public int getUserPerfectFootPosition() {
        if (mUser.getSex().toLowerCase().equals("left")) {
            return 0;
        } else {
            return 1;
        }
    }


    public void updateUser(String avatarPath, String userName, String password, String email, String birthDate,
                           String favNum, String favFoot, String name, String phone, String sex,
                           String height, String countryId, String positionId) {

//        if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
            mListener.downloadComplete(OnDataLoaded.ResponseStates.START);

            File avatarFile = null;
            if (avatarPath != null) {
                avatarFile = new File(avatarPath);
            }


            RequestBody requestFile = null;
            if (avatarFile != null) {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
            }
            RequestBody userNameBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), userName);
            RequestBody emailBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), email);
            RequestBody birthDateBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), birthDate != null ? birthDate : "");
            RequestBody favNumBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), favNum != null ? favNum : "");
            RequestBody favFootBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), favFoot != null ? favFoot : "");
            RequestBody nameBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), name != null ? name : "");
            RequestBody phoneBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), phone != null ? phone : "");
            RequestBody sexBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), sex != null ? sex : "");
            RequestBody deviceType =
                    RequestBody.create(
                            MediaType.parse("text/plain"), "2");
            RequestBody positionIdBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), positionId);
            RequestBody heightBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), height != null ? height : "");
            RequestBody countryIdBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), countryId);
            RequestBody passwordBody =
                    RequestBody.create(
                            MediaType.parse("text/plain"), password);
            RequestBody userId =
                    RequestBody.create(
                            MediaType.parse("text/plain"), String.valueOf(Utils.getSavedUserIdInSharedPref(mContext)));


            ApiHandler.getInstance().getServices().
                    updateUser(requestFile, userNameBody, passwordBody, nameBody, emailBody,
                            phoneBody, countryIdBody, sexBody,
                            favFootBody, favNumBody, positionIdBody, heightBody, birthDateBody,
                            deviceType, userId).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<RegisterResponse>() {
                        @Override
                        public void call(final RegisterResponse registerResponse) {
                            mRealm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(registerResponse.getResult().getData());
                                }
                            });
                            mListener.downloadComplete(OnDataLoaded.ResponseStates.SUCCESS);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                            RetrofitException error = (RetrofitException) throwable;
                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.NO_NETWORK);
                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                try {
                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                    String errorString = "";
                                    for (String string : response.getResult().getError()) {
                                        errorString += string + "\n";
                                    }
                                    mOnError.showError(errorString);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                                }
                            } else {
                                mListener.downloadComplete(OnDataLoaded.ResponseStates.ERROR);
                            }
                        }
                    });
    }

    public boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public String getFaqUrl() {
        return mRealm.where(Settings.class).findFirst().getFaqUrl();
    }

    public String getPrivacyUrl() {
        return mRealm.where(Settings.class).findFirst().getPrivacyUrl();
    }

    public String getContactUrl() {
        return mRealm.where(Settings.class).findFirst().getContactUrl();
    }

    public void clearDataBase(){
        mRealm.deleteAll();
    }

    public interface OnError {
        void showError(String error);
    }

}
