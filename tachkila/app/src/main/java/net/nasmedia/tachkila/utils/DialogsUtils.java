package net.nasmedia.tachkila.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import net.nasmedia.tachkila.R;


/**
 * Created by Mahmoud Galal on 13/12/2016.
 */

public class DialogsUtils {

    public static void showError(Context context, String error, MaterialDialog.SingleButtonCallback onPositiveClick) {
        new MaterialDialog.Builder(context)
                .title(context.getString(R.string.error_happened))
                .content(error)
                .positiveText(context.getString(R.string.Ok))
                .onPositive(onPositiveClick)
                .show();
    }




}
