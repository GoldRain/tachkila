package net.nasmedia.tachkila.services.response;

import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.test.Finished_game;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class FinishedGameData {

    int success;
    Finished_game data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public Finished_game getData() {
        return data;
    }

    public void setData(Finished_game data) {
        this.data = data;
    }
}
