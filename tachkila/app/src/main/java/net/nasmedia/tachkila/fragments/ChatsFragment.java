package net.nasmedia.tachkila.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nasmedia.tachkila.ChatWrapper;
import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.MessageActivity;
import net.nasmedia.tachkila.adapters.ChatsAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.MutedGroups;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;
import net.nasmedia.tachkila.realmmodels.test.User_;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by galal on 01/06/17.
 */

public class ChatsFragment extends BaseFragment implements OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.matchList)
    RecyclerView mRecyclerView;
    @BindView(R.id.noDataTxt)
    CustomTextView mNoDataTxt;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    ChatsAdapter mAdapter;

    public static ChatsFragment getInstance() {
        ChatsFragment fragment = new ChatsFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        unbinder = ButterKnife.bind(this, view);

        setSwipeContainer(mSwipeContainer);

        setupRecyclerView();

        return view;
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new ChatsAdapter(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        reloadFragment();
        mRealm.where(UnReadMessage.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<UnReadMessage>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<UnReadMessage> unReadMessages) {
                        return unReadMessages.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<UnReadMessage>>() {
            @Override
            public void call(RealmResults<UnReadMessage> unReadMessages) {
                if (isAdded())
                    reloadFragment();
            }
        });

    }

    private void removeSystemMessages(){
        final Realm deleteRealm = Realm.getDefaultInstance();
        deleteRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Message> rows = deleteRealm.where(Message.class).equalTo("type","system").findAll();
                rows.deleteAllFromRealm();
            }
        });
    }

    private void reloadFragment() {
        unSubscribeFromObservable();
        final Map<Integer, List<Message>> messageMap = new HashMap<>();
        removeSystemMessages();

        mSubscription = Realm.getDefaultInstance().where(Message.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<Message>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Message> messages) {
                        return messages.isLoaded();
                    }
                })
                .flatMap(new Func1<RealmResults<Message>, Observable<Map<Integer, List<Message>>>>() {
                    @Override
                    public Observable<Map<Integer, List<Message>>> call(RealmResults<Message> messages) {
                        for (Message message : messages) {
                            if(message.getType() == "system") {
                                continue;
                            }
                            else {
                                if (messageMap.containsKey(message.getGroup_id())) {
                                    messageMap.get(message.getGroup_id()).add(message);
                                } else {
                                    List<Message> messages1 = new ArrayList<Message>();
                                    messages1.add(message);
                                    messageMap.put(message.getGroup_id(), messages1);
                                }
                            }
                        }
                        return Observable.just(messageMap);
                    }
                })
                .flatMap(new Func1<Map<Integer, List<Message>>, Observable<List<ChatWrapper>>>() {
                    @Override
                    public Observable<List<ChatWrapper>> call(Map<Integer, List<Message>> messages) {
                        List<ChatWrapper> groups = new ArrayList<>();

                        for (Map.Entry<Integer, List<Message>> entry : messages.entrySet()) {

                            int groupId = entry.getKey();
                            List<Message> tempMessageList = entry.getValue();
                            ArrayList<Message> messageList = new ArrayList<>();

                            for (Message message: tempMessageList) {
                                if (!message.getType().equals("system")){
                                    Log.d("type ===> ", message.getType());
                                    messageList.add(message);
                                }
                            }
                            Message lastMessage = messageList.get(messageList.size() - 1);


//                        }
//                        for (Message message : messages) {
                            ChatWrapper chatWrapper = new ChatWrapper();

                            Group group = Realm.getDefaultInstance().where(Group.class).equalTo("id", groupId).findFirst();
                            chatWrapper.setGroupId(group.getId());
                            chatWrapper.setGroupAvatar(group.getAvatar());
                            chatWrapper.setGroupName(group.getName());

                            if (Realm.getDefaultInstance().where(UnReadMessage.class).equalTo("group_id", groupId).count() > 0) {
                                chatWrapper.setHasNewMessage(true);
                            } else {
                                chatWrapper.setHasNewMessage(false);
                            }

                            MutedGroups mutedGroups = Realm.getDefaultInstance().where(MutedGroups.class).equalTo("groupId", groupId).findFirst();
                            if (mutedGroups != null) {
                                chatWrapper.setMuted(true);
                            } else {
                                chatWrapper.setMuted(false);
                            }


                              chatWrapper.setMessage(lastMessage.getMessage());

                              chatWrapper.setUsername(Realm.getDefaultInstance().where(User_.class).equalTo("id", lastMessage.getUser_id()).findFirst().getUsername());

                              groups.add(chatWrapper);
                        }
                        return Observable.just(groups);
                    }
                })
                .subscribe(new Action1<List<ChatWrapper>>() {
                    @Override
                    public void call(List<ChatWrapper> groups) {
                        if (groups.size() > 0) {

                            mAdapter.setItems(groups);

                            mRecyclerView.setVisibility(View.VISIBLE);
                            mNoDataTxt.setVisibility(View.GONE);
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            mNoDataTxt.setVisibility(View.VISIBLE);
                        }
                    }
                });

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public String getTitle() {
        return "Chat";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent intent = new Intent(getActivity(), MessageActivity.class);
        intent.putExtra(MessagesFragment.ARG_GROUP_ID, mAdapter.getSelectedGroupId(position));
        startActivity(intent);
    }
}
