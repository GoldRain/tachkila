package net.nasmedia.tachkila.services.response.viewplayer;

import net.nasmedia.tachkila.realmmodels.test.My_current_friend;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class ViewPlayerData {

    int success;
    My_current_friend data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public My_current_friend getData() {
        return data;
    }

    public void setData(My_current_friend data) {
        this.data = data;
    }
}
