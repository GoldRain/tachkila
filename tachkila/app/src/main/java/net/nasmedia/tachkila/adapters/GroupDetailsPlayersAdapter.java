package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.test.User_;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class GroupDetailsPlayersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User_> mFriendsList;
    private Context mContext;
    private int mAdminId;
    private OnItemClickListener mClickListener;
    private OnKickClicked mKickClickListener;

    public void setClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public void setKickClickListener(OnKickClicked listener) {
        mKickClickListener = listener;
    }

    public GroupDetailsPlayersAdapter(Context context) {
        this.mContext = context;
        mFriendsList = new ArrayList<>();
    }

    public void setFriendsList(List<User_> friendsList, int adminId) {
        this.mFriendsList = friendsList;
        mAdminId = adminId;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_details_player_full, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(position, view);
            }
        });
        final User_ friend = mFriendsList.get(position);
        String name;
        if (friend.getId() == mAdminId) {
            name = friend.getName() + " <font color=#ff0000>(admin)</font>";
        } else {
            name = friend.getName();
        }

        if (friend.getId() == Utils.getSavedUserIdInSharedPref(mContext)){
            name += " (YOU)";
        }

        viewHolder.kick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mKickClickListener != null)
                    mKickClickListener.onKick(friend.getId());
            }
        });

        viewHolder.mPlayerName.setText(Html.fromHtml(name));

        loadImage( friend.getAvatar(), viewHolder.mPlayerImg);

    }

    private void loadImage(String url, ImageView imageView) {
//        if (url == null || url.equals("")) {
//            imageView.setImageResource(R.drawable.ic_user_default);
//        } else {
            Constants.getPicGroupUrl(imageView, Constants.URL_AVATAR_PIC + url);
//            Glide.with(mContext)
//                    .load(Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url))
//                    .placeholder(R.drawable.ic_user_default)
//                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
//                    .into(imageView);
//        }
    }

    public int getUserIdAtPosition(int position) {
        return mFriendsList.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return mFriendsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.playerImg)
        ImageView mPlayerImg;
        @BindView(R.id.playerName)
        TextView mPlayerName;
        @BindView(R.id.smContentView)
        LinearLayout root;
        @BindView(R.id.btnKick)
        TextView kick;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface OnKickClicked {
        void onKick(int userId);
    }
}
