package net.nasmedia.tachkila.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mahmoud Galal on 7/14/2016.
 */
public class Utils {

    public static void saveUserIdToSharedPref(Context context, int userId) {
        SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(Constants.USER_ID_PREF_KEY, userId);
        editor.apply();
    }

    public static int getSavedUserIdInSharedPref(Context context) {
//        return 10;
        SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getInt(Constants.USER_ID_PREF_KEY, -1);
    }

    public static int convertDpToPx(Context context, int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }


}
