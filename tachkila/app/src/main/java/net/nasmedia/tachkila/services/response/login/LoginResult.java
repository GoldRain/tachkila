package net.nasmedia.tachkila.services.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class LoginResult {
    @SerializedName("success")
    @Expose
    int success;
    @SerializedName("data")
    @Expose
    LoginData data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
