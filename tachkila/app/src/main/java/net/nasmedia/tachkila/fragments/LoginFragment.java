package net.nasmedia.tachkila.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.WelcomeActivity;
import net.nasmedia.tachkila.services.response.FinishedGameResponse;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.login.LoginResponse;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CustomButton;
import net.nasmedia.tachkila.widgets.CustomEditText;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by mahmoudgalal on 5/22/16.
 */
public class LoginFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener {

//    https://github.com/jaychang0917/SocialLoginManager

    private static final String TAG = LoginFragment.class.getSimpleName();
    private static final int RC_SIGN_IN = 9001;
    @BindView(R.id.logoImg)
    ImageView logoImg;
    @BindView(R.id.userNameEdt)
    CustomEditText mUserNameEdt;
    @BindView(R.id.passwordEdt)
    CustomEditText mPasswordEdt;
    @BindView(R.id.forgetPassBtn)
    CustomButton forgetPassBtn;
    @BindView(R.id.loginBtn)
    CustomButton mLoginBtn;
    @BindView(R.id.facebookBtn)
    LoginButton mFacebookBtn;
    @BindView(R.id.fbBtn)
    CustomButton fbBtn;
    @BindView(R.id.sign_in_button)
    CustomButton signInButton;
    @BindView(R.id.registerBtn)
    CustomButton mRegisterBtn;

    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private MaterialDialog mProgressDialog;

    private String mRegId;


    public static LoginFragment getInstance() {
        return new LoginFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mProgressDialog = new MaterialDialog.Builder(context)
                .content(R.string.Please_wait)
                .progress(true, 0)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

//        FacebookSdk.sdkInitialize(getActivity());

        setupGoogle();
        setupFacebook();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                mRegId = userId;
            }
        });

        return view;
    }

    private void setupFacebook() {
        mCallbackManager = CallbackManager.Factory.create();

        mFacebookBtn.setReadPermissions(Arrays.asList("email", "public_profile"));
        // If using in a fragment
        mFacebookBtn.setFragment(this);
        // Other app specific specialization

        // Callback registration
        mFacebookBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginBySocial("facebook", loginResult.getAccessToken().getUserId(), mRegId);
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "onCancel");

            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, exception.getMessage() + "");

            }
        });
    }

    private void setupGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private GoogleSignInAccount googleSignInAccount;

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            googleSignInAccount = result.getSignInAccount();
            loginBySocial("google", googleSignInAccount.getId(), mRegId);
            // use API

        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    @OnClick({R.id.forgetPassBtn, R.id.loginBtn, R.id.facebookBtn, R.id.fbBtn, R.id.sign_in_button, R.id.registerBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fbBtn:
                mFacebookBtn.performClick();
                break;
            case R.id.loginBtn:
                if (!TextUtils.isEmpty(mUserNameEdt.getText().toString().trim()) && !TextUtils.isEmpty(mPasswordEdt.getText().toString().trim())) {
                    loginByUsername(mUserNameEdt.getText().toString(), mPasswordEdt.getText().toString(), mRegId);
                } else {
                    DialogsUtils.showError(getActivity(), getString(R.string.error_please_complete_require_data), null);
                }
                break;
            case R.id.registerBtn:
                Intent intent = new Intent(Constants.ACTION_SHOW_REGISTER_ACTIVITY);
                intent.putExtra(RegisterFragment.ARG_REGISTER_TYPE, RegisterFragment.RegisterType.NORMAL.ordinal());
                getActivity().sendBroadcast(intent);
                break;
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.forgetPassBtn:
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_forget_pass);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(lp);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#80000000")));
                final EditText emailEdt = (EditText) dialog.findViewById(R.id.emailEdt);
                Button resetPassBtn = (Button) dialog.findViewById(R.id.resetPassBtn);
                ImageButton closeBtn = (ImageButton) dialog.findViewById(R.id.closeBtn);

                closeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                resetPassBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        if (!TextUtils.isEmpty(emailEdt.getText().toString())) {
                            resetPass(emailEdt.getText().toString());
                        } else {
                            DialogsUtils.showError(getActivity(), "Please enter your email address", null);
                        }

                    }
                });

                dialog.show();
                break;


        }
    }

    private void resetPass(String email) {
        mProgressDialog.show();
        unSubscribeFromObservable();
        mSubscription = mApiHandler.getServices().resetPass(email).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FinishedGameResponse>() {
                    @Override
                    public void call(FinishedGameResponse finishedGameResponse) {
                        mProgressDialog.dismiss();
                        new AlertDialogWrapper.Builder(getActivity())
                                .setTitle("Reset password")
                                .setMessage("Please check your email to complete your password resetting")
                                .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }                                DialogsUtils.showError(getActivity(), errorString, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);

                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                });
    }

    private void loginBySocial(String socialType, String socialToken, String mRegId) {
        mProgressDialog.show();
        unSubscribeFromObservable();
        mSubscription = mApiHandler.getServices().loginBySocial(socialType, socialToken, 2, mRegId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<LoginResponse>() {
                    @Override
                    public void call(final LoginResponse loginResponse) {
                        mProgressDialog.dismiss();
                        Utils.saveUserIdToSharedPref(getActivity(), loginResponse.getResult().getData().getUser().getId());
                        PreferenceHelper.getInstance(getActivity()).setIsUserLogged(true);
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(loginResponse.getResult().getData().getUser());
                            }
                        });
                        startActivity(new Intent(getActivity(), WelcomeActivity.class));
                        getActivity().finish();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }                                DialogsUtils.showError(getActivity(), errorString, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);

                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                });
    }

    private void loginByUsername(String username, String password, String mRegId) {
        mProgressDialog.show();
        unSubscribeFromObservable();
        mSubscription = mApiHandler.getServices().loginByUsername(username, password, 2, mRegId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<LoginResponse>() {
                    @Override
                    public void call(final LoginResponse loginResponse) {
                        mProgressDialog.dismiss();
                        Utils.saveUserIdToSharedPref(getActivity(), loginResponse.getResult().getData().getUser().getId());
                        PreferenceHelper.getInstance(getActivity()).setIsUserLogged(true);
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(loginResponse.getResult().getData().getUser());
                            }
                        });
                        startActivity(new Intent(getActivity(), WelcomeActivity.class));
                        getActivity().finish();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                        } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                            try {
                                ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                String errorString = "";
                                for (String string : response.getResult().getError()) {
                                    errorString += string + "\n";
                                }                                DialogsUtils.showError(getActivity(), errorString, null);
                            } catch (IOException e) {
                                e.printStackTrace();
                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);

                            }
                        } else {
                            DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                        }
                    }
                });
    }
}
