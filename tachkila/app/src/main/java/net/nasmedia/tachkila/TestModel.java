package net.nasmedia.tachkila;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mahmoudgalal on 8/23/16.
 */
public class TestModel {

    @SerializedName("groupID")
    int groupId;

    @SerializedName("userIDs")
    ArrayList<Integer> userIds;

    @SerializedName("myID")
    int myID;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public ArrayList<Integer> getUserIds() {
        return userIds;
    }

    public void setUserIds(ArrayList<Integer> userIds) {
        this.userIds = userIds;
    }

    public int getMyID() {
        return myID;
    }

    public void setMyID(int myID) {
        this.myID = myID;
    }
}
