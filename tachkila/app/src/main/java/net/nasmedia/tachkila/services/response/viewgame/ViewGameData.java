package net.nasmedia.tachkila.services.response.viewgame;

import net.nasmedia.tachkila.realmmodels.ActiveGame;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class ViewGameData {

    int success;
    ActiveGame data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ActiveGame getData() {
        return data;
    }

    public void setData(ActiveGame data) {
        this.data = data;
    }
}
