package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.fragments.OtherPlayerCardFragment;
import net.nasmedia.tachkila.realmmodels.test.My_current_friend;
import net.nasmedia.tachkila.realmmodels.test.My_followers_friend;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.utils.ConectivityUtils;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 9/18/2016.
 */
public class FollowersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<My_followers_friend> mUsersList;
    Context mContext;

    public FollowersAdapter(Context context) {
        mContext = context;
        mUsersList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return mUsersList.size();
    }


    public void setUsers(List<My_followers_friend> users) {
        mUsersList = users;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_full, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        final My_followers_friend user = mUsersList.get(position);
        holder.userNameTxt.setText(user.getUsername());
        holder.userScoreTxt.setText(user.getWin() + "");
        loadImage(user.getAvatar(), holder.userImg);
        holder.btAccept.setTag(position);
        holder.btAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConectivityUtils.isDeviceConnectedToNetwork(mContext)) {
                    MaterialDialog.Builder mMaterialDialogBuilder;
                    mMaterialDialogBuilder = new MaterialDialog.Builder(mContext);
                    mMaterialDialogBuilder.content(R.string.Please_wait);
                    mMaterialDialogBuilder.progress(true, 0);
                    final MaterialDialog materialDialog = mMaterialDialogBuilder.show();
                    final int pos = (int) view.getTag();
                    final My_followers_friend user1 = mUsersList.get(pos);
                    ApiHandler.getInstance().getServices().makeFriend(Utils.getSavedUserIdInSharedPref(mContext), user1.getId())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<ViewGroupResponse>() {
                                @Override
                                public void call(ViewGroupResponse viewGroupResponse) {
                                    materialDialog.dismiss();
                                    notifyItemRemoved(pos);
                                    Realm realm = Realm.getDefaultInstance();
                                    final User user2 = realm.where(User.class).equalTo("id", Utils.getSavedUserIdInSharedPref(mContext)).findFirst();
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            My_current_friend friend = new My_current_friend();
                                            friend.setId(user1.getId());
                                            friend.setName(user1.getName());
                                            friend.setUsername(user1.getUsername());
                                            friend.setActive(user1.getActive());
                                            friend.setAvatar(user1.getAvatar());
                                            friend.setBirthday(user1.getBirthday());
                                            friend.setCountry(user1.getCountry());
                                            friend.setCountry_id(user1.getCountry_id());
                                            friend.setEmail(user1.getEmail());
                                            friend.setHeight(user1.getHeight());
                                            friend.setWin(user1.getWin());
                                            friend.setSex(user1.getSex());
                                            friend.setPosition_id(user1.getPosition_id());
                                            friend.setLosses(user1.getLosses());
                                            friend.setWin(user1.getWin());
                                            friend.setPhone(user1.getPhone());
                                            friend.setName(user1.getName());
                                            friend.setPerfect_foot(user1.getPerfect_foot());


                                            user2.getMy_current_friends().add(friend);
                                            mUsersList.remove(user1);
                                        }
                                    });
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    throwable.printStackTrace();
                                    materialDialog.dismiss();
                                    Toast.makeText(mContext, mContext.getString(R.string.error_happened), Toast.LENGTH_LONG).show();

                                }
                            });

                }
            }
        });

        holder.itemPlayerContainer.setTag(position);
        holder.itemPlayerContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, mUsersList.get((Integer) view.getTag()).getId());
                intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.REQUEST);
                intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
                mContext.startActivity(intent);
            }
        });
    }

    private void loadImage(String url, ImageView imageView) {
//        if (url == null || url.equals("")) {
//            imageView.setImageResource(R.drawable.ic_user_default);
//        } else {
            Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url);
//            Glide.with(mContext)
//                    .load(Constants.getPicResizeUrl(imageView, Constants.URL_AVATAR_PIC + url))
//                    .placeholder(R.drawable.ic_user_default)
//                    .bitmapTransform(new CropCircleTransformation(Glide.get(mContext).getBitmapPool()))
//                    .into(imageView);
//        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userImg)
        ImageView userImg;
        @BindView(R.id.userNameTxt)
        TextView userNameTxt;
        @BindView(R.id.userScoreTxt)
        TextView userScoreTxt;
        @BindView(R.id.btaccept)
        TextView btAccept;
        @BindView(R.id.smContentView)
        LinearLayout itemPlayerContainer;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
