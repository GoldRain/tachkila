package net.nasmedia.tachkila.services.response;

/**
 * Created by Mahmoud Galal on 27/11/2016.
 */

public class SyncContactsResponse {

    SyncContactsData result;

    public SyncContactsData getResult() {
        return result;
    }

    public void setResult(SyncContactsData result) {
        this.result = result;
    }
}
