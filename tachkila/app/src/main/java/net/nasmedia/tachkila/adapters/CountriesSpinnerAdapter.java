package net.nasmedia.tachkila.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.test.Country;

import java.util.List;

/**
 * Created by Mahmoud Galal on 19/10/2016.
 */

public class CountriesSpinnerAdapter extends ArrayAdapter {

    private Context context;
    private List<Country> itemList;
    LayoutInflater mInflater;

    public CountriesSpinnerAdapter(Context context, int textViewResourceId, List<Country> itemList) {

        super(context, textViewResourceId);
        this.context = context;
        this.itemList = itemList;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public TextView getView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.item_spinner_country_text, parent, false);
        TextView v = (TextView) view.findViewById(android.R.id.text1);
        v.setText(itemList.get(position).getAlpha_2() + " +" + itemList.get(position).getPhonecode());
        return v;
    }

    public TextView getDropDownView(int position, View convertView,
                                    ViewGroup parent) {

        View view = mInflater.inflate(R.layout.item_spinner_text, parent, false);
        TextView v = (TextView) view.findViewById(android.R.id.text1);

        v.setText(itemList.get(position).getTitleAr());
        return v;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }
}
