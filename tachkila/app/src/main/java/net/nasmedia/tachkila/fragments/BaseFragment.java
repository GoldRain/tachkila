package net.nasmedia.tachkila.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.Color;
import net.nasmedia.tachkila.realmmodels.GameType;
import net.nasmedia.tachkila.realmmodels.Paths;
import net.nasmedia.tachkila.realmmodels.Position;
import net.nasmedia.tachkila.realmmodels.Settings;
import net.nasmedia.tachkila.realmmodels.test.Country;
import net.nasmedia.tachkila.realmmodels.test.User;
import net.nasmedia.tachkila.services.ApiHandler;
import net.nasmedia.tachkila.services.response.settings.SettingsResponse;
import net.nasmedia.tachkila.utils.PreferenceHelper;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by mahmoudgalal on 8/3/16.
 */
public abstract class BaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, Realm.Transaction {

    SwipeRefreshLayout mSwipeContainer;
    SettingsResponse mSettingsResponse;

    Subscription mSubscription;
    List<Subscription> mSubscriptions = new ArrayList<>();
    Realm mRealm;
    ApiHandler mApiHandler;


    public abstract String getTitle();

    @Override
    public void onStart() {
        super.onStart();
        if (mSwipeContainer != null) {
            mSwipeContainer.setOnRefreshListener(this);
        }
        mRealm = Realm.getDefaultInstance();
        mApiHandler = ApiHandler.getInstance();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mSwipeContainer != null) {
            mSwipeContainer.setRefreshing(false);
            mSwipeContainer.destroyDrawingCache();
            mSwipeContainer.clearAnimation();
        }
        destroy();
    }

    void destroy() {
        reset();
    }

    void unSubscribeFromObservable() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }

        for (Subscription subscription : mSubscriptions) {
            subscription.unsubscribe();
        }
    }

    void reset() {
        unSubscribeFromObservable();
        mRealm.close();
        mRealm = null;
        mApiHandler = null;
        mSubscription = null;
    }

    void setSwipeContainer(SwipeRefreshLayout swipeContainer) {
        mSwipeContainer = swipeContainer;
    }

    @Override
    public void onRefresh() {
        getSettingsData();
    }

    private void getSettingsData() {
        unSubscribeFromObservable();
        String url = "settings";
        if (PreferenceHelper.getInstance(getActivity()).isUserLogged()) {
            url += "?userID=" + Utils.getSavedUserIdInSharedPref(getActivity());
        }
        mSubscription = mApiHandler.getServices().getSettings(url).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SettingsResponse>() {
                    @Override
                    public void call(final SettingsResponse response) {
                        mSettingsResponse = response;
                        mRealm.executeTransactionAsync(BaseFragment.this);
                        mSwipeContainer.setRefreshing(false);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mSwipeContainer.setRefreshing(false);
                        throwable.printStackTrace();
                        RetrofitException error = (RetrofitException) throwable;
                        if (error.getKind() == RetrofitException.Kind.NETWORK) {
                            Toast.makeText(getActivity(), getString(R.string.error_No_Ineternet_connection), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.error_happened), Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    @Override
    public void execute(Realm realm) {
        realm.delete(User.class);
        realm.delete(Color.class);
        realm.delete(ActiveGame.class);
        realm.delete(Country.class);
        realm.delete(GameType.class);
        realm.delete(Position.class);
        realm.delete(Settings.class);

        Settings settings = mSettingsResponse.getSettingsResult().getData().getSettings();
        settings.setPrimaryKet(1);
        realm.copyToRealmOrUpdate(settings);
        Paths paths = mSettingsResponse.getSettingsResult().getData().getPaths();
        paths.setPrimaryKey(1);
        realm.copyToRealmOrUpdate(paths);
        if (mSettingsResponse.getSettingsResult().getData().getUser() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getUser());
        if (mSettingsResponse.getSettingsResult().getData().getColors() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getColors());
        if (mSettingsResponse.getSettingsResult().getData().getActiveGames() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getActiveGames());
        if (mSettingsResponse.getSettingsResult().getData().getCountries() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getCountries());
        if (mSettingsResponse.getSettingsResult().getData().getGameTypes() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getGameTypes());
        if (mSettingsResponse.getSettingsResult().getData().getPositions() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getPositions());
        if (mSettingsResponse.getSettingsResult().getData().getChats() != null && mSettingsResponse.getSettingsResult().getData().getChats().getMessages() != null)
            realm.copyToRealmOrUpdate(mSettingsResponse.getSettingsResult().getData().getChats().getMessages());
    }
}
