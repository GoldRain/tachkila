package net.nasmedia.tachkila.services.response.delete_game;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mahmoud Galal on 21/11/2016.
 */

public class DeleteGameResult {
    @SerializedName("success")
    int success;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
