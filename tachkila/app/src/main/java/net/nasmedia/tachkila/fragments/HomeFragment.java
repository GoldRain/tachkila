package net.nasmedia.tachkila.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.adapters.MatchActiveAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.ActiveGame;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.widgets.DividerItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

import static net.nasmedia.tachkila.fragments.GameDetailsFragment.ARG_GROUP_NAME;

/**
 * Created by mahmoudgalal on 8/3/16.
 */
public class HomeFragment extends BaseFragment implements OnItemClickListener {


    @BindView(R.id.matchList)
    RecyclerView mRecyclerView;

    @BindView(R.id.noDataTxt)
    TextView mNoDataTxt;
    @BindView(R.id.newMatchBtn)
    LinearLayout mNewMatchBtn;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    MatchActiveAdapter mAdapter;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.unReadCircle)
    ImageView unReadCircle;



    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        setupRecyclerView();
        setupPullToRefresh();

        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    private void setupRecyclerView() {
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new MatchActiveAdapter(getActivity());
        mAdapter.setClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), R.dimen.groupRecyclerViewVerticalSpace);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        unSubscribeFromObservable();
        mSubscription = mRealm.where(ActiveGame.class).findAllSortedAsync("timeOfGame", Sort.ASCENDING).asObservable()
                .filter(new Func1<RealmResults<ActiveGame>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<ActiveGame> activeGames) {
                        return activeGames.isLoaded();
                    }
                })
                .subscribe(new Action1<RealmResults<ActiveGame>>() {
                    @Override
                    public void call(RealmResults<ActiveGame> activeGames) {
                        if (activeGames.size() > 0) {
                            mAdapter.setNotificationList(mRealm.copyFromRealm(activeGames));
                            mNoDataTxt.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mNoDataTxt.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    }
                });

        mRealm.where(UnReadMessage.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<UnReadMessage>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<UnReadMessage> unReadMessages) {
                        return unReadMessages.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<UnReadMessage>>() {
            @Override
            public void call(RealmResults<UnReadMessage> unReadMessages) {
                if (isAdded()) {
                    if (unReadMessages.size() > 0) {
                        unReadCircle.setVisibility(View.VISIBLE);
                    } else {
                        unReadCircle.setVisibility(View.GONE);
                    }
                }
            }
        });
//        if (mRealm.where(UnReadMessage.class).findAll().size() > 0) {
//            unReadCircle.setVisibility(View.VISIBLE);
//        } else {
//            unReadCircle.setVisibility(View.GONE);
//        }
    }

    @Override
    public String getTitle() {
        return "Home";
    }

    @OnClick({R.id.newMatchBtn, R.id.btn_fab, R.id.chatBtn})

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newMatchBtn:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.setAction(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.chatBtn:
                Intent intent3 = new Intent(getActivity(), DetailsActivity.class);
                intent3.setAction(Constants.ACTION_SHOW_CHATS_FRAGMENT);
                startActivity(intent3);
                getActivity().finish();
                break;
            case R.id.btn_fab:
                Intent intent2 = new Intent(getActivity(), DetailsActivity.class);
                intent2.setAction(Constants.ACTION_SHOW_NOTIFICATION_FRAGMENT);
                startActivity(intent2);
                getActivity().finish();
                break;
        }

    }

    @Override
    public void onItemClick(int position, View view) {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(GameDetailsFragment.ARG_GAME_ID, mAdapter.getGameAtPosition(position).getId());
        Group group = mRealm.where(Group.class).equalTo("id", mAdapter.getGameAtPosition(position).getGroupId()).findFirst();
        intent.putExtra(ARG_GROUP_NAME, group != null ? group.getName() : "");
        intent.setAction(Constants.ACTION_SHOW_GAME_DETAILS_FRAGMENT);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
