package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.SelectFriendsAdapter;
import net.nasmedia.tachkila.interfaces.OnDataLoaded;
import net.nasmedia.tachkila.viewmodels.AddPlayersViewModel;
import net.nasmedia.tachkila.viewmodels.InvitePlayersGameViewModel;
import net.nasmedia.tachkila.viewmodels.NewMatchViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahmoud Galal on 9/14/2016.
 */
public class InvitePlayersGameFragment extends BaseFragment implements OnDataLoaded,NewMatchViewModel.OnError {

    public static final String ARG_GAME_ID = "gameId";
    @BindView(R.id.searchEdt)
    EditText mSearchEdt;
    @BindView(R.id.friendsList)
    RecyclerView mFriendsList;

    private int mGameId;
    SelectFriendsAdapter mSelectFriendsAdapter;
    private InvitePlayersGameViewModel mViewModel;
    private MaterialDialog mProgressDialog;
    private MaterialDialog.Builder mMaterialDialogBuilder;

    public static InvitePlayersGameFragment getInstance(int gameId) {
        InvitePlayersGameFragment fragment = new InvitePlayersGameFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GAME_ID, gameId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mGameId = getArguments().getInt(ARG_GAME_ID);
        mViewModel = new InvitePlayersGameViewModel(context, mGameId, this, this);
        mMaterialDialogBuilder = new MaterialDialog.Builder(context);
        mMaterialDialogBuilder.content(R.string.Please_wait);
        mMaterialDialogBuilder.progress(true, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_players, container, false);
        ButterKnife.bind(this, view);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mFriendsList.setLayoutManager(linearLayoutManager);
        mSelectFriendsAdapter = new SelectFriendsAdapter(getActivity());
        mFriendsList.setAdapter(mSelectFriendsAdapter);

        mSearchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ((SelectFriendsAdapter) mFriendsList.getAdapter()).filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
        mSelectFriendsAdapter.setFriendsList(mViewModel.getUserFriends());
    }

    @Override
    public void onStop() {
        mViewModel.onStop();
        super.onStop();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_players_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                mViewModel.invitePlayers(mSelectFriendsAdapter.getSelectedFriends());
                break;
        }
        return true;

    }

    @Override
    public String getTitle() {
        return null;
    }


    @Override
    public void downloadComplete(ResponseStates responseStates) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        switch (responseStates) {
            case ERROR:
                showErrorDialog(getString(R.string.error_happened));
                break;
            case NO_DATA:

                break;
            case NO_NETWORK:
                showErrorDialog(getString(R.string.error_No_Ineternet_connection));
                break;
            case START:
                if (mProgressDialog == null) {
                    mProgressDialog = mMaterialDialogBuilder.show();
                } else if (!mProgressDialog.isShowing()) {
                    mProgressDialog = mMaterialDialogBuilder.show();
                }
                break;
            case SUCCESS:
                getFragmentManager().popBackStack();
                break;
        }
    }

    private void showErrorDialog(String message) {
        new AlertDialogWrapper.Builder(getActivity())
                .setTitle(R.string.Error)
                .setMessage(message)
                .setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void showError(String error) {
        showErrorDialog(error);
    }
}
