package net.nasmedia.tachkila.services.response.viewplayer;

import net.nasmedia.tachkila.services.response.viewgame.ViewGameData;

/**
 * Created by mahmoudgalal on 11/4/16.
 */

public class ViewPlayerResponse {

    ViewPlayerData result;

    public ViewPlayerData getResult() {
        return result;
    }

    public void setResult(ViewPlayerData result) {
        this.result = result;
    }
}
