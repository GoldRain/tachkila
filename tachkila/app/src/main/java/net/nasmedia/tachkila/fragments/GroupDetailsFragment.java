package net.nasmedia.tachkila.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.onesignal.OneSignal;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.activities.DetailsActivity;
import net.nasmedia.tachkila.activities.MessageActivity;
import net.nasmedia.tachkila.adapters.GroupDetailsPlayersAdapter;
import net.nasmedia.tachkila.interfaces.OnItemClickListener;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.MutedGroups;
import net.nasmedia.tachkila.realmmodels.test.Group;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;
import net.nasmedia.tachkila.services.response.error.ErrorResponse;
import net.nasmedia.tachkila.services.response.groups.ViewGroupResponse;
import net.nasmedia.tachkila.utils.Constants;
import net.nasmedia.tachkila.utils.DialogsUtils;
import net.nasmedia.tachkila.utils.RetrofitException;
import net.nasmedia.tachkila.utils.Utils;
import net.nasmedia.tachkila.widgets.CustomButton;
import net.nasmedia.tachkila.widgets.CustomTextView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mahmoud Galal on 8/30/2016.
 */
public class GroupDetailsFragment extends BaseFragment implements OnItemClickListener, GroupDetailsPlayersAdapter.OnKickClicked {


    public static final String ARG_GROUP_ID = "ARG_GROUP_ID";
    public static final String ARG_OPEN_FROM = "ARG_OPEN_FROM";
    private static final String TAG = GroupDetailsFragment.class.getSimpleName();

    @BindView(R.id.newMatchBtn)
    LinearLayout mNewMatchBtn;
    @BindView(R.id.matchHistoryBtn)
    Button mMatchHistoryBtn;
    @BindView(R.id.leaveTheGroup)
    Button mLeaveTheGroup;
    @BindView(R.id.addNewPlayer)
    Button mAddNewPlayer;
    @BindView(R.id.playersContainer)
    RecyclerView mRecyclerView;
    @BindView(R.id.container)
    NestedScrollView mContainer;
    //    @BindView(R.id.editGroup)
//    Button mEditGroupPlayer;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    int mGroupId;
    Group mGroup;
    GroupDetailsPlayersAdapter mAdapter;

    MaterialDialog.Builder mDialog;
    @BindView(R.id.unReadCircle)
    ImageView unReadCircle;
    @BindView(R.id.photoCountTxt)
    CustomTextView photoCountTxt;
    @BindView(R.id.galleryBtn)
    RelativeLayout galleryBtn;
    @BindView(R.id.muteChatBtn)
    CustomButton muteChatBtn;
    @BindView(R.id.clearChatBtn)
    CustomButton clearChatBtn;
    @BindView(R.id.chatBtn)
    LinearLayout mChatBtn;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.chatContainer)
    RelativeLayout chatContainer;
    @BindView(R.id.topButtons)
    LinearLayout topButtons;
    @BindView(R.id.imageView_mute)
    ImageView mImageViewMute;
    private MaterialDialog mProgressDialog;


    public static GroupDetailsFragment getInstance(int groupId, boolean isOpenFromChat) {
        GroupDetailsFragment fragment = new GroupDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        bundle.putBoolean(ARG_OPEN_FROM, isOpenFromChat);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mProgressDialog = new MaterialDialog.Builder(getActivity())
                .content(R.string.Please_wait)
                .progress(true, 0)
                .build();
        mGroupId = getArguments().getInt(ARG_GROUP_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_details, container, false);
        ButterKnife.bind(this, view);

        if (getArguments().getBoolean(ARG_OPEN_FROM)) {
            chatContainer.setVisibility(View.GONE);
        }

        setupRecyclerView();
        setupPullToRefresh();


        return view;
    }

    private void setupPullToRefresh() {
        setSwipeContainer(mSwipeContainer);
        mSwipeContainer.setOnRefreshListener(this);
        mSwipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    private void setupRecyclerView() {
        mAdapter = new GroupDetailsPlayersAdapter(getActivity());
        mAdapter.setClickListener(this);
        mAdapter.setKickClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        RecyclerView.ItemDecoration itemDecoration =
//                new DividerItemDecoration(getActivity(), R.dimen.recyclerViewVerticalSpace);
//        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        getGroup();
        mRealm.where(UnReadMessage.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<UnReadMessage>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<UnReadMessage> unReadMessages) {
                        return unReadMessages.isLoaded();
                    }
                }).subscribe(new Action1<RealmResults<UnReadMessage>>() {
            @Override
            public void call(RealmResults<UnReadMessage> unReadMessages) {
                if (isAdded()) {
                    if (unReadMessages.size() > 0) {
                        unReadCircle.setVisibility(View.VISIBLE);
                    } else {
                        unReadCircle.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void getGroup() {
        unSubscribeFromObservable();
        mSubscription = mRealm.where(Group.class).equalTo("id", mGroupId).findFirstAsync().asObservable().filter(new Func1<RealmObject, Boolean>() {
            @Override
            public Boolean call(RealmObject realmObject) {
                return realmObject.isLoaded() && realmObject.isValid();
            }
        }).subscribe(new Action1<RealmObject>() {
            @Override
            public void call(RealmObject realmObject) {
                mGroup = mRealm.copyFromRealm((Group) realmObject);
                if (mGroup.getUsers() != null && mGroup.getUsers().size() > 0) {
                    mAdapter.setFriendsList(mGroup.getUsers(), mGroup.getAdmin_id());
                    mRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                }
            }
        });


        long photosCount = mRealm.where(Message.class).equalTo("group_id", mGroupId).equalTo("type", "photo").count();

        if (isGroupMuted()) {
            changeButtonText(true);
        } else {
            changeButtonText(false);
        }

        photoCountTxt.setText(String.valueOf(photosCount) + " >");

    }

    private void changeButtonText(boolean isMuted) {
        if (isMuted) {
            muteChatBtn.setText("Unmute the group");
            mImageViewMute.setImageResource(R.drawable.ios_unmute);
        } else {
            muteChatBtn.setText("Mute the group");
            mImageViewMute.setImageResource(R.drawable.ios_mute);
        }
    }

    public boolean isGroupMuted() {
        boolean groupMuted = false;

        MutedGroups mutedGroups = mRealm.where(MutedGroups.class).equalTo("groupId", mGroupId).findFirst();
        if (mutedGroups != null) {
            groupMuted = true;
        }

        return groupMuted;
    }


    @OnClick({R.id.newMatchBtn, R.id.matchHistoryBtn, R.id.leaveTheGroup, R.id.addNewPlayer, R.id.chatBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newMatchBtn:
                createNewMatch();
                break;
            case R.id.matchHistoryBtn:
                showMatchHistory();
                break;
            case R.id.leaveTheGroup:
                leaveGroup();
                break;
            case R.id.addNewPlayer:
                addNewPlayer();
                break;
//            case R.id.editGroup:
//                editGroup();
//                break;
            case R.id.chatBtn:
                chat();
                break;
        }
    }

    private void chat() {
        Intent intent = new Intent(getActivity(), MessageActivity.class);
        intent.putExtra(MessagesFragment.ARG_GROUP_ID, mGroupId);
        startActivity(intent);
    }

    private void addNewPlayer() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(AddPlayersFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_ADD_PLAYERS_FRAGMENT);
        startActivity(intent);
    }

    private void leaveGroup() {
        mDialog = new MaterialDialog.Builder(getActivity());
        mDialog.content("Are you sure you want to leave the group ?");
        mDialog.positiveText("Yes");
        mDialog.negativeText("NO");
        mDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                leaveGroupWebService();
            }
        });
        mDialog.show();
    }

    private void leaveGroupWebService() {
        mProgressDialog.show();
        unSubscribeFromObservable();
        mSubscription = mApiHandler.getServices().leaveGroup(mGroupId, Utils.getSavedUserIdInSharedPref(getActivity()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ViewGroupResponse>() {
                    @Override
                    public void call(ViewGroupResponse viewGroupResponse) {
                        mProgressDialog.dismiss();
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                Group group = mRealm.where(Group.class).equalTo("id", mGroupId).findFirst();
                                RealmResults<Message> messages = mRealm.where(Message.class).equalTo("group_id", mGroupId).findAll();
                                messages.deleteAllFromRealm();
                                group.deleteFromRealm();
                            }
                        });
                        getActivity().finish();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mProgressDialog.dismiss();
                        throwable.printStackTrace();
                    }
                });
    }

    private void showMatchHistory() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(MatchHistoryFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_MATCH_HISTORY_FRAGMENT);
        startActivity(intent);
    }

    private void createNewMatch() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra("groupId", mGroupId);
        intent.setAction(Constants.ACTION_SHOW_ADD_NEW_MATCH_FRAGMENT);
        startActivity(intent);
    }

    private void editGroup() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(EditGroupFragment.ARG_GROUP_ID, mGroupId);
        intent.setAction(Constants.ACTION_SHOW_EDIT_GROUP_FRAGMENT);
        startActivity(intent);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(OtherPlayerCardFragment.ARG_PLAYER_ID, ((GroupDetailsPlayersAdapter) mRecyclerView.getAdapter()).getUserIdAtPosition(position));
        intent.putExtra(OtherPlayerCardFragment.ARG_TYPE, OtherPlayerCardFragment.PLAYER);
        intent.setAction(Constants.ACTION_SHOW_OTHER_PLAYER_FRAGMENT);
        getActivity().startActivity(intent);
    }

    @Override
    public void onKick(final int userId) {
        if (Utils.getSavedUserIdInSharedPref(getActivity()) == mGroup.getAdmin_id()) {
            new MaterialDialog.Builder(getActivity()).title("Kick player").content("Are you sure you want to kick him off from the group")
                    .positiveText("YES")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            mProgressDialog.show();
//                            unSubscribeFromObservable();
                            mSubscription = mApiHandler.getServices().leaveGroup(mGroupId, userId)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Action1<ViewGroupResponse>() {
                                        @Override
                                        public void call(final ViewGroupResponse viewGroupResponse) {
                                            mProgressDialog.dismiss();
                                            mRealm.executeTransactionAsync(new Realm.Transaction() {
                                                @Override
                                                public void execute(Realm realm) {
                                                    realm.copyToRealmOrUpdate(viewGroupResponse.getResult().getData());
                                                }
                                            });
                                        }
                                    }, new Action1<Throwable>() {
                                        @Override
                                        public void call(Throwable throwable) {
                                            mProgressDialog.dismiss();
                                            throwable.printStackTrace();
                                            RetrofitException error = (RetrofitException) throwable;
                                            if (error.getKind() == RetrofitException.Kind.NETWORK) {
                                                DialogsUtils.showError(getActivity(), getString(R.string.error_No_Ineternet_connection), null);
                                            } else if (error.getKind() == RetrofitException.Kind.HTTP) {
                                                try {
                                                    ErrorResponse response = error.getErrorBodyAs(ErrorResponse.class);
                                                    String errorString = "";
                                                    for (String string : response.getResult().getError()) {
                                                        errorString += string + "\n";
                                                    }
                                                    DialogsUtils.showError(getActivity(), errorString, null);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                    DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                                                }
                                            } else {
                                                DialogsUtils.showError(getActivity(), getString(R.string.error_happened), null);
                                            }
                                        }
                                    });
                        }
                    })
                    .negativeText("NO").show();

        } else {
            DialogsUtils.showError(getActivity(), "Only admin can kick players from the group", null);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.galleryBtn)
    public void onGalleryBtnClicked() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.setAction(Constants.ACTION_SHOW_CHAT_GALLERY_FRAGMENT);
        intent.putExtra(GalleryFragment.ARG_GROUP_ID, mGroupId);
        startActivity(intent);
    }

    @OnClick(R.id.muteChatBtn)
    public void onMuteChatBtnClicked() {
        muteChat();
    }

    private void muteChat() {
        if (!isGroupMuted()) {
            OneSignal.sendTag("silent_group_" + mGroupId, "1");
            saveMutedGroup();
            changeButtonText(true);
        } else {
            OneSignal.sendTag("silent_group_" + mGroupId, "0");
            deleteMutedGroup();
            changeButtonText(false);
        }
    }

    private void saveMutedGroup() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MutedGroups mutedGroups = new MutedGroups();
                mutedGroups.setGroupId(mGroupId);
                realm.copyToRealmOrUpdate(mutedGroups);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.groups_detaile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(EditGroupFragment.ARG_GROUP_ID, mGroupId);
                intent.setAction(Constants.ACTION_SHOW_EDIT_GROUP_FRAGMENT);
                startActivity(intent);
                break;
        }
        return true;

    }

    private void deleteMutedGroup() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MutedGroups mutedGroups = realm.where(MutedGroups.class).equalTo("groupId", mGroupId).findFirst();
                if (mutedGroups != null) {
                    mutedGroups.deleteFromRealm();
                }
            }
        });
    }

    @OnClick(R.id.clearChatBtn)
    public void onClearChatBtnClicked() {
        new MaterialDialog.Builder(getActivity())
                .content("Are you sure?")
                .title("Clear chat")
                .positiveText("Yes")
                .negativeText("No")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mRealm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(Message.class).equalTo("group_id", mGroupId).findAll().deleteAllFromRealm();
                            }
                        });
                        photoCountTxt.setText("0 >");
                    }
                })
                .show();
    }
}
