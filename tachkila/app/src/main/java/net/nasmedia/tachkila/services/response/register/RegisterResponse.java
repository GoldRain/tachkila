package net.nasmedia.tachkila.services.response.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mahmoudgalal on 5/21/16.
 */
public class RegisterResponse {
    @SerializedName("result")
    @Expose
    private RegisterResult result;

    public RegisterResult getResult() {
        return result;
    }

    public void setResult(RegisterResult result) {
        this.result = result;
    }
}
