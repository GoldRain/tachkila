package net.nasmedia.tachkila.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nasmedia.tachkila.R;
import net.nasmedia.tachkila.adapters.GalleryAdapter;
import net.nasmedia.tachkila.realmmodels.Message;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by galal on 08/06/17.
 */

public class GalleryFragment extends BaseFragment {

public static final String ARG_GROUP_ID = "ARG_GROUP_ID";

    @BindView(R.id.galleyList)
    RecyclerView mGalleyList;
    Unbinder unbinder;
    GalleryAdapter mAdapter;

    int mGroupId;

    public static GalleryFragment getInstance(int groupId) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_GROUP_ID, groupId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        unbinder = ButterKnife.bind(this, view);

        mGroupId = getArguments().getInt(ARG_GROUP_ID);

        setupRecyclerView();



        return view;
    }

    private void setupRecyclerView() {
        mGalleyList.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mAdapter = new GalleryAdapter(getActivity());
        mGalleyList.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        mSubscription = mRealm.where(Message.class).equalTo("group_id", mGroupId).equalTo("type", "photo").findAllAsync().asObservable()
                .filter(new Func1<RealmResults<Message>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Message> messages) {
                        return messages.isLoaded();
                    }
                })
                .subscribe(new Action1<RealmResults<Message>>() {
                    @Override
                    public void call(RealmResults<Message> messages) {
                        if (messages != null && messages.size() > 0) {
                            mAdapter.setmItems(messages);
                        }
                    }
                });
    }

    @Override
    public String getTitle() {
        return "Gallery";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
