package net.nasmedia.tachkila.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionStateChange;

import net.nasmedia.tachkila.fragments.MessagesFragment;
import net.nasmedia.tachkila.realmmodels.Message;
import net.nasmedia.tachkila.realmmodels.test.UnReadMessage;
import net.nasmedia.tachkila.utils.Utils;

import io.realm.Realm;
import io.realm.internal.Context;

/**
 * Created by Mahmoud Galal on 24/09/2017.
 */

public class BaseActivity extends AppCompatActivity {

    Pusher mPusher;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PusherOptions opts = new PusherOptions();
        opts.setEncrypted(true);
        opts.setCluster("ap2");
        mPusher = new Pusher("9e375b26beee5e5f7274", opts);

        // subscribe to our "messages" channel
        Channel channel = mPusher.subscribe("user_" + Utils.getSavedUserIdInSharedPref(this));


        // listen for the "new_message" event
        channel.bind("new_message", new SubscriptionEventListener() {

            @Override
            public void onEvent(String channelName, String eventName, final String data) {

                final Message message = new Gson().fromJson(data, Message.class);

                Log.d("message_base==>", String.valueOf(message.getType()));

                BaseActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Realm realm = Realm.getDefaultInstance(); // opens "myrealm.realm"
                        try {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(message);
                                    Log.d("messages", "message recieved");
                                }
                            });
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    UnReadMessage unReadMessage = new UnReadMessage();
                                    unReadMessage.setGroup_id(message.getGroup_id());
                                    unReadMessage.setAndroid_id(message.getAndroid_id());
                                    realm.copyToRealmOrUpdate(message);
                                }
                            });
                        } finally {
                            realm.close();
                        }
                    }
                });


                Log.d("message", message.getMessage());
            }
        });


        // connect to the Pusher API
        mPusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange connectionStateChange) {
                Log.d("Pusher", "pusher state : " + connectionStateChange.getCurrentState().name());
            }

            @Override
            public void onError(String s, String s1, Exception e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPusher.disconnect();
    }
}
